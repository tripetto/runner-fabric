const fs = require("fs");
const webFontLoader = `(function(window,module){\n${fs.readFileSync("./node_modules/webfontloader/webfontloader.js", "utf-8")}return module.exports;})(typeof window === "undefined" && {} || window, typeof module === "undefined" && {exports:{}} || module)`;

function hoist(input) {
    fs.writeFileSync(input, fs.readFileSync(input, "utf-8").replace(`require("webfontloader")`, webFontLoader), "utf8");
}

hoist("./dist/esm/fontloader/loader.mjs");
