const fs = require("fs");
const pkg = require("../package.json");
const prettier = require("prettier");

pkg.peerDependencies["react"] = ">= 16.14.x";
pkg.peerDependencies["styled-components"] = ">= 5.1.x";
pkg.peerDependencies["@tripetto/runner"] = ">= 8.x";

prettier.format(JSON.stringify(pkg), { parser: "json-stringify" }).then((file) => {
    fs.writeFileSync("./package.json", file, "utf8");
});
