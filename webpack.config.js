const path = require("path");

module.exports = {
    entry: "./src/tests/app.tsx",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "src/tests/static"),
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                options: {
                    compilerOptions: {
                        noEmit: false,
                    },
                },
            },
        ],
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"],
    },
    optimization: {
        minimize: false,
    },
    devServer: {
        static: path.resolve(__dirname, "src/tests/static"),
        port: 9000,
        host: "0.0.0.0",
    },
};
