import * as Color from "color";

interface IColorOperations {
    /** Manipulate a color. */
    readonly manipulate: (manipulation: (color: Color) => Color, condition?: boolean) => IColorOperations;

    /** Evaluates the color and makes it black or white. */
    readonly makeBlackOrWhite: (condition?: boolean) => IColorOperations;

    /** Specifies a color in case the color is transparent. */
    readonly makeUnclear: (color?: string) => IColorOperations;
}

export const getColor = (input: string | undefined) => {
    if (input) {
        input = input.toLowerCase();

        if (input !== "transparent") {
            try {
                return Color.default(input);
            } catch {
                try {
                    return Color.default("#" + input);
                } catch {
                    return Color.default("rgba(255,255,255,0)");
                }
            }
        }
    }

    return Color.default("rgba(255,255,255,0)");
};

/**
 * Normalizes a color to a RGB(A) value.
 * @param input Specifies the input color (can be a hex, rgb, hsl or color name).
 * @param operations Optional operations to perform.
 */
export const color = (input: string | undefined, operations?: (color: IColorOperations) => void) => {
    let clr = getColor(input);

    if (operations) {
        const ops = {
            manipulate: (manipulation: (c: Color) => Color, condition: boolean = true) => {
                if (condition) {
                    clr = manipulation(clr);
                }

                return ops;
            },
            makeBlackOrWhite: (condition: boolean = true) => {
                if (condition) {
                    const luminance = (0.299 * clr.red() + 0.587 * clr.green() + 0.114 * clr.blue()) / 255;

                    clr = clr
                        .red(luminance > 0.55 ? 0 : 255)
                        .green(luminance > 0.55 ? 0 : 255)
                        .blue(luminance > 0.55 ? 0 : 255)
                        .alpha(1);
                }

                return ops;
            },
            makeUnclear: (c?: string) => {
                if (c && clr.alpha() === 0) {
                    clr = getColor(c);
                }

                return ops;
            },
        };

        operations(ops);
    }

    return clr.rgb().string();
};
