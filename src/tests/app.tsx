import { styled } from "styled-components";
import { createRoot } from "react-dom/client";
import { noop } from "@tripetto/runner";
import { color } from "../color";
import { useOverlay } from "../overlay";
import { ButtonFabric } from "../components/button";
import { CheckboxesFabric } from "../components/checkboxes";
import { CheckboxFabric } from "../components/checkbox";
import { DateTimeFabric } from "../components/datetime";
import { DropdownFabric } from "../components/dropdown";
import { MultiSelectFabric } from "../components/multi-select";
import { EmailFabric } from "../components/email";
import { FileFabric, IFileService } from "../components/file";
import { SignatureFabric, ISignatureService } from "../components/signature";
import { InputFabric } from "../components/input";
import { MatrixFabric } from "../components/matrix";
import { MultipleChoiceFabric } from "../components/multiple-choice";
import { PictureChoiceFabric } from "../components/picture-choice";
import { NumberFabric } from "../components/number";
import { PasswordFabric } from "../components/password";
import { PhoneNumberFabric } from "../components/phone-number";
import { RadiobuttonsFabric } from "../components/radiobuttons";
import { RatingFabric } from "../components/rating";
import { TextareaFabric } from "../components/textarea";
import { TextFabric } from "../components/text";
import { URLFabric } from "../components/url";
import { YesNoFabric } from "../components/yes-no";
import { ScaleFabric } from "../components/scale";
import { RankingFabric } from "../components/ranking";
import { avataaars } from "../components/avataaars";

/* eslint-disable @typescript-eslint/no-unused-vars */

const App = styled.div<{
    $backgroundColor?: string;
    $backgroundImage?: string;
    $backgroundMode?: "center" | "width" | "height" | "cover" | "contain" | "repeat";
    $fontFamily?: string;
    $fontSize?: number;
    $fontColor?: string;
}>`
    font-family: ${(props) => props.$fontFamily || "Arial"};
    font-size: ${(props) => props.$fontSize || 16}px;
    background-color: ${(props) => color(props.$backgroundColor || "#fff")};
    color: ${(props) => color(props.$fontColor || "#333")};
    line-height: 1.5em;
    -webkit-tap-highlight-color: transparent;
    padding: 24px;

    * {
        font-family: ${(props) => props.$fontFamily || "Arial"};
        box-sizing: border-box;
    }

    > div + div {
        margin-top: 32px;
    }

    > div {
        > * + * {
            margin-top: 8px;
        }
    }
`;

const OverlayElement = styled.nav<{
    fontFamily: string;
}>`
    font-family: ${({ fontFamily }) => fontFamily};
    font-variant-ligatures: none;
    line-height: 1.5em;
    outline: 0;

    * {
        font-family: ${({ fontFamily }) => fontFamily};
        font-variant-ligatures: none;
        box-sizing: border-box;
        outline: none;
    }

    a {
        text-decoration: underline;
        color: inherit !important;
    }

    @media (prefers-reduced-motion: reduce) {
        scroll-behavior: auto;
    }
`;

const TestIcon = (
    <svg viewBox="0 0 20 20">
        <path d="M9.854 19.354l6-6c0.195-0.195 0.195-0.512 0-0.707s-0.512-0.195-0.707 0l-5.146 5.146v-16.293c0-0.276-0.224-0.5-0.5-0.5s-0.5 0.224-0.5 0.5v16.293l-5.146-5.146c-0.195-0.195-0.512-0.195-0.707 0-0.098 0.098-0.146 0.226-0.146 0.354s0.049 0.256 0.146 0.354l6 6c0.195 0.195 0.512 0.195 0.707 0z" />
    </svg>
);

const Root = () => {
    const [OverlayProvider, OverlayContext] = useOverlay();

    return (
        <App $fontSize={14} $backgroundColor="white">
            <OverlayProvider
                element={OverlayElement}
                props={{
                    fontFamily: "Arial",
                }}
            />
            <h1>Tripetto Collector Fabric</h1>
            <div>
                <h2>InputFabric</h2>
                <InputFabric
                    type="text"
                    placeholder="Background only"
                    autoComplete="name"
                    styles={{
                        backgroundColor: "000",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background only + scale"
                    styles={{
                        backgroundColor: "#000",
                        borderColor: "transparent",
                        errorColor: "red",
                        scale: 2,
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background only + error"
                    error={true}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background only + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background only + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + text color"
                    styles={{
                        backgroundColor: "black",
                        textColor: "green",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + text color + error"
                    error={true}
                    styles={{
                        backgroundColor: "black",
                        textColor: "green",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + text color + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "black",
                        textColor: "green",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + text color + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "black",
                        textColor: "green",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Border only"
                    styles={{ backgroundColor: "transparent", borderColor: "black", errorColor: "red" }}
                />
                <InputFabric
                    type="text"
                    placeholder="Border only + error"
                    error={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Border only + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Border only + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + border"
                    styles={{ backgroundColor: "green", borderColor: "blue", errorColor: "red" }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + border + error"
                    error={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + border + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + border + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + custom border"
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        borderSize: 3,
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + custom border + error"
                    error={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        borderSize: 3,
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + custom border + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        borderSize: 3,
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Background + custom border + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        borderSize: 3,
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Transparent"
                    styles={{ backgroundColor: "transparent", borderColor: "transparent", errorColor: "red" }}
                />
                <InputFabric
                    type="text"
                    placeholder="Transparent + error"
                    error={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Transparent + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <InputFabric
                    type="text"
                    placeholder="Transparent + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
            </div>
            <div>
                <h2>TextFabric</h2>
                <TextFabric
                    autoComplete="name"
                    suggestions={["Option 1", "Option 2", "Option 3"]}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
            </div>
            <div>
                <h2>EmailFabric</h2>
                <EmailFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
            </div>
            <div>
                <h2>URLFabric</h2>
                <URLFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
            </div>
            <div>
                <h2>PhoneNumberFabric</h2>
                <PhoneNumberFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
            </div>
            <div>
                <h2>PasswordFabric</h2>
                <PasswordFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
            </div>
            <div>
                <h2>NumberFabric</h2>
                <NumberFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                    onChange={(value: string) => {
                        console.log(`Value: ${value}`);
                    }}
                />
                <NumberFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                    onChange={(value: string) => {
                        console.log(`Value: ${value}`);
                    }}
                    precision={2}
                />
            </div>
            <div>
                <h2>DateTimeFabric</h2>
                <DateTimeFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        roundness: 0,
                        errorColor: "red",
                    }}
                    onChange={(value: Date) => {
                        console.log(`Value: ${value}`);
                    }}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <DateTimeFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                    mode={"date"}
                    placeholder="Please enter a date"
                    onChange={(value: Date) => {
                        console.log(`Value: ${value}`);
                    }}
                />
                <DateTimeFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                    mode={"datetime"}
                    error={true}
                    placeholder="Please enter a date + time"
                    onChange={(value: Date) => {
                        console.log(`Value: ${value}`);
                    }}
                />
            </div>
            <div>
                <h2>TextareaFabric</h2>
                <TextareaFabric
                    placeholder="Background only"
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <TextareaFabric
                    placeholder="Background only + autosize"
                    autoSize={true}
                    error={true}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <TextareaFabric
                    placeholder="Background only + scale"
                    autoSize={true}
                    error={true}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                        scale: 2,
                    }}
                />
            </div>
            <div>
                <h2>DropdownFabric</h2>
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background only"
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[
                        {
                            name: "Option option option option option option option option option option option option",
                            id: "option",
                            value: "option",
                        },
                    ]}
                    placeholder="Background only + scale"
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                        scale: 2,
                    }}
                />
                <DropdownFabric
                    options={[
                        {
                            name: "Option option option option option option option option option option option option",
                            id: "option",
                            value: "option",
                        },
                    ]}
                    placeholder="Background only + error"
                    error={true}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background only + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background only + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + text color"
                    styles={{
                        backgroundColor: "black",
                        textColor: "green",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + text color + error"
                    error={true}
                    styles={{
                        backgroundColor: "black",
                        textColor: "green",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + text color + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "black",
                        textColor: "green",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + text color + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "black",
                        textColor: "green",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Border only"
                    styles={{ backgroundColor: "transparent", borderColor: "black", errorColor: "red" }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Border only + error"
                    error={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Border only + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Border only + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + border"
                    styles={{ backgroundColor: "green", borderColor: "blue", errorColor: "red" }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + border + error"
                    error={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + border + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + border + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + custom border"
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        borderSize: 3,
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + custom border + error"
                    error={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        borderSize: 3,
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + custom border + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        borderSize: 3,
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Background + custom border + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        borderSize: 3,
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Transparent"
                    styles={{ backgroundColor: "transparent", borderColor: "transparent", errorColor: "red" }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Transparent + error"
                    error={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Transparent + disabled"
                    disabled={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <DropdownFabric
                    options={[{ name: "Option", id: "option", value: "option" }]}
                    placeholder="Transparent + readonly"
                    readOnly={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
            </div>
            <div>
                <h2>MultiSelectFabric</h2>
                <MultiSelectFabric
                    id="1"
                    overlay={OverlayContext}
                    options={[
                        {
                            name: "Option 1",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget eros vitae mi aliquam dignissim.",
                            id: "option1",
                            value: {
                                value: true,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 2",
                            id: "option2",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 3",
                            id: "option3",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 4",
                            id: "option4",
                            value: {
                                value: true,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 5",
                            id: "option5",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                    ]}
                    placeholder="Background only"
                    maxSelected={2}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <MultiSelectFabric
                    id="1"
                    overlay={OverlayContext}
                    options={[]}
                    placeholder="Background only"
                    maxSelected={2}
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <MultiSelectFabric
                    id="2"
                    overlay={OverlayContext}
                    options={[
                        {
                            name: "Option 1",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget eros vitae mi aliquam dignissim.",
                            id: "option1",
                            value: {
                                value: true,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 2",
                            id: "option2",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 3",
                            id: "option3",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 4",
                            id: "option4",
                            value: {
                                value: true,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 5",
                            id: "option5",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 6",
                            id: "option6",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 7",
                            id: "option7",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 8",
                            id: "option8",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                    ]}
                    placeholder="Background only"
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                />
                <MultiSelectFabric
                    id="3"
                    overlay={OverlayContext}
                    options={[]}
                    placeholder="Background only + scale"
                    styles={{
                        backgroundColor: "black",
                        borderColor: "transparent",
                        errorColor: "red",
                        scale: 2,
                    }}
                />
                <MultiSelectFabric
                    id="4"
                    overlay={OverlayContext}
                    options={[
                        {
                            name: "Option 1",
                            id: "option1",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 2",
                            id: "option2",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                    ]}
                    placeholder="Border only"
                    styles={{ backgroundColor: "transparent", borderColor: "black", errorColor: "red" }}
                />
                <MultiSelectFabric
                    id="5"
                    overlay={OverlayContext}
                    options={[
                        {
                            name: "Option 1",
                            id: "option1",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 2",
                            id: "option2",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                    ]}
                    placeholder="Border only + error"
                    error={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "black",
                        errorColor: "red",
                    }}
                />
                <MultiSelectFabric
                    id="6"
                    overlay={OverlayContext}
                    options={[
                        {
                            name: "Option 1",
                            id: "option1",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 2",
                            id: "option2",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                    ]}
                    placeholder="Background + border"
                    styles={{ backgroundColor: "green", borderColor: "blue", errorColor: "red" }}
                />
                <MultiSelectFabric
                    id="7"
                    overlay={OverlayContext}
                    options={[
                        {
                            name: "Option 1",
                            id: "option1",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 2",
                            id: "option2",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                    ]}
                    placeholder="Background + border + error"
                    error={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        errorColor: "red",
                    }}
                />
                <MultiSelectFabric
                    id="8"
                    overlay={OverlayContext}
                    options={[
                        {
                            name: "Option 1",
                            id: "option1",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 2",
                            id: "option2",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                    ]}
                    placeholder="Background + custom border"
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        borderSize: 3,
                        errorColor: "red",
                    }}
                />
                <MultiSelectFabric
                    id="9"
                    overlay={OverlayContext}
                    options={[
                        {
                            name: "Option 1",
                            id: "option1",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                        {
                            name: "Option 2",
                            id: "option2",
                            value: {
                                value: false,
                                isFrozen: false,
                                isLocked: false,
                                confirm: () => noop(),
                            },
                        },
                    ]}
                    placeholder="Transparent"
                    styles={{ backgroundColor: "transparent", borderColor: "transparent", errorColor: "red" }}
                />
            </div>
            <div>
                <h2>CheckboxFabric</h2>
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    description="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                        roundness: 0,
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        textColor: "green",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    value={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "blue",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    error={true}
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    error={true}
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "blue",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    error={true}
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    required={true}
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    readOnly={true}
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit"
                    disabled={true}
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet urna sed dui viverra porta quis id erat. Vivamus faucibus dignissim condimentum. Nullam nisi ipsum, eleifend egestas urna pulvinar, varius condimentum mi. Nulla consequat metus nec arcu convallis, a auctor magna suscipit."
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                        errorColor: "red",
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet urna sed dui viverra porta quis id erat. Vivamus faucibus dignissim condimentum. Nullam nisi ipsum, eleifend egestas urna pulvinar, varius condimentum mi. Nulla consequat metus nec arcu convallis, a auctor magna suscipit."
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "blue",
                        borderSize: 4,
                        textColor: "black",
                        errorColor: "red",
                        scale: 2,
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet urna sed dui viverra porta quis id erat. Vivamus faucibus dignissim condimentum. Nullam nisi ipsum, eleifend egestas urna pulvinar, varius condimentum mi. Nulla consequat metus nec arcu convallis, a auctor magna suscipit."
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "blue",
                        borderSize: 4,
                        textColor: "black",
                        errorColor: "red",
                        scale: 2,
                    }}
                />
                <CheckboxFabric
                    label="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sit amet urna sed dui viverra porta quis id erat. Vivamus faucibus dignissim condimentum. Nullam nisi ipsum, eleifend egestas urna pulvinar, varius condimentum mi. Nulla consequat metus nec arcu convallis, a auctor magna suscipit."
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "blue",
                        borderSize: 4,
                        textColor: "black",
                        errorColor: "red",
                        scale: 2,
                    }}
                />
            </div>
            <div>
                <h2>CheckboxesFabric</h2>
                <CheckboxesFabric
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                        errorColor: "red",
                    }}
                    checkboxes={[
                        {
                            id: "1",
                            label: "Checkbox 1",
                        },
                        {
                            id: "2",
                            label: "Checkbox 2",
                        },
                        {
                            id: "3",
                            label: "Checkbox 3",
                        },
                    ]}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <CheckboxesFabric
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                        errorColor: "red",
                    }}
                    checkboxes={[
                        {
                            id: "1",
                            readOnly: true,
                            label: "Checkbox 1",
                            value: true,
                        },
                        {
                            id: "2",
                            label: "Checkbox 2",
                            value: true,
                        },
                        {
                            id: "3",
                            label: "Checkbox 3",
                            value: true,
                        },
                    ]}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
            </div>
            <div>
                <h2>RadiobuttonsFabric</h2>
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                    }}
                    value="2"
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        textColor: "black",
                    }}
                    allowUnselect={true}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "transparent",
                        textColor: "green",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "blue",
                        textColor: "black",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        textColor: "black",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "blue",
                        textColor: "black",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "green",
                        borderColor: "blue",
                        textColor: "black",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                            description: "Explanation of option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    readOnly={true}
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    disabled={true}
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                    }}
                    value="1"
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        textColor: "black",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
                <RadiobuttonsFabric
                    styles={{
                        backgroundColor: "transparent",
                        borderColor: "blue",
                        borderSize: 4,
                        textColor: "black",
                        scale: 2,
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Option 1",
                        },
                        {
                            id: "2",
                            name: "Option 2",
                        },
                        {
                            id: "3",
                            name: "Option 3",
                        },
                    ]}
                />
            </div>
            <div>
                <h2>YesNoFabric</h2>
                <YesNoFabric
                    styles={{
                        yesColor: "green",
                        noColor: "red",
                    }}
                    yes={{
                        label: "Yes",
                        icon: "yes",
                    }}
                    no={{
                        label: "No",
                        icon: "no",
                    }}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <YesNoFabric
                    styles={{
                        yesColor: "green",
                        noColor: "red",
                        margin: 16,
                        alignment: "vertical",
                    }}
                    yes={{
                        label: "Yes",
                        icon: "yes",
                    }}
                    no={{
                        label: "No",
                        icon: "no",
                    }}
                />
                <YesNoFabric
                    styles={{
                        yesColor: "green",
                        noColor: "red",
                        scale: 2,
                    }}
                    yes={{
                        label: "Yes",
                    }}
                    no={{
                        label: "No",
                    }}
                />
                <YesNoFabric
                    styles={{
                        yesColor: "green",
                        noColor: "red",
                        scale: 2,
                    }}
                    yes={{
                        label: "Yes",
                        icon: "yes",
                    }}
                    no={{
                        label: "No",
                        icon: "no",
                    }}
                    disabled={true}
                />
                <YesNoFabric
                    styles={{
                        yesColor: "green",
                        noColor: "red",
                        scale: 2,
                    }}
                    yes={{
                        label: "Yes",
                        icon: "yes",
                        color: "purple",
                    }}
                    no={{
                        label: "No",
                        icon: "no",
                        color: "black",
                    }}
                    readOnly={true}
                />
            </div>
            <div>
                <h2>ScaleFabric</h2>
                <ScaleFabric
                    styles={{
                        color: "green",
                    }}
                    options={{
                        from: 0,
                        to: 10,
                    }}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <ScaleFabric
                    styles={{
                        color: "green",
                    }}
                    options={{
                        from: 0,
                        to: 0,
                    }}
                    labelLeft="Left"
                    labelRight="Right"
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <ScaleFabric
                    styles={{
                        color: "green",
                        labelColor: "red",
                    }}
                    options={{
                        from: -5,
                        to: 5,
                        stepSize: 2,
                    }}
                    labelLeft="Left"
                    labelRight="Right"
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <ScaleFabric
                    styles={{
                        color: "green",
                    }}
                    options={[
                        {
                            id: "1",
                            name: "Good",
                        },
                        {
                            id: "2",
                            name: "Medium",
                            value: "medium",
                        },
                        {
                            id: "3",
                            name: "Bad",
                            value: "bad",
                        },
                    ]}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <ScaleFabric
                    styles={{
                        color: "green",
                    }}
                    options={[
                        {
                            id: "1",
                            name: "Bad",
                        },
                        {
                            id: "2",
                            name: "Medium",
                            value: "medium",
                        },
                        {
                            id: "3",
                            name: "Good",
                            value: "good",
                        },
                        {
                            id: "4",
                            name: "Very good",
                        },
                        {
                            id: "5",
                            name: "Very very very very very very very very very good",
                        },
                    ]}
                    labelLeft="Left"
                    labelCenter="Center"
                    labelRight="Right"
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
            </div>
            <div>
                <h2>RatingFabric</h2>
                <RatingFabric
                    styles={{
                        color: "green",
                    }}
                    steps={10}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <RatingFabric
                    styles={{
                        color: "green",
                    }}
                    shape="hearts"
                    steps={5}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <RatingFabric
                    styles={{
                        color: "green",
                    }}
                    shape="thumbs-up"
                    steps={5}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <RatingFabric
                    styles={{
                        color: "green",
                    }}
                    shape="thumbs-down"
                    steps={5}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <RatingFabric
                    styles={{
                        color: "green",
                    }}
                    shape="persons"
                    steps={5}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <RatingFabric
                    styles={{
                        color: "blue",
                        scale: 2,
                    }}
                    shape="stars"
                    steps={10}
                    value={5}
                    showLabels={true}
                />
                <RatingFabric
                    styles={{
                        color: "red",
                    }}
                    shape="thumbs-up"
                    steps={10}
                    disabled={true}
                />
                <RatingFabric
                    styles={{
                        color: "purple",
                    }}
                    shape="thumbs-down"
                    steps={10}
                    readOnly={true}
                />
                <RatingFabric
                    styles={{
                        color: "blue",
                        scale: 2,
                    }}
                    shape="thumbs-up"
                    steps={10}
                    value={5}
                    showLabels={true}
                />
                <RatingFabric
                    styles={{
                        color: "blue",
                        scale: 2,
                    }}
                    shape="thumbs-down"
                    steps={10}
                    value={5}
                    showLabels={true}
                />
            </div>
            <div>
                <h2>RankingFabric</h2>
                <RankingFabric
                    styles={{
                        color: "green",
                    }}
                    options={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus fringilla diam in congue.",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus fringilla diam in congue. Nullam semper, lectus vehicula porttitor ullamcorper, ex nisi aliquam ligula, eu malesuada urna arcu eu mi.",
                        },
                        {
                            id: "4",
                            name: "Link",
                        },
                    ]}
                />
                <RankingFabric
                    styles={{
                        color: "green",
                        scale: 2,
                    }}
                    slots={[
                        {
                            key: "1",
                            reference: "",
                            isLocked: false,
                            isFrozen: false,
                            set: (v, r) => {
                                console.log("Set");
                            },
                        },
                    ]}
                    options={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus fringilla diam in congue.",
                            description:
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus fringilla diam in congue. Nullam semper, lectus vehicula porttitor ullamcorper, ex nisi aliquam ligula, eu malesuada urna arcu eu mi.",
                        },
                        {
                            id: "4",
                            name: "Link",
                        },
                    ]}
                />
            </div>
            <div>
                <h2>MultipleChoiceFabric</h2>
                <MultipleChoiceFabric
                    styles={{
                        color: "green",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Button 3",
                            description: "Button description",
                        },
                        {
                            id: "4",
                            name: "Link",
                            url: "https://tripetto.com",
                            color: "red",
                        },
                    ]}
                />
                <MultipleChoiceFabric
                    styles={{
                        color: "green",
                    }}
                    buttons={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Button 3",
                            description: "Button description",
                        },
                        {
                            id: "4",
                            name: "Link",
                            url: "https://tripetto.com",
                        },
                    ]}
                    disabled={true}
                />
                <h3>Equal</h3>
                <MultipleChoiceFabric
                    styles={{
                        color: "green",
                    }}
                    alignment="equal"
                    buttons={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Button 3",
                            description: "Button description",
                        },
                        {
                            id: "4",
                            name: "Link  fsfsd fsd fds fasd fasd fasd fasdLink  fsfsd fsd fds fasd fasd fasd fasd",
                            url: "https://tripetto.com",
                            description: "Opens in a new window",
                        },
                        {
                            id: "5",
                            name: "Button with a long text dfsfds fds fsdsfd afdas asfdfsd afsd",
                        },
                    ]}
                />
                <h3>Full</h3>
                <MultipleChoiceFabric
                    styles={{
                        color: "green",
                    }}
                    alignment="full"
                    buttons={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Button 3",
                            description: "Button description",
                        },
                        {
                            id: "4",
                            name: "Link",
                            url: "https://tripetto.com",
                            description: "Opens in a new window",
                        },
                        {
                            id: "5",
                            name: "Button with a long text fsd fsd sfda fsd sfda fsda sdfa fsd sfd fsda fsda fsdasfadsfadsafdsfadsdfsfsf",
                        },
                    ]}
                />
                <h3>Columns</h3>
                <MultipleChoiceFabric
                    styles={{
                        color: "green",
                    }}
                    alignment="columns"
                    buttons={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Button 3",
                            description: "Button description",
                        },
                        {
                            id: "4",
                            name: "Link",
                            url: "https://tripetto.com",
                            description: "Opens in a new window",
                        },
                        {
                            id: "5",
                            name: "Button with a long text fds fds fsda sdfa sfda fsd",
                        },
                        {
                            id: "6",
                            name: "Button 6",
                        },
                        {
                            id: "7",
                            name: "Button 7",
                        },
                    ]}
                />
                <h3>Horizontal</h3>
                <MultipleChoiceFabric
                    styles={{
                        color: "green",
                    }}
                    alignment="horizontal"
                    buttons={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Button 3",
                            description: "Button description",
                        },
                        {
                            id: "4",
                            name: "Link",
                            url: "https://tripetto.com",
                            description: "Opens in a new window",
                        },
                        {
                            id: "5",
                            name: "Button with a long text",
                        },
                    ]}
                />
            </div>
            <div>
                <h2>PictureChoiceFabric</h2>
                <PictureChoiceFabric
                    styles={{
                        color: "green",
                    }}
                    options={[
                        {
                            id: "0",
                            emoji: "😀",
                        },
                        {
                            id: "1",
                            emoji: "😍",
                            name: "Button 1",
                            nameVisible: true,
                        },
                        {
                            id: "2",
                            name: "Button 2",
                            image:
                                "data:image/svg+xml;base64," +
                                avataaars(
                                    {
                                        skin: "Light",
                                        eyeBrow: "UnibrowNatural",
                                        eyes: "Hearts",
                                        mouth: "Smile",
                                        top: "ShortHairTheCaesarSidePart",
                                        accessories: "Round",
                                        hairColor: "SilverGray",
                                        hatColor: "Blue03",
                                        facialHair: "BeardMedium",
                                        facialHairColor: "Red",
                                        clothing: "ShirtVNeck",
                                        clothingFabric: "Pink",
                                        clothingGraphic: "Skull",
                                    },
                                    "base64"
                                ),
                        },
                        {
                            id: "3",
                            name: "Button 3",
                            description: "Button description",
                        },
                        {
                            id: "4",
                            name: "Link",
                            url: "https://tripetto.com",
                        },
                    ]}
                />
                <PictureChoiceFabric
                    styles={{
                        color: "green",
                    }}
                    options={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Button 3",
                            description: "Button description",
                        },
                        {
                            id: "4",
                            name: "Link",
                            url: "https://tripetto.com",
                        },
                    ]}
                    disabled={true}
                />
                <PictureChoiceFabric
                    styles={{
                        color: "green",
                    }}
                    size="large"
                    options={[
                        {
                            id: "1",
                            name: "Button 1",
                        },
                        {
                            id: "2",
                            name: "Button 2",
                        },
                        {
                            id: "3",
                            name: "Button 3",
                            description: "Button description",
                        },
                        {
                            id: "4",
                            name: "Link",
                            url: "https://tripetto.com",
                            description: "Opens in a new window",
                        },
                        {
                            id: "5",
                            name: "Button with a long text",
                        },
                    ]}
                    readOnly={true}
                />
            </div>
            <div>
                <h2>MatrixFabric</h2>
                <MatrixFabric
                    styles={{
                        textColor: "green",
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        errorColor: "red",
                        scale: 1,
                    }}
                    columns={[
                        {
                            id: "1",
                            name: "Column 1",
                        },
                        {
                            id: "2",
                            name: "Column 2",
                        },
                        {
                            id: "3",
                            name: "Column 3",
                        },
                        {
                            id: "4",
                            name: "Column 4",
                        },
                        {
                            id: "5",
                            name: "Column 5",
                        },
                        {
                            id: "6",
                            name: "Column 6",
                        },
                    ]}
                    rows={[
                        {
                            id: "1",
                            label: "Row 1",
                            explanation: "Explanation row 1",
                            value: "1",
                        },
                        {
                            id: "2",
                            label: "Row 2",
                        },
                        {
                            id: "3",
                            label: "Row 3",
                        },
                    ]}
                    onSubmit={() => console.log("Submit!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <MatrixFabric
                    styles={{
                        textColor: "blue",
                        backgroundColor: "blue",
                        borderColor: "blue",
                        errorColor: "red",
                        scale: 1,
                    }}
                    columns={[
                        {
                            id: "1",
                            name: "Column 1",
                        },
                        {
                            id: "2",
                            name: "Column 2",
                        },
                        {
                            id: "3",
                            name: "Column 3",
                        },
                    ]}
                    rows={[
                        {
                            id: "1",
                            label: "Row 1",
                            explanation: "Explanation row 1",
                            value: "1",
                        },
                        {
                            id: "2",
                            label: "Row 2",
                        },
                        {
                            id: "3",
                            label: "Row 3",
                        },
                    ]}
                />
                <MatrixFabric
                    styles={{
                        textColor: "green",
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                    columns={[
                        {
                            id: "1",
                            name: "Column 1",
                        },
                        {
                            id: "2",
                            name: "Column 2",
                        },
                    ]}
                    rows={[
                        {
                            id: "1",
                            label: "Row 1",
                            required: true,
                        },
                        {
                            id: "2",
                            label: "Row 2",
                        },
                    ]}
                />
                <MatrixFabric
                    styles={{
                        textColor: "green",
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                    required={true}
                    columns={[
                        {
                            id: "1",
                            name: "Column 1",
                        },
                        {
                            id: "2",
                            name: "Column 2",
                        },
                    ]}
                    rows={[
                        {
                            id: "1",
                            label: "Row 1",
                        },
                        {
                            id: "2",
                            label: "Row 2",
                        },
                    ]}
                />
                <MatrixFabric
                    styles={{
                        textColor: "green",
                        backgroundColor: "blue",
                        borderColor: "transparent",
                        errorColor: "red",
                    }}
                    required={true}
                    columns={[
                        {
                            id: "1",
                            name: "",
                        },
                        {
                            id: "2",
                            name: "",
                        },
                    ]}
                    rows={[
                        {
                            id: "1",
                            label: "",
                        },
                        {
                            id: "2",
                            label: "",
                        },
                    ]}
                />
            </div>
            <div>
                <h2>FileFabric</h2>
                <FileFabric
                    styles={{
                        backgroundColor: "green",
                        borderColor: "red",
                        errorColor: "red",
                    }}
                    controller={{
                        isImage: false,
                        upload: (files: FileList, service: IFileService | undefined, onProgress: (percent: number) => void) =>
                            new Promise((resolve: () => void) => {
                                let percentage = 0;
                                const interval = setInterval(() => {
                                    percentage++;

                                    onProgress(percentage);

                                    if (percentage === 100) {
                                        clearInterval(interval);

                                        resolve();
                                    }
                                }, 200);

                                console.dir(files);
                            }),
                        download: () => new Promise<string>((resolve: (data: string) => void) => setTimeout(() => resolve("data"), 2000)),
                        delete: () => new Promise((resolve: () => void) => setTimeout(() => resolve(), 2000)),
                        fileSlot: {
                            hasValue: false,
                            string: "",
                            reference: "",
                            isFrozen: false,
                            isLocked: false,
                        },
                        limit: 10,
                        allowedExtensions: [".jpg", ".gif"],
                    }}
                    labels={(id, message) => `Label<${id}:${message}>`}
                />
                <FileFabric
                    styles={{
                        backgroundColor: "green",
                        borderColor: "red",
                        errorColor: "red",
                    }}
                    controller={{
                        isImage: false,
                        upload: (files: FileList, service: IFileService | undefined, onProgress: (percent: number) => void) =>
                            new Promise((resolve: () => void) => {
                                let percentage = 0;
                                const interval = setInterval(() => {
                                    percentage++;

                                    onProgress(percentage);

                                    if (percentage === 100) {
                                        clearInterval(interval);

                                        resolve();
                                    }
                                }, 200);

                                console.dir(files);
                            }),
                        download: () => new Promise<string>((resolve: (data: string) => void) => setTimeout(() => resolve("data"), 2000)),
                        delete: () => new Promise((resolve: () => void) => setTimeout(() => resolve(), 2000)),
                        fileSlot: {
                            hasValue: true,
                            string: "file.txt",
                            reference: "12345",
                            isFrozen: false,
                            isLocked: false,
                        },
                        limit: 10,
                        allowedExtensions: [".jpg", ".gif"],
                    }}
                    labels={(id, message) => `Label<${id}:${message}>`}
                />
                <FileFabric
                    styles={{
                        backgroundColor: "green",
                        borderColor: "red",
                        errorColor: "red",
                    }}
                    controller={{
                        isImage: true,
                        upload: (files: FileList, service: IFileService | undefined, onProgress: (percent: number) => void) =>
                            new Promise((resolve: () => void) => {
                                let percentage = 0;
                                const interval = setInterval(() => {
                                    percentage++;

                                    onProgress(percentage);

                                    if (percentage === 100) {
                                        clearInterval(interval);

                                        resolve();
                                    }
                                }, 200);

                                console.dir(files);
                            }),
                        download: () =>
                            new Promise<string>((resolve: (data: string) => void) =>
                                setTimeout(() => resolve("https://tripetto.com/images/tripetto-logo.svg"), 2000)
                            ),
                        delete: () => new Promise((resolve: () => void) => setTimeout(() => resolve(), 2000)),
                        fileSlot: {
                            hasValue: true,
                            string: "image.png",
                            reference: "https://tripetto.com/images/tripetto-logo.svg",
                            isFrozen: false,
                            isLocked: false,
                        },
                        limit: 10,
                        allowedExtensions: [".jpg", ".gif"],
                    }}
                    labels={(id, message) => `Label<${id}:${message}>`}
                />
                <FileFabric
                    styles={{ backgroundColor: "transparent", borderColor: "black", errorColor: "red" }}
                    controller={{
                        isImage: false,
                        upload: (files: FileList) =>
                            new Promise(
                                (
                                    resolve: () => void,
                                    reject: (error: "invalid-amount" | "invalid-extension" | "invalid-size" | string) => void
                                ) => {
                                    reject("invalid-amount");
                                }
                            ),
                        download: () => new Promise<string>((resolve: (data: string) => void) => resolve("data")),
                        delete: () => new Promise((resolve: () => void, reject: () => void) => setTimeout(() => reject(), 2000)),
                        fileSlot: {
                            hasValue: false,
                            string: "",
                            reference: "",
                            isFrozen: false,
                            isLocked: false,
                        },
                        limit: 10,
                        allowedExtensions: [".jpg", ".gif"],
                    }}
                    labels={(id, message) => `Label<${id}:${message}>`}
                />
                <FileFabric
                    styles={{ backgroundColor: "transparent", borderColor: "black", errorColor: "red" }}
                    controller={{
                        isImage: false,
                        upload: (files: FileList) =>
                            new Promise(
                                (
                                    resolve: () => void,
                                    reject: (error: "invalid-amount" | "invalid-extension" | "invalid-size" | string) => void
                                ) => {
                                    reject("invalid-extension");
                                }
                            ),
                        download: () => new Promise<string>((resolve: (data: string) => void) => resolve("data")),
                        delete: () => new Promise((resolve: () => void) => setTimeout(() => resolve(), 2000)),
                        fileSlot: {
                            hasValue: false,
                            string: "",
                            reference: "",
                            isFrozen: false,
                            isLocked: false,
                        },
                        limit: 10,
                        allowedExtensions: [".jpg", ".gif"],
                    }}
                    labels={(id, message) => `Label<${id}:${message}>`}
                />
                <FileFabric
                    styles={{ backgroundColor: "transparent", borderColor: "black", errorColor: "red" }}
                    controller={{
                        isImage: false,
                        upload: (files: FileList) =>
                            new Promise(
                                (
                                    resolve: () => void,
                                    reject: (error: "invalid-amount" | "invalid-extension" | "invalid-size" | string) => void
                                ) => {
                                    reject("invalid-size");
                                }
                            ),
                        download: () => new Promise<string>((resolve: (data: string) => void) => resolve("data")),
                        delete: () => new Promise((resolve: () => void) => setTimeout(() => resolve(), 2000)),
                        fileSlot: {
                            hasValue: false,
                            string: "",
                            reference: "",
                            isFrozen: false,
                            isLocked: false,
                        },
                        limit: 10,
                        allowedExtensions: [".jpg", ".gif"],
                    }}
                    labels={(id, message) => `Label<${id}:${message}>`}
                />
                <FileFabric
                    styles={{ backgroundColor: "transparent", borderColor: "black", errorColor: "red" }}
                    controller={{
                        isImage: false,
                        upload: (files: FileList) =>
                            new Promise(
                                (
                                    resolve: () => void,
                                    reject: (error: "invalid-amount" | "invalid-extension" | "invalid-size" | string) => void
                                ) => {
                                    reject("invalid-size");
                                }
                            ),
                        download: () => new Promise<string>((resolve: (data: string) => void) => resolve("data")),
                        delete: () => new Promise((resolve: () => void) => setTimeout(() => resolve(), 2000)),
                        fileSlot: {
                            hasValue: false,
                            string: "",
                            reference: "",
                            isFrozen: false,
                            isLocked: false,
                        },
                        limit: 10,
                        allowedExtensions: [],
                    }}
                    labels={(id, message) => `Label<${id}:${message}>`}
                />
                <FileFabric
                    styles={{ backgroundColor: "transparent", borderColor: "black", errorColor: "red" }}
                    controller={{
                        isImage: false,
                        upload: (files: FileList) =>
                            new Promise(
                                (
                                    resolve: () => void,
                                    reject: (error: "invalid-amount" | "invalid-extension" | "invalid-size" | string) => void
                                ) => {
                                    reject("invalid-size");
                                }
                            ),
                        download: () => new Promise<string>((resolve: (data: string) => void) => resolve("data")),
                        delete: () => new Promise((resolve: () => void) => setTimeout(() => resolve(), 2000)),
                        fileSlot: {
                            hasValue: false,
                            string: "",
                            reference: "",
                            isFrozen: false,
                            isLocked: false,
                        },
                        limit: 10,
                        allowedExtensions: [],
                    }}
                    labels={(id, message) => `Label<${id}:${message}>`}
                    error={true}
                />
            </div>
            <div>
                <h2>SignatureFabric</h2>
                <SignatureFabric
                    styles={{
                        backgroundColor: "green",
                        borderColor: "red",
                        errorColor: "red",
                    }}
                    color="darkblue"
                    labels={(id, message) => `Label<${id}:${message}>`}
                    controller={{
                        upload: (files: FileList, service: ISignatureService | undefined, onProgress: (percent: number) => void) =>
                            new Promise((resolve: () => void) => {
                                let percentage = 0;
                                const interval = setInterval(() => {
                                    percentage++;

                                    onProgress(percentage);

                                    if (percentage === 100) {
                                        clearInterval(interval);

                                        resolve();
                                    }
                                }, 20);

                                console.dir(files);
                            }),
                        download: () => new Promise<string>((resolve: (data: string) => void) => setTimeout(() => resolve("data"), 2000)),
                        delete: () => new Promise((resolve: () => void) => setTimeout(() => resolve(), 2000)),
                        signatureSlot: {
                            hasValue: false,
                            string: "",
                            reference: "",
                            isFrozen: false,
                            isLocked: false,
                            time: undefined,
                        },
                    }}
                />
                <SignatureFabric
                    styles={{ backgroundColor: "transparent", borderColor: "black", errorColor: "red" }}
                    labels={(id, message) => `Label<${id}:${message}>`}
                    size="sm"
                    controller={{
                        upload: (files: FileList, service: ISignatureService | undefined, onProgress: (percent: number) => void) =>
                            new Promise((resolve: () => void) => {
                                let percentage = 0;
                                const interval = setInterval(() => {
                                    percentage++;

                                    onProgress(percentage);

                                    if (percentage === 100) {
                                        clearInterval(interval);

                                        resolve();
                                    }
                                }, 20);

                                console.dir(files);
                            }),
                        download: () => new Promise<string>((resolve: (data: string) => void) => setTimeout(() => resolve("data"), 2000)),
                        delete: () => new Promise((resolve: () => void) => setTimeout(() => resolve(), 2000)),
                        signatureSlot: {
                            hasValue: true,
                            string: "",
                            reference: "",
                            isFrozen: false,
                            isLocked: false,
                            time: 1,
                        },
                    }}
                />
            </div>
            <div>
                <h2>ButtonFabric</h2>
                <ButtonFabric
                    label="Button"
                    styles={{ baseColor: "#007bff" }}
                    onClick={() => alert(`Ok`)}
                    onTab={() => console.log("Tab!")}
                    onCancel={() => console.log("Cancel!")}
                />
                <ButtonFabric
                    label="Button"
                    styles={{ baseColor: "#007bff", mode: "outline" }}
                    disabled={false}
                    onClick={() => alert(`Ok`)}
                />
                <ButtonFabric label="Button" styles={{ baseColor: "#007bff" }} disabled={true} onClick={() => alert(`Ok`)} />
                <ButtonFabric
                    label="Button"
                    styles={{ baseColor: "#007bff", mode: "outline" }}
                    disabled={true}
                    onClick={() => alert(`Ok`)}
                />
                <ButtonFabric label="Button" styles={{ baseColor: "#007bff" }} />
                <ButtonFabric label="Button" styles={{ baseColor: "#007bff", mode: "outline", group: "start" }} />
                <ButtonFabric
                    label="Button"
                    styles={{ baseColor: "#007bff", textColor: "red", group: "middle" }}
                    onClick={() => alert(`Ok`)}
                />
                <ButtonFabric
                    label="Button"
                    styles={{ baseColor: "#007bff", textColor: "red", group: "end", mode: "outline" }}
                    onClick={() => alert(`Ok`)}
                />
                <ButtonFabric label="Button" styles={{ baseColor: "#000" }} onClick={() => alert(`Ok`)} />
                <ButtonFabric label="Button" styles={{ baseColor: "#000", mode: "outline" }} onClick={() => alert(`Ok`)} />
                <ButtonFabric label="Button" styles={{ baseColor: "#fff" }} onClick={() => alert(`Ok`)} />
                <ButtonFabric label="Button" styles={{ baseColor: "#fff", mode: "outline" }} onClick={() => alert(`Ok`)} />
                <ButtonFabric label="Button" styles={{ baseColor: "#007bff" }} icon={TestIcon} onClick={() => alert(`Ok`)} />
                <ButtonFabric label="Button" styles={{ baseColor: "#fff", textColor: "red" }} icon={TestIcon} onClick={() => alert(`Ok`)} />
                <ButtonFabric label="Button" styles={{ baseColor: "#000" }} icon={TestIcon} onClick={() => alert(`Ok`)} />
                <ButtonFabric label="Button" styles={{ baseColor: "#000", textColor: "red" }} icon={TestIcon} onClick={() => alert(`Ok`)} />
                <ButtonFabric
                    label="Button"
                    styles={{ baseColor: "#007bff" }}
                    icon={TestIcon}
                    iconPosition="right"
                    onClick={() => alert(`Ok`)}
                />
                <ButtonFabric
                    label="Hyperlink (self)"
                    styles={{ baseColor: "#007bff" }}
                    hyperlink={{
                        url: "https://tripetto.com",
                        target: "self",
                    }}
                />
                <ButtonFabric
                    label="Hyperlink (blank) with a long label"
                    description="And a description with a long line of text"
                    styles={{ baseColor: "#007bff" }}
                    hyperlink={{
                        url: "https://tripetto.com",
                        target: "blank",
                    }}
                />
                <ButtonFabric styles={{ baseColor: "#007bff" }} icon={TestIcon} onClick={() => alert(`Ok`)} />
                <ButtonFabric styles={{ baseColor: "#007bff" }} icon={TestIcon} iconPosition="right" onClick={() => alert(`Ok`)} />
                <ButtonFabric
                    styles={{ baseColor: "#007bff" }}
                    image={
                        "data:image/svg+xml;base64," +
                        avataaars(
                            {
                                skin: "Light",
                                eyeBrow: "UnibrowNatural",
                                eyes: "Hearts",
                                mouth: "Smile",
                                top: "ShortHairTheCaesarSidePart",
                                accessories: "Round",
                                hairColor: "SilverGray",
                                hatColor: "Blue03",
                                facialHair: "BeardMedium",
                                facialHairColor: "Red",
                                clothing: "ShirtVNeck",
                                clothingFabric: "Pink",
                                clothingGraphic: "Skull",
                            },
                            "base64"
                        )
                    }
                    onClick={() => alert(`Ok`)}
                />
                <ButtonFabric
                    styles={{ baseColor: "#007bff" }}
                    image={
                        "data:image/svg+xml;base64," +
                        avataaars(
                            {
                                skin: "Light",
                                eyeBrow: "UnibrowNatural",
                                eyes: "Hearts",
                                mouth: "Smile",
                                top: "ShortHairTheCaesarSidePart",
                                accessories: "Round",
                                hairColor: "SilverGray",
                                hatColor: "Blue03",
                                facialHair: "BeardMedium",
                                facialHairColor: "Red",
                                clothing: "ShirtVNeck",
                                clothingFabric: "Pink",
                                clothingGraphic: "Skull",
                            },
                            "base64"
                        )
                    }
                    size="large"
                    onClick={() => alert(`Ok`)}
                />
                <ButtonFabric
                    image={
                        "data:image/svg+xml;base64," +
                        avataaars(
                            {
                                skin: "Light",
                                eyeBrow: "UnibrowNatural",
                                eyes: "Hearts",
                                mouth: "Smile",
                                top: "ShortHairTheCaesarSidePart",
                                accessories: "Round",
                                hairColor: "SilverGray",
                                hatColor: "Blue03",
                                facialHair: "BeardMedium",
                                facialHairColor: "Red",
                                clothing: "ShirtVNeck",
                                clothingFabric: "Pink",
                                clothingGraphic: "Skull",
                            },
                            "base64"
                        )
                    }
                    label="Hyperlink (self)"
                    styles={{ baseColor: "#007bff" }}
                    size="small"
                    hyperlink={{
                        url: "https://tripetto.com",
                        target: "self",
                    }}
                />
                <ButtonFabric
                    image={
                        "data:image/svg+xml;base64," +
                        avataaars(
                            {
                                skin: "Light",
                                eyeBrow: "UnibrowNatural",
                                eyes: "Hearts",
                                mouth: "Smile",
                                top: "ShortHairTheCaesarSidePart",
                                accessories: "Round",
                                hairColor: "SilverGray",
                                hatColor: "Blue03",
                                facialHair: "BeardMedium",
                                facialHairColor: "Red",
                                clothing: "ShirtVNeck",
                                clothingFabric: "Pink",
                                clothingGraphic: "Skull",
                            },
                            "base64"
                        )
                    }
                    label="Hyperlink (blank) with a long label"
                    description="And a description with a long line of text"
                    styles={{ baseColor: "#007bff" }}
                    hyperlink={{
                        url: "https://tripetto.com",
                        target: "blank",
                    }}
                />
                <ButtonFabric
                    emoji="👨‍👨‍👦‍👦"
                    label="Hyperlink (blank) with a long label"
                    description="And a description with a long line of text"
                    styles={{ baseColor: "#007bff" }}
                    hyperlink={{
                        url: "https://tripetto.com",
                        target: "blank",
                    }}
                />
                <ButtonFabric styles={{ baseColor: "#007bff" }} emoji="😀" size="small" onClick={() => alert(`Ok`)} />
                <ButtonFabric styles={{ baseColor: "#007bff" }} emoji="😀" size="large" onClick={() => alert(`Ok`)} />
                <ButtonFabric styles={{ baseColor: "#007bff" }} emoji="👨‍👨‍👦‍👦" size="small" onClick={() => alert(`Ok`)} />
                <ButtonFabric styles={{ baseColor: "#007bff" }} emoji="👨‍👨‍👦‍👦" size="medium" onClick={() => alert(`Ok`)} />
                <ButtonFabric styles={{ baseColor: "#007bff" }} emoji="👨‍👨‍👦‍👦" size="large" onClick={() => alert(`Ok`)} />
                <ButtonFabric styles={{ baseColor: "#007bff" }} emoji="Aaa" onClick={() => alert(`Ok`)} />
            </div>
            <div>
                <h2>Avataaars</h2>
                <img
                    src={
                        "data:image/svg+xml;base64," +
                        avataaars(
                            {
                                skin: "Light",
                                eyeBrow: "UnibrowNatural",
                                eyes: "Hearts",
                                mouth: "Smile",
                                top: "ShortHairTheCaesarSidePart",
                                accessories: "Round",
                                hairColor: "SilverGray",
                                hatColor: "Blue03",
                                facialHair: "BeardMedium",
                                facialHairColor: "Red",
                                clothing: "ShirtVNeck",
                                clothingFabric: "Pink",
                                clothingGraphic: "Skull",
                            },
                            "base64"
                        )
                    }
                    width="400"
                    height="400"
                />
            </div>
        </App>
    );
};

createRoot(document.getElementById("app") as Element).render(<Root />);
