import { ReactNode, useEffect, useRef, useState } from "react";

export interface IOverlay {
    readonly key?: string;
    readonly onEffect?: () => (() => void) | undefined;
    readonly children: ReactNode;
}

interface IOverlayProvider {
    readonly element?: (props: { children: ReactNode }) => JSX.Element;
    // eslint-disable-next-line @typescript-eslint/ban-types
    readonly props?: {};
}

export type TOverlayProvider = (props: IOverlayProvider) => JSX.Element;
export type TOverlayContext = (props: IOverlay) => JSX.Element;

/**
 * Creates an overlay for dynamic content.
 * @returns Returns the provider component and the context component.
 */
export const useOverlay = (): [TOverlayProvider, TOverlayContext] => {
    const contentRef = useRef<
        IOverlay & {
            readonly id: number;
            rendered: boolean;
        }
    >();
    const updateRef = useRef<() => void>();
    const idRef = useRef(0);

    return [
        (props: IOverlayProvider) => {
            // eslint-disable-next-line @typescript-eslint/ban-types
            const [, doUpdate] = useState<{}>();

            useEffect(() => contentRef.current?.onEffect && contentRef.current.onEffect());

            updateRef.current = () => doUpdate({});

            if (contentRef.current) {
                contentRef.current.rendered = true;
            }

            return props.element && contentRef.current?.children ? (
                <props.element {...props.props}>{contentRef.current?.children}</props.element>
            ) : (
                <>{contentRef.current?.children}</>
            );
        },
        (props: IOverlay) => {
            const id = ++idRef.current;

            contentRef.current = {
                ...props,
                id,
                rendered: false,
            };

            useEffect(() => {
                if (contentRef.current && !contentRef.current.rendered && updateRef.current) {
                    updateRef.current();
                }

                return () => {
                    if (contentRef.current?.id === id) {
                        if (contentRef.current?.rendered && updateRef.current) {
                            updateRef.current();
                        }

                        contentRef.current = undefined;
                    }
                };
            });

            return <></>;
        },
    ] as [TOverlayProvider, TOverlayContext];
};
