import { css, styled } from "styled-components";
import { ChangeEvent, FocusEvent, KeyboardEvent, useEffect, useRef, useState } from "react";
import {
    DateTime,
    L10n,
    Num,
    Str,
    TSerializeTypes,
    cancelUITimeout,
    castToNumber,
    isNumberFinite,
    scheduleUITimeout,
} from "@tripetto/runner";
import { IInputFabricStyles } from "../input/styles";
import { handleBlur, handleFocus, setReturnValue } from "../helpers";
import { color } from "../../color";
import { warningIcon } from "../../icons/warning";
import { DEBOUNCE_MAX, DEBOUNCE_MIN } from "../const";

export const DateInputElement = styled.input<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $roundness: number | undefined;
    $textColor: string | undefined;
    $errorColor: string;
    $errorVisible: boolean;
    $error: boolean;
    $scale: number;
    $height?: number;
    $placeholder?: boolean;
}>`
    appearance: none;
    outline: none;
    box-sizing: border-box;
    display: block;
    width: 100%;
    height: ${(props) => (props.$height ? `${props.$height}px` : undefined)};
    font-size: 1em;
    line-height: 1.5em;
    background-color: ${(props) =>
        color(
            props.$error && props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                ? props.$errorColor
                : props.$backgroundColor
        )};
    background-image: ${(props) =>
        props.$error
            ? `url("data:image/svg+xml;base64,${warningIcon(
                  color(props.$errorColor, (o) =>
                      o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                  )
              )}")`
            : ""};
    background-repeat: no-repeat;
    background-size: ${8 / 7}em;
    background-position: right 0.6em center;
    border: ${(props) =>
        `${props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? 0 : props.$borderSize}px solid ${color(
            props.$error
                ? props.$errorColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : props.$backgroundColor
        )}`};
    color: ${(props) =>
        color(
            props.$error
                ? props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                    ? color(props.$errorColor, (o) => o.makeBlackOrWhite())
                    : props.$errorColor
                : props.$textColor
                ? props.$textColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
        )};
    border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : "0.5em")};
    padding: ${(props) =>
        props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? "0" : `${0.375 * props.$scale}em 0.75em`};
    padding-right: ${(props) => (props.$error && `${8 / 7 + 0.75}em`) || undefined};
    margin: 0;
    opacity: 0.65;
    transition:
        color 0.15s ease-in-out,
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out,
        opacity 0.15s ease-in-out;

    &::placeholder {
        color: ${(props) =>
            color(
                props.$error
                    ? props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                        ? color(props.$errorColor, (o) => o.makeBlackOrWhite())
                        : props.$errorColor
                    : props.$textColor
                    ? props.$textColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
            )};
        opacity: 0.5;
        transition: color 0.15s ease-in-out opacity 0.15s ease-in-out;
    }

    &:not(:disabled):not([readonly]) {
        opacity: 1;

        &:hover {
            box-shadow: ${(props) =>
                (props.$placeholder &&
                    (props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                    `0 0 0 0.2rem ${color(
                        props.$error
                            ? props.$errorColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : props.$backgroundColor,
                        (o) => o.manipulate((m) => m.alpha(0.2))
                    )}`) ||
                undefined};
        }

        ${(props) =>
            !props.$placeholder &&
            css`
                background-color: ${color(props.$backgroundColor)};
                background-image: none;
                border-color: ${color(
                    props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor
                )};
                color: ${color(
                    props.$textColor
                        ? props.$textColor
                        : props.$borderColor && props.$borderColor !== "transparent"
                        ? props.$borderColor
                        : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                )};
                box-shadow: ${((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                    `0 0 0 0.2rem ${color(
                        props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                        (o) => o.manipulate((m) => m.alpha(0.5))
                    )}`) ||
                undefined};
                &::placeholder {
                    color: ${color(
                        props.$textColor
                            ? props.$textColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                    )};
                    opacity: 0.3;
                }
            `}
    }

    &::-webkit-clear-button,
    &::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
        height: 1px;
        display: none;
    }
`;

function parseValue(value: number | Date | true | undefined, mode: "date" | "datetime" = "datetime"): string {
    if (!value) {
        return "";
    }

    if (value instanceof Date) {
        value = value.getTime();
    }

    if (value !== 0) {
        const date = value === true ? new Date() : new Date(value);

        return (
            `${date.getUTCFullYear()}-${Str.padLeft(date.getUTCMonth() + 1, "0", 2)}-${Str.padLeft(date.getUTCDate(), "0", 2)}` +
            (mode === "datetime" ? `T${Str.padLeft(date.getUTCHours(), "0", 2)}:${Str.padLeft(date.getUTCMinutes(), "0", 2)}` : "")
        );
    }

    return "";
}

function parseInput(value: string): Date {
    if (value) {
        const src = Str.uppercase(Str.sanitize(value)).replace(" ", "T").split("T");

        if (src.length > 0) {
            const date = Str.replaceMultiple(src[0], ["/", "\\"], "-").split("-");
            const time = src.length > 1 ? Str.replace(src[1], ".", ":").split(":") : ["0", "0"];

            if (date.length === 3 && time.length >= 2) {
                return new Date(
                    `${Str.padLeft(castToNumber(date[0]), "0", 4)}-${Str.padLeft(castToNumber(date[1]), "0", 2)}-${Str.padLeft(
                        castToNumber(date[2]),
                        "0",
                        2
                    )}T${Str.padLeft(castToNumber(time[0]), "0", 2)}:${Str.padLeft(castToNumber(time[1]), "0", 2)}:00.000Z`
                );
            }
        }
    }

    return new Date(0);
}

export const DateTimeFabric = (props: {
    readonly styles: IInputFabricStyles;
    readonly l10n?: L10n.Namespace;
    readonly id?: string;
    readonly mode?: "date" | "datetime";
    readonly placeholder?: string;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly value?:
        | Date
        | number
        | {
              pristine: TSerializeTypes;
              readonly value: Date | number;
              readonly hasValue: boolean;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly slot: {
                  readonly minimum?: Date | number | true;
                  readonly maximum?: Date | number | true;
                  readonly toValue: (value: TSerializeTypes) => number;
              };
          };
    readonly ariaDescribedBy?: string;
    readonly onChange?: (value: Date) => Date | void;
    readonly onFocus?: (e: FocusEvent) => string | void;
    readonly onBlur?: (e: FocusEvent) => string | void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value instanceof Date ? props.value.getTime() : props.value;
    const debounceRef = useRef<{
        duration: number;
        handle: number;
        update?: () => void;
    }>({
        duration: 0,
        handle: 0,
    });
    const [type, setType] = useState<"text" | "date" | "datetime-local">("text");
    const [height, setHeight] = useState(0);
    const [focus, setFocus] = useState(false);
    const [focusValue, setFocusValue] = useState(
        typeof valueRef === "object"
            ? parseValue(valueRef.slot.toValue(valueRef.pristine), props.mode || "date")
            : typeof valueRef === "number"
            ? parseValue(valueRef, props.mode || "date")
            : ""
    );
    const [value, setValue] =
        typeof valueRef === "object" && !(valueRef instanceof Date)
            ? [
                  focus ? focusValue : valueRef.hasValue ? parseValue(valueRef.slot.toValue(valueRef.pristine), props.mode || "date") : "",
                  (val: string) => {
                      cancelUITimeout(debounceRef.current.handle);

                      setFocusValue(val);

                      const nTimeout = Num.range(debounceRef.current.duration * 2, DEBOUNCE_MIN, DEBOUNCE_MAX);

                      debounceRef.current.handle = scheduleUITimeout(() => {
                          const start = DateTime.precise;

                          debounceRef.current.handle = 0;
                          debounceRef.current.update = () => {
                              debounceRef.current.duration = DateTime.elapsed(start, true);
                              debounceRef.current.update = undefined;
                          };

                          valueRef.pristine = val !== "" ? parseInput(val).getTime() : undefined;
                      }, nTimeout);
                  },
              ]
            : [focusValue, setFocusValue];
    const [errorVisible, makeErrorVisible] = useState(value ? true : false);
    const focusRef = useRef(false);
    const isReadOnly = () => props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false;
    const styles = {
        $backgroundColor: props.styles.backgroundColor || "transparent",
        $borderColor: props.styles.borderColor || "transparent",
        $borderSize: props.styles.borderSize || 1,
        $roundness: props.styles.roundness,
        $textColor: props.styles.textColor,
        $errorColor: props.styles.errorColor,
        $errorVisible: errorVisible,
        $error: props.error || false,
        $scale: props.styles.scale || 1,
        $placeholder: true,
        $height: height,
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current.handle);
        };
    }, []);

    if (debounceRef.current.update) {
        debounceRef.current.update();
    }

    return (
        <>
            {(type === "text" || isReadOnly()) && (
                <DateInputElement
                    id={props.id}
                    ref={props.onAutoFocus}
                    type={"text"}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    required={props.required || false}
                    disabled={props.disabled || false}
                    readOnly={isReadOnly()}
                    value={
                        (value &&
                            (props.mode === "datetime"
                                ? (props.l10n?.locale || L10n.Locales).dateTimeFull(parseInput(value).getTime(), true)
                                : (props.l10n?.locale || L10n.Locales).dateFull(parseInput(value).getTime(), true))) ||
                        ""
                    }
                    autoComplete={"off"}
                    inputMode={"none"}
                    aria-describedby={props.ariaDescribedBy}
                    onChange={() => undefined}
                    onFocus={(e: FocusEvent<HTMLInputElement>) => {
                        if (!isReadOnly()) {
                            focusRef.current = true;

                            setHeight(e.target.getBoundingClientRect().height);
                            setType(props.mode === "datetime" ? "datetime-local" : "date");
                        }

                        handleFocus(setFocus, setValue, props.onFocus)(e);
                    }}
                    onBlur={(e: FocusEvent<HTMLInputElement>) => {
                        if (!isReadOnly()) {
                            handleBlur(setFocus, setValue, props.onBlur)(e);
                        }
                    }}
                    {...styles}
                />
            )}
            {type !== "text" && !isReadOnly() && (
                <DateInputElement
                    id={props.id}
                    ref={(el: HTMLInputElement) => {
                        if (focusRef.current && el) {
                            focusRef.current = false;

                            el.focus();
                        }
                    }}
                    type={type}
                    placeholder="yyyy-MM-ddThh:mm"
                    tabIndex={props.tabIndex}
                    required={true}
                    disabled={props.disabled || false}
                    defaultValue={value}
                    autoComplete={"off"}
                    aria-describedby={props.ariaDescribedBy}
                    min={parseValue((typeof valueRef === "object" && valueRef.slot.minimum) || undefined) || undefined}
                    max={parseValue((typeof valueRef === "object" && valueRef.slot.maximum) || undefined) || undefined}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setValue(e.target.value);
                        makeErrorVisible(true);

                        if (props.onChange) {
                            const returnValue = props.onChange(parseInput(e.target.value));

                            if (returnValue instanceof Date) {
                                setReturnValue(setValue, parseValue(returnValue, props.mode || "date"));
                            }
                        }
                    }}
                    onFocus={(e: FocusEvent<HTMLInputElement>) => {
                        handleFocus(setFocus, setValue, props.onFocus)(e);
                    }}
                    onBlur={(e: FocusEvent<HTMLInputElement>) => {
                        setType("text");

                        if (typeof valueRef === "object" && valueRef.hasValue) {
                            setFocusValue(parseValue(valueRef.slot.toValue(valueRef.pristine), props.mode || "date"));
                        }

                        handleBlur(setFocus, setValue, props.onBlur)(e);
                    }}
                    onKeyDown={(e: KeyboardEvent<HTMLInputElement>) => {
                        if (e.key === "Enter" && props.onSubmit) {
                            e.preventDefault();

                            props.onSubmit();
                        } else if (e.key === "Escape") {
                            e.currentTarget.blur();
                        } else if (e.key === "Tab") {
                            if (e.shiftKey) {
                                if (props.onCancel) {
                                    e.preventDefault();

                                    props.onCancel();
                                }
                            } else if (props.onSubmit) {
                                e.preventDefault();

                                props.onSubmit();
                            }
                        }
                    }}
                    {...styles}
                />
            )}
        </>
    );
};
