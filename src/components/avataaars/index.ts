import { TAvataaarsSkin, skin } from "./skin";
import { TAvataaarsClothing, clothing } from "./clothing";
import { TAvataaarsClothingFabric } from "./clothing/fabric";
import { TAvataaarsClothingGraphic } from "./clothing/graphic";
import { face } from "./face";
import { TAvataaarsEyes } from "./face/eyes";
import { TAvataaarsEyebrow } from "./face/eyebrow";
import { TAvataaarsMouth } from "./face/mouth";
import { TAvataaarsTop, top } from "./top";
import { TAvataaarsHairColor } from "./top/hair";
import { TAvataaarsHatColor } from "./top/hat";
import { TAvataaarsFacialHair, TAvataaarsFacialHairColor } from "./top/facial-hair";
import { TAvataaarsAccessories } from "./top/accessories";

export interface IAvataaars {
    readonly skin: TAvataaarsSkin;
    readonly eyes: TAvataaarsEyes;
    readonly eyeBrow: TAvataaarsEyebrow;
    readonly mouth: TAvataaarsMouth;
    readonly top: TAvataaarsTop;
    readonly hairColor?: TAvataaarsHairColor;
    readonly hatColor?: TAvataaarsHatColor;
    readonly facialHair: TAvataaarsFacialHair;
    readonly facialHairColor?: TAvataaarsFacialHairColor;
    readonly clothing: TAvataaarsClothing;
    readonly clothingFabric?: TAvataaarsClothingFabric;
    readonly clothingGraphic?: TAvataaarsClothingGraphic;
    readonly accessories: TAvataaarsAccessories;
}

const avataaarsCache: {
    [hash: string]: string;
} = {};

export const avataaars = (props: IAvataaars, type: "svg" | "base64") => {
    const key = `${type}:${props.skin}:${props.eyes}:${props.eyeBrow}:${props.mouth}:${props.top}:${props.hairColor || ""}:${
        props.hatColor || ""
    }:${props.facialHair}:${props.facialHairColor || ""}:${props.clothing}:${props.clothingFabric || ""}:${props.clothingGraphic || ""}:${
        props.accessories
    }`;
    const cache = avataaarsCache[key];

    if (cache) {
        return cache;
    }

    const svg = `
        <svg width="264px" height="280px" viewBox="0 0 264 280" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g id="Avataaar" stroke-width="1" fill-rule="evenodd">
                ${skin(props.skin)}
                ${clothing(props.clothing, props.clothingFabric, props.clothingGraphic)}
                ${face(props.mouth, props.eyes, props.eyeBrow)}
                ${top(props.top, props.hairColor, props.hatColor, props.facialHair, props.facialHairColor, props.accessories)}
            </g>
        </svg>
    `;

    return (avataaarsCache[key] = type === "base64" ? btoa(svg) : svg);
};
