export type TAvataaarsClothingFabric =
    | "Black"
    | "Blue01"
    | "Blue02"
    | "Blue03"
    | "Gray01"
    | "Gray02"
    | "Heather"
    | "PastelBlue"
    | "PastelGreen"
    | "PastelOrange"
    | "PastelRed"
    | "PastelYellow"
    | "Pink"
    | "Red"
    | "White";

export const clothingFabric = (type: TAvataaarsClothingFabric): string => {
    switch (type) {
        case "Black":
            return "#262E33";
        case "Blue01":
            return "#65C9FF";
        case "Blue02":
            return "#5199E4";
        case "Blue03":
            return "#25557C";
        case "Gray01":
            return "#E5E5E5";
        case "Gray02":
            return "#929598";
        case "Heather":
            return "#3C4F5C";
        case "PastelBlue":
            return "#B1E2FF";
        case "PastelGreen":
            return "#A7FFC4";
        case "PastelOrange":
            return "#FFDEB5";
        case "PastelRed":
            return "#FFAFB9";
        case "PastelYellow":
            return "#FFFFB1";
        case "Pink":
            return "#FF488E";
        case "Red":
            return "#FF5C5C";
        case "White":
            return "#FFFFFF";
    }
};
