export type TAvataaarsHairColor =
    | "Auburn"
    | "Black"
    | "Blonde"
    | "BlondeGolden"
    | "Brown"
    | "BrownDark"
    | "PastelPink"
    | "Platinum"
    | "Red"
    | "SilverGray";

export const topHairColor = (type: TAvataaarsHairColor): string => {
    switch (type) {
        case "Auburn":
            return "#A55728";
            break;
        case "Black":
            return "#2C1B18";
            break;
        case "Blonde":
            return "#B58143";
            break;
        case "BlondeGolden":
            return "#D6B370";
            break;
        case "Brown":
            return "#724133";
            break;
        case "BrownDark":
            return "#4A312C";
            break;
        case "PastelPink":
            return "#F59797";
            break;
        case "Platinum":
            return "#ECDCBF";
            break;
        case "Red":
            return "#C93305";
            break;
        case "SilverGray":
            return "#E8E1E1";
            break;
    }
};
