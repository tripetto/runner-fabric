import { TAvataaarsMouth, faceMouth } from "./mouth";
import { TAvataaarsEyes, faceEyes } from "./eyes";
import { TAvataaarsEyebrow, faceEyebrow } from "./eyebrow";

export const face = (mouth: TAvataaarsMouth, eyes: TAvataaarsEyes, eyebrow: TAvataaarsEyebrow) => `
    <g transform="translate(76.000000, 82.000000)" fill="#000000">
        ${faceMouth(mouth)}
        <g transform="translate(28.000000, 40.000000)" fill-opacity="0.16">
            <path d="M16,8 C16,12.418278 21.372583,16 28,16 L28,16 C34.627417,16 40,12.418278 40,8" id="Nose"></path>
        </g>
        ${faceEyes(eyes)}
        ${faceEyebrow(eyebrow)}
    </g>
`;
