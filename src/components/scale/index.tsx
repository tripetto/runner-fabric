import { styled } from "styled-components";
import { FocusEvent, KeyboardEvent, ReactNode, useEffect, useRef, useState } from "react";
import {
    Num,
    TSerializeTypes,
    cancelUITimeout,
    castToString,
    findFirst,
    isArray,
    isNumberFinite,
    scheduleUITimeout,
} from "@tripetto/runner";
import { handleAutoSubmit } from "../helpers";
import { ButtonFabric } from "../button";
import { DEBOUNCE_NORMAL } from "../const";

export interface IScaleNumeric {
    readonly from: number;
    readonly to: number;
    readonly stepSize?: number;
}

export interface IScaleOption {
    readonly id: string;
    readonly name: string;
    readonly value?: string;
}

const ScaleElement = styled.div<{
    $justify?: boolean;
}>`
    display: block;
    width: 100%;

    > div {
        max-width: calc(100% + 0.4rem);
        overflow-x: scroll;
        -ms-overflow-style: none;
        -webkit-overflow-scrolling: touch;
        scrollbar-width: none;
        padding: 0.2rem;
        margin -0.2rem;

        &::-webkit-scrollbar {
            display: none;
        }

        > div {
            display: inline-block;
            white-space: nowrap;
            min-width: ${(props) => (props.$justify && "100%") || undefined};

            @media (max-device-width: 500px) {
                min-width: 100%;
            }

            @media (max-width: 500px) {
                min-width: 100%;
            }

            > div {
                display: flex;
                flex-wrap: nowrap;

                > button {
                    flex-grow: ${(props) => (props.$justify && "1") || undefined};
                    min-width: ${(props) => (!props.$justify && "3em") || undefined};

                    @media (max-device-width: 500px) {
                        min-width: auto;
                        flex-grow: 1;
                        padding-left: 0.55em;
                        padding-right: 0.55em;
                    }

                    @media (max-width: 500px) {
                        min-width: auto;
                        flex-grow: 1;
                        padding-left: 0.55em;
                        padding-right: 0.55em;
                    }

                    > span > span {
                        display: flex;
                        align-content: center;
                        justify-content: center;
                    }
                }
            }
        }
    }
`;

const ScaleLabelsElement = styled.div<{
    $labelColor?: string;
}>`
    display: flex;
    flex-direction: row;
    padding-top: 0.1em;

    > small {
        flex: 1 1 0%;
        font-size: 0.8em;
        color: ${(props) => props.$labelColor};

        &:first-child {
            text-align: left;
        }

        &:nth-child(2) {
            text-align: center;
        }

        &:last-child {
            text-align: right;
        }
    }
`;

const assertValue = (
    valueRef: {
        reference?: string;
        readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
        readonly clear: () => void;
    },
    options: IScaleNumeric | IScaleOption[],
    reference?: number | string | undefined
) => {
    if (isArray(options)) {
        const selected = findFirst(options, (option) => option.id === reference);

        if (valueRef.reference !== selected?.id) {
            valueRef.set(selected && (selected.value || selected.name), selected?.id, selected?.name);
        }

        return (selected && selected.id) || "";
    } else if (isNumberFinite(reference) && options) {
        const value = Num.range(reference, Num.min(options.from, options.to), Num.max(options.from, options.to));

        valueRef.set(value);

        return value;
    }

    valueRef.clear();

    return undefined;
};

export const ScaleFabric = (props: {
    readonly styles: {
        readonly color: string;
        readonly outlineSize?: number;
        readonly roundness?: number;
        readonly scale?: number;
        readonly margin?: number;
        readonly labelColor?: string;
    };
    readonly options: IScaleNumeric | IScaleOption[];
    readonly labelLeft?: ReactNode;
    readonly labelCenter?: ReactNode;
    readonly labelRight?: ReactNode;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly ariaDescribedBy?: string;
    readonly tabIndex?: number;
    readonly justify?: boolean;
    readonly value?:
        | number
        | string
        | {
              pristine: TSerializeTypes;
              reference?: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
              readonly clear: () => void;
          };
    readonly required?: boolean;
    readonly autoSubmit?: boolean;
    readonly view?: "live" | "test" | "preview";
    readonly onChange?: (value: number | string | undefined) => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLButtonElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<number>(0);
    const [proxy, setProxy] = useState((typeof valueRef !== "object" && valueRef) || undefined);
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  debounceRef.current !== 0
                      ? proxy
                      : assertValue(
                            valueRef,
                            props.options,
                            isArray(props.options) ? valueRef.reference : isNumberFinite(valueRef.pristine) ? valueRef.pristine : undefined
                        ),
                  (val: number | string | undefined, submit: boolean) => {
                      cancelUITimeout(debounceRef.current);

                      setProxy(val);

                      debounceRef.current = scheduleUITimeout(() => {
                          debounceRef.current = 0;

                          assertValue(valueRef, props.options, val);

                          if (submit && props.autoSubmit && val !== undefined) {
                              handleAutoSubmit(autoSubmit);
                          }
                      }, DEBOUNCE_NORMAL);
                  },
              ]
            : [proxy, setProxy];
    const autoSubmit = useRef({
        id: 0,
        cb: props.onSubmit,
    });

    const changeValue = (val: number | string | undefined, submit: boolean) => {
        if (autoSubmit.current.id) {
            clearTimeout(autoSubmit.current.id);

            autoSubmit.current.id = 0;
        }

        setValue(val, submit);

        if (props.onChange) {
            props.onChange(val);
        }

        if (submit && props.autoSubmit && val !== undefined) {
            handleAutoSubmit(autoSubmit);
        }
    };

    let buttons: JSX.Element[] = [];
    const handleKeyDown = (index: number, action?: (key: number | "-" | "+") => void) => (e: KeyboardEvent<HTMLButtonElement>) => {
        if (e.shiftKey && e.key === "Enter" && props.onSubmit) {
            e.preventDefault();

            props.onSubmit();
        } else if (e.key === "Escape") {
            e.currentTarget.blur();
        } else if (e.key === "Tab") {
            if (e.shiftKey) {
                if (props.onCancel && index === 0) {
                    e.preventDefault();

                    props.onCancel();
                }
            } else if (props.onSubmit && index + 1 === buttons.length) {
                e.preventDefault();

                props.onSubmit();
            }
        } else if (action) {
            if (e.key === "-" || e.key === "+") {
                action(e.key);
            } else {
                const n = e.key.length === 1 ? e.key.charCodeAt(0) - 48 : -1;

                if (n >= 0 && n <= 9) {
                    action(n);
                }
            }
        }
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    autoSubmit.current.cb = props.onSubmit;

    if (isArray(props.options)) {
        const options = props.view === "preview" ? props.options : props.options.filter((option) => (option.name ? true : false));
        const count = options.length;

        buttons = options.map((option, index) => (
            <ButtonFabric
                key={index}
                styles={{
                    baseColor: props.styles.color,
                    mode: value === option.id ? "fill" : "outline",
                    hover: "outline",
                    outlineSize: props.styles.outlineSize,
                    roundness: props.styles.roundness,
                    scale: props.styles.scale,
                    group: count > 1 ? (index === 0 ? "start" : index + 1 === count ? "end" : "middle") : undefined,
                }}
                label={option.name || "..."}
                tabIndex={props.tabIndex}
                ariaDescribedBy={props.ariaDescribedBy}
                disabled={
                    props.disabled || props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false
                }
                onAutoFocus={(((!value && index === 0) || value === option.id) && props.onAutoFocus) || undefined}
                onFocus={props.onFocus}
                onBlur={props.onBlur}
                onKeyDown={handleKeyDown(index)}
                onClick={() => changeValue(props.required || value !== option.id ? option.id : undefined, true)}
            />
        ));
    } else if (props.options) {
        const from = Num.min(props.options.from, props.options.to);
        const to = Num.max(props.options.from, props.options.to);
        const stepSize = Num.max(props.options.stepSize || 0, 1);

        for (let index = from; index <= to; index += stepSize) {
            buttons.push(
                <ButtonFabric
                    key={index}
                    styles={{
                        baseColor: props.styles.color,
                        mode: value === index ? "fill" : "outline",
                        hover: "outline",
                        outlineSize: props.styles.outlineSize,
                        roundness: props.styles.roundness,
                        scale: props.styles.scale,
                        group:
                            to > from && from + stepSize <= to
                                ? index === from
                                    ? "start"
                                    : index + stepSize > to
                                    ? "end"
                                    : "middle"
                                : undefined,
                    }}
                    label={castToString(index)}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    disabled={
                        props.disabled ||
                        props.readOnly ||
                        (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) ||
                        false
                    }
                    onAutoFocus={(index === (isNumberFinite(value) ? value : from) && props.onAutoFocus) || undefined}
                    onFocus={props.onFocus}
                    onBlur={props.onBlur}
                    onKeyDown={handleKeyDown(index - from, (key) => {
                        if (key === "-" || key === "+") {
                            if (!isNumberFinite(value)) {
                                changeValue(index, false);
                            } else if ((key === "-" && value > from) || (key === "+" && value < to)) {
                                changeValue(value + (key === "-" ? -1 : 1), false);
                            }
                        } else if (key >= from && key <= to) {
                            changeValue(value !== key ? key : undefined, true);
                        }
                    })}
                    onClick={() => changeValue(props.required || value !== index ? index : undefined, true)}
                />
            );
        }
    }

    return (
        <ScaleElement $justify={props.justify}>
            <div>
                <div>
                    <div>{buttons}</div>
                    {buttons.length > 0 &&
                        (props.labelLeft || (props.labelCenter && buttons.length > 2) || (props.labelRight && buttons.length > 1)) && (
                            <ScaleLabelsElement $labelColor={props.styles.labelColor}>
                                <small>{props.labelLeft || ""}</small>
                                <small>{(buttons.length > 2 && props.labelCenter) || ""}</small>
                                <small>{(buttons.length > 1 && props.labelRight) || ""}</small>
                            </ScaleLabelsElement>
                        )}
                </div>
            </div>
        </ScaleElement>
    );
};
