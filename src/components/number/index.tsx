import { ChangeEvent, FocusEvent, KeyboardEvent, useEffect, useRef, useState } from "react";
import {
    DateTime,
    L10n,
    Num,
    Str,
    TSerializeTypes,
    cancelUITimeout,
    castToFloat,
    castToString,
    isNumberFinite,
    scheduleUITimeout,
} from "@tripetto/runner";
import { InputElement } from "../input";
import { IInputFabricStyles } from "../input/styles";
import { handleBlur, handleFocus, setReturnValue } from "../helpers";
import { DEBOUNCE_MAX, DEBOUNCE_MIN } from "../const";

export const NumberFabric = (props: {
    readonly styles: IInputFabricStyles;
    readonly l10n?: L10n.Namespace;
    readonly id?: string;
    readonly precision?: number;
    readonly placeholder?: string;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly maxLength?: number;
    readonly value?:
        | number
        | {
              pristine: TSerializeTypes;
              readonly value: number;
              readonly string: string;
              readonly hasValue: boolean;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly slot: {
                  readonly precision?: number;
                  readonly digits?: number;
                  readonly minimum?: number;
                  readonly maximum?: number;
                  readonly separator?: string;
                  readonly formatString: (value: string, plural: boolean) => string;
                  readonly toValue: (value: string) => number;
              };
          };
    readonly ariaDescribedBy?: string;
    readonly onChange?: (value: string) => string | void;
    readonly onFocus?: (e: FocusEvent) => string | void;
    readonly onBlur?: (e: FocusEvent) => string | void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<{
        duration: number;
        handle: number;
        update?: () => void;
    }>({
        duration: 0,
        handle: 0,
    });
    const [type, setType] = useState<"text" | "number">("text");
    const [focus, setFocus] = useState(false);
    const [focusValue, setFocusValue] = useState(
        typeof valueRef === "object" ? castToString(valueRef.pristine) : typeof valueRef === "number" ? valueRef.toString() : ""
    );
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  focus
                      ? focusValue
                      : valueRef.hasValue
                      ? valueRef.slot.formatString(
                            valueRef.slot.digits
                                ? Str.padLeft(valueRef.value, "0", valueRef.slot.digits, false, true)
                                : (props.l10n?.locale || L10n.Locales).number(
                                      valueRef.value,
                                      valueRef.slot.precision,
                                      (valueRef.slot.separator && true) || false
                                  ),
                            valueRef.value !== 1
                        )
                      : "",
                  (val: string) => {
                      cancelUITimeout(debounceRef.current.handle);

                      setFocusValue(val);

                      const nTimeout = Num.range(debounceRef.current.duration * 2, DEBOUNCE_MIN, DEBOUNCE_MAX);

                      debounceRef.current.handle = scheduleUITimeout(() => {
                          const start = DateTime.precise;

                          debounceRef.current.handle = 0;
                          debounceRef.current.update = () => {
                              debounceRef.current.duration = DateTime.elapsed(start, true);
                              debounceRef.current.update = undefined;
                          };
                          valueRef.pristine = val !== "" ? valueRef.slot.toValue(val) : undefined;
                          valueRef.pristine = val || undefined;
                      }, nTimeout);
                  },
              ]
            : [focusValue, setFocusValue];
    const [errorVisible, makeErrorVisible] = useState(value ? true : false);
    const focusRef = useRef(0);
    const currentValue = focus && typeof valueRef === "object" ? castToFloat(focusValue) : 0;
    const error =
        props.error ||
        (focus &&
            typeof valueRef === "object" &&
            ((isNumberFinite(valueRef.slot.minimum) && currentValue < valueRef.slot.minimum) ||
                (isNumberFinite(valueRef.slot.maximum) && currentValue > valueRef.slot.maximum))) ||
        false;

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current.handle);
        };
    }, []);

    if (debounceRef.current.update) {
        debounceRef.current.update();
    }

    return (
        <InputElement
            id={props.id}
            ref={props.onAutoFocus}
            type={type}
            tabIndex={props.tabIndex}
            placeholder={props.placeholder}
            required={props.required || false}
            disabled={props.disabled || false}
            readOnly={props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false}
            maxLength={props.maxLength}
            value={value}
            autoComplete={"off"}
            step={
                type === "number"
                    ? (typeof valueRef === "object" && valueRef.slot.precision && `0.${Str.fill("0", valueRef.slot.precision - 1)}1`) || "1"
                    : undefined
            }
            inputMode={props.precision || (typeof valueRef === "object" && valueRef.slot.precision) ? "decimal" : "numeric"}
            aria-describedby={props.ariaDescribedBy}
            onChange={(e: ChangeEvent<HTMLInputElement>) => {
                setValue(e.target.value);
                makeErrorVisible(true);

                if (props.onChange) {
                    setReturnValue(setValue, props.onChange(e.target.value));
                }
            }}
            onFocus={(e: FocusEvent<HTMLInputElement>) => {
                if (focusRef.current) {
                    cancelAnimationFrame(focusRef.current);

                    focusRef.current = 0;
                }

                if (type === "text") {
                    const el = e.target;

                    /** In Firefox we lose focus when switching input type. */
                    requestAnimationFrame(() => {
                        el.focus();
                    });

                    setType("number");
                }

                handleFocus(setFocus, setValue, props.onFocus)(e);
            }}
            onBlur={(e: FocusEvent<HTMLInputElement>) => {
                focusRef.current = requestAnimationFrame(() => {
                    focusRef.current = 0;

                    if (type === "number") {
                        setType("text");
                    }

                    if (typeof valueRef === "object" && valueRef.hasValue) {
                        setFocusValue(castToString(valueRef.pristine));
                    }

                    handleBlur(setFocus, setValue, props.onBlur)(e);
                });
            }}
            onKeyDown={(e: KeyboardEvent<HTMLInputElement>) => {
                if (e.key === "Enter" && props.onSubmit) {
                    e.preventDefault();

                    props.onSubmit();
                } else if (e.key === "Escape") {
                    e.currentTarget.blur();
                } else if (e.key === "Tab") {
                    if (e.shiftKey) {
                        if (props.onCancel) {
                            e.preventDefault();

                            props.onCancel();
                        }
                    } else if (props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                }
            }}
            $backgroundColor={props.styles.backgroundColor || "transparent"}
            $borderColor={props.styles.borderColor || "transparent"}
            $borderSize={props.styles.borderSize || 1}
            $roundness={props.styles.roundness}
            $textColor={props.styles.textColor}
            $errorColor={props.styles.errorColor}
            $errorVisible={errorVisible}
            $error={error}
            $scale={props.styles.scale || 1}
        />
    );
};
