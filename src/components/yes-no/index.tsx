import { styled } from "styled-components";
import { FocusEvent, KeyboardEvent, useEffect, useRef, useState } from "react";
import { TSerializeTypes, cancelUITimeout, scheduleUITimeout } from "@tripetto/runner";
import { handleAutoSubmit } from "../helpers";
import { ButtonFabric } from "../button";
import { yesIcon } from "../../icons/yes";
import { noIcon } from "../../icons/no";
import { DEBOUNCE_NORMAL } from "../const";

export interface IYesNo {
    readonly label: string;
    readonly icon?: "yes" | "no";
    readonly color?: string;
}

const YesNoElement = styled.div<{
    $alignment?: "horizontal" | "vertical";
    $margin?: number;
}>`
    display: block;
    width: 100%;

    > * {
        display: ${(props) => (props.$alignment === "vertical" && "block") || undefined};
    }

    > * + * {
        margin-top: ${(props) => (props.$alignment === "vertical" && `${props.$margin || 8}px`) || undefined};
    }
`;

const getIcon = (icon: "yes" | "no") => {
    switch (icon) {
        case "yes":
            return yesIcon;
        case "no":
            return noIcon;
    }
};

const assertValue = (
    valueRef: {
        reference?: string;
        readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
        readonly clear: () => void;
    },
    reference: string | undefined,
    labels: {
        readonly yes: IYesNo;
        readonly no: IYesNo;
    }
) => {
    if (reference === "yes" || reference === "no") {
        if (valueRef.reference !== reference) {
            const value = reference === "yes" ? labels.yes.label : labels.no.label;

            valueRef.set(value, reference, value);
        }

        return reference;
    }

    valueRef.clear();

    return "";
};

export const YesNoFabric = (props: {
    readonly styles: {
        readonly yesColor: string;
        readonly noColor: string;
        readonly outlineSize?: number;
        readonly roundness?: number;
        readonly scale?: number;
        readonly margin?: number;
        readonly alignment?: "horizontal" | "vertical";
    };
    readonly yes: IYesNo;
    readonly no: IYesNo;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly ariaDescribedBy?: string;
    readonly tabIndex?: number;
    readonly value?:
        | ""
        | "yes"
        | "no"
        | {
              reference?: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
              readonly clear: () => void;
          };
    readonly autoSubmit?: boolean;
    readonly onChange?: (value: "" | "yes" | "no") => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLButtonElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<number>(0);
    const [proxy, setProxy] = useState<"yes" | "no" | "">((typeof valueRef !== "object" && valueRef) || "");
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  debounceRef.current !== 0 ? proxy : assertValue(valueRef, valueRef.reference, props),
                  (val: "yes" | "no" | "") => {
                      cancelUITimeout(debounceRef.current);

                      setProxy(val);

                      debounceRef.current = scheduleUITimeout(() => {
                          debounceRef.current = 0;

                          assertValue(valueRef, val, props);

                          if (props.autoSubmit && val) {
                              handleAutoSubmit(autoSubmit);
                          }
                      }, DEBOUNCE_NORMAL);
                  },
              ]
            : [proxy, setProxy];
    const autoSubmit = useRef({
        id: 0,
        cb: props.onSubmit,
    });

    const changeValue = (val: "yes" | "no" | false) => {
        if (autoSubmit.current.id) {
            clearTimeout(autoSubmit.current.id);

            autoSubmit.current.id = 0;
        }

        setValue(val || "");

        if (props.onChange) {
            props.onChange(val || "");
        }

        if (typeof valueRef !== "object" && props.autoSubmit && val) {
            handleAutoSubmit(autoSubmit);
        }
    };

    const handleKeyDown = (e: KeyboardEvent<HTMLButtonElement>, b: "yes" | "no") => {
        if (
            e.key === "y" ||
            e.key === "Y" ||
            e.key === props.yes.label.charAt(0).toLowerCase() ||
            e.key === props.yes.label.charAt(0).toUpperCase()
        ) {
            e.preventDefault();

            changeValue("yes");
        } else if (
            e.key === "n" ||
            e.key === "N" ||
            e.key === props.no.label.charAt(0).toLowerCase() ||
            e.key === props.no.label.charAt(0).toUpperCase()
        ) {
            e.preventDefault();

            changeValue("no");
        } else if (e.shiftKey && e.key === "Enter" && props.onSubmit) {
            e.preventDefault();

            props.onSubmit();
        } else if (e.key === "Escape") {
            e.currentTarget.blur();
        } else if (e.key === "Tab") {
            if (e.shiftKey) {
                if (b === "yes" && props.onCancel) {
                    e.preventDefault();

                    props.onCancel();
                }
            } else if (b === "no" && props.onSubmit) {
                e.preventDefault();

                props.onSubmit();
            }
        }
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    autoSubmit.current.cb = props.onSubmit;

    return (
        <YesNoElement $alignment={props.styles.alignment} $margin={props.styles.margin}>
            <ButtonFabric
                styles={{
                    baseColor: props.yes.color || props.styles.yesColor,
                    mode: value === "yes" ? "fill" : "outline",
                    hover: "outline",
                    outlineSize: props.styles.outlineSize,
                    roundness: props.styles.roundness,
                    scale: props.styles.scale,
                    group: (props.styles.alignment !== "vertical" && "start") || undefined,
                }}
                label={props.yes.label}
                icon={props.yes.icon && getIcon(props.yes.icon)}
                iconPosition={"left"}
                tabIndex={props.tabIndex}
                ariaDescribedBy={props.ariaDescribedBy}
                disabled={
                    props.disabled || props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false
                }
                onAutoFocus={((!value || value === "yes") && props.onAutoFocus) || undefined}
                onFocus={props.onFocus}
                onBlur={props.onBlur}
                onKeyDown={(e) => handleKeyDown(e, "yes")}
                onClick={() => changeValue((props.required || value !== "yes") && "yes")}
            />
            <ButtonFabric
                styles={{
                    baseColor: props.no.color || props.styles.noColor,
                    mode: value === "no" ? "fill" : "outline",
                    hover: "outline",
                    outlineSize: props.styles.outlineSize,
                    roundness: props.styles.roundness,
                    scale: props.styles.scale,
                    group: (props.styles.alignment !== "vertical" && "end") || undefined,
                }}
                label={props.no.label}
                icon={props.no.icon && getIcon(props.no.icon)}
                iconPosition={"right"}
                tabIndex={props.tabIndex}
                ariaDescribedBy={props.ariaDescribedBy}
                disabled={
                    props.disabled || props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false
                }
                onAutoFocus={(value === "no" && props.onAutoFocus) || undefined}
                onFocus={props.onFocus}
                onBlur={props.onBlur}
                onKeyDown={(e) => handleKeyDown(e, "no")}
                onClick={() => changeValue((props.required || value !== "no") && "no")}
            />
        </YesNoElement>
    );
};
