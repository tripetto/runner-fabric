import { styled } from "styled-components";
import { DragEvent, FocusEvent, KeyboardEvent, ReactNode, useEffect, useRef, useState } from "react";
import { Num, Str, isNumberFinite } from "@tripetto/runner";
import { color } from "../../color";
import { ButtonFabric } from "../button";
import { warningIcon } from "../../icons/warning";
import { fileIcon } from "../../icons/file";
import { uploadIcon } from "../../icons/upload";

export interface IFileController {
    readonly isImage: boolean;
    readonly limit: number;
    readonly allowedExtensions: string[];
    readonly upload: (files: FileList, service: IFileService | undefined, onProgress: (percent: number) => void) => Promise<void>;
    readonly download: (service?: IFileService) => Promise<string>;
    readonly delete: (service?: IFileService) => Promise<void>;
    readonly fileSlot: {
        readonly hasValue: boolean;
        readonly string: string;
        readonly reference: string | undefined;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
    };
}

export interface IFileService {
    readonly get: (file: string) => Promise<Blob>;
    readonly put: (file: File, onProgress?: (percentage: number) => void) => Promise<string>;
    readonly delete: (file: string) => Promise<void>;
}

const FileElement = styled.div<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $roundness: number | undefined;
    $textColor: string | undefined;
    $errorColor: string;
    $errorVisible: boolean;
    $error: boolean;
    $disabled: boolean;
    $scale: number;
}>`
    outline: none;
    box-sizing: border-box;
    display: block;
    width: 100%;
    height: ${(props) => `${14 * props.$scale}em`};
    position: relative;
    overflow: hidden;
    font-size: 1em;
    line-height: 1.5em;
    background-color: ${(props) => color(props.$backgroundColor)};
    background-image: ${(props) =>
        props.$error
            ? `url("data:image/svg+xml;base64,${warningIcon(
                  color(props.$errorColor, (o) =>
                      o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                  )
              )}")`
            : undefined};
    background-repeat: no-repeat;
    background-size: ${8 / 7}em;
    background-position: right 0.375em top 0.375em;
    border: ${(props) =>
        `${props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? 0 : props.$borderSize}px solid ${color(
            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor
        )}`};
    color: ${(props) =>
        color(
            props.$textColor
                ? props.$textColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
        )};
    border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : "0.5em")};
    padding: ${(props) =>
        props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? "0" : `${0.375 * props.$scale}em 0.75em`};
    margin: 0;
    opacity: ${(props) => (props.$disabled ? 0.65 : 1)};
    transition:
        color 0.15s ease-in-out,
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out,
        opacity 0.15s ease-in-out;
    cursor: default;

    svg {
        fill: ${(props) =>
            color(
                props.$textColor
                    ? props.$textColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
            )};
        transition: fill 0.15s ease-in-out;
    }

    > div,
    > label {
        position: absolute;
        left: 0.75em;
        right: 0.75em;
        top: 0.75em;
        bottom: 0.75em;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    &:hover {
        box-shadow: ${(props) =>
            (!props.$disabled &&
                (props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                `0 0 0 0.2rem ${color(
                    props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                    (o) => o.manipulate((m) => m.alpha(0.2))
                )}`) ||
            undefined};
    }

    &:focus {
        background-color: ${(props) => (!props.$disabled && color(props.$backgroundColor)) || undefined};
        background-image: ${(props) => (!props.$errorVisible && "none") || undefined};
        border-color: ${(props) =>
            (!props.$disabled &&
                color(props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor)) ||
            undefined};
        color: ${(props) =>
            (!props.$disabled &&
                color(
                    props.$textColor
                        ? props.$textColor
                        : props.$borderColor && props.$borderColor !== "transparent"
                        ? props.$borderColor
                        : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                )) ||
            undefined};
        box-shadow: ${(props) =>
            (!props.$disabled &&
                (props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                `0 0 0 0.2rem ${color(
                    props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                    (o) => o.manipulate((m) => m.alpha(0.5))
                )}`) ||
            undefined};
    }
`;

const FileBrowseElement = styled.label<{
    $scale: number;
}>`
    cursor: pointer;

    * {
        pointer-events: none;
    }

    > svg {
        display: block;
        height: ${(props) => `${5 * props.$scale}em`};
        margin-bottom: 0.375em;
    }

    > input {
        width: 1px;
        height: 1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }

    > span {
        display: block;
        text-align: center;
    }

    > small {
        display: block;
        text-align: center;
    }
`;

const FileErrorElement = styled.div`
    > div {
        display: block;
        text-align: center;
    }

    > div:first-child {
        font-weight: bold;
    }

    > div:last-of-type {
        margin-bottom: 0.375em;
    }
`;

const FileProgressElement = styled.div<{
    $color: string;
}>`
    > div {
        display: inline-block;
        width: 80%;
        max-width: 300px;
        height: 16px;
        background-color: ${(props) => color(props.$color, (o) => o.manipulate((m) => m.alpha(0.25)))};
        border-radius: 4px;
        overflow: hidden;
        margin-bottom: 0.375em;

        > div {
            width: 0%;
            height: 16px;
            background-color: ${(props) => color(props.$color)};
            background-image: linear-gradient(
                45deg,
                ${(props) => color(props.$color, (o) => o.makeBlackOrWhite().manipulate((m) => m.alpha(0.25)))} 25%,
                transparent 25%,
                transparent 50%,
                ${(props) => color(props.$color, (o) => o.makeBlackOrWhite().manipulate((m) => m.alpha(0.25)))} 50%,
                ${(props) => color(props.$color, (o) => o.makeBlackOrWhite().manipulate((m) => m.alpha(0.25)))} 75%,
                transparent 75%,
                transparent
            );
            background-size: 1rem 1rem;
            transition: width 0.5s ease-out;
        }
    }

    > span {
        display: block;
        text-align: center;
    }
`;

const FilePreviewElement = styled.div<{
    $scale: number;
}>`
    > img,
    > svg {
        display: block;
        height: ${(props) => `${6 * props.$scale}em`};
        margin: 0.375em;
    }

    > div {
        display: block;
        text-align: center;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    > div:last-of-type {
        margin-bottom: 0.75em;
    }
`;

export const FileThumbnailFabric = (props: {
    readonly controller: IFileController;
    readonly service?: IFileService;
    readonly host?: (props: { children: ReactNode }) => JSX.Element;
    readonly error?: ReactNode;
    readonly loading?: ReactNode;
}) => {
    const [data, setData] = useState({
        loading: props.controller.isImage,
        base64data: "",
    });

    useEffect(() => {
        if (props.controller.isImage) {
            if (!data.loading) {
                setData({
                    loading: true,
                    base64data: "",
                });
            }

            props.controller
                .download(props.service)
                .then((base64data) =>
                    setData({
                        loading: false,
                        base64data,
                    })
                )
                .catch(() =>
                    setData({
                        loading: false,
                        base64data: "",
                    })
                );
        }
    }, [props.controller.fileSlot.reference]);

    if (data.loading) {
        return <>{props.loading}</>;
    }

    return (
        (data.base64data && (props.host ? <props.host children={<img src={data.base64data} />} /> : <img src={data.base64data} />)) || (
            <>{props.error}</>
        )
    );
};

export const FileFabric = (props: {
    readonly styles: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize?: number;
        readonly roundness?: number;
        readonly textColor?: string;
        readonly errorColor: string;
        readonly scale?: number;
    };
    readonly controller: IFileController;
    readonly labels: (
        id:
            | "explanation"
            | "dragging"
            | "limit"
            | "extensions"
            | "retry"
            | "progress"
            | "delete"
            | "invalid-file"
            | "invalid-amount"
            | "invalid-extension"
            | "invalid-size"
            | "error",
        message: string
    ) => string;
    readonly service?: IFileService;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly ariaDescribedBy?: string;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLDivElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const [dragging, setDragging] = useState(false);
    const [deleting, setDeleting] = useState(false);
    const [progress, setProgress] = useState(-1);
    const [error, setError] = useState<"invalid-amount" | "invalid-extension" | "invalid-size" | string>("");
    const [errorVisible, makeErrorVisible] = useState(false);
    const inputRef = useRef<HTMLInputElement | null>();
    const disabled = props.disabled || props.controller.fileSlot.isFrozen || props.controller.fileSlot.isLocked || false;
    const baseColor = props.styles.textColor
        ? props.styles.textColor
        : props.styles.borderColor && props.styles.borderColor !== "transparent"
        ? props.styles.borderColor
        : color(props.styles.backgroundColor || "transparent", (o) => o.makeBlackOrWhite());

    const handleUpload = (files: FileList) => {
        if (!disabled) {
            setProgress(0);
            makeErrorVisible(true);

            props.controller
                .upload(files, props.service, (percent) => setProgress(Num.floor(percent)))
                .then(() => setProgress(-1))
                .catch((err: "invalid-amount" | "invalid-extension" | "invalid-size" | string) => {
                    setError(err);
                    setProgress(-1);
                });
        }
    };

    const handleDelete = () => {
        if (props.controller.fileSlot.hasValue && !error && progress === -1) {
            setDeleting(true);
            makeErrorVisible(true);

            props.controller
                .delete(props.service)
                .then(() => setDeleting(false))
                .catch(() => setDeleting(false));
        }
    };

    return (
        <FileElement
            ref={props.onAutoFocus}
            tabIndex={props.tabIndex || 0}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            onKeyDown={(e: KeyboardEvent<HTMLDivElement>) => {
                if (e.key === "Enter") {
                    if (!e.shiftKey) {
                        if (!error && progress === -1) {
                            if (props.controller.fileSlot.hasValue && props.onSubmit) {
                                e.preventDefault();

                                props.onSubmit();
                            } else if (inputRef.current) {
                                e.preventDefault();

                                inputRef.current.click();
                            }
                        }
                    } else if (props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                } else if (e.key === "Escape") {
                    e.currentTarget.blur();
                } else if (e.key === "Tab") {
                    if (e.shiftKey) {
                        if (props.onCancel) {
                            e.preventDefault();

                            props.onCancel();
                        }
                    } else if (props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                } else if (e.key === "Delete" && props.controller.fileSlot.hasValue && !error && progress === -1) {
                    e.preventDefault();

                    handleDelete();
                }
            }}
            $backgroundColor={props.styles.backgroundColor || "transparent"}
            $borderColor={props.styles.borderColor || "transparent"}
            $borderSize={props.styles.borderSize || 1}
            $roundness={props.styles.roundness}
            $textColor={props.styles.textColor}
            $errorColor={props.styles.errorColor}
            $errorVisible={errorVisible}
            $error={props.error || false}
            $scale={props.styles.scale || 1}
            $disabled={disabled}
        >
            {!props.controller.fileSlot.hasValue && !error && progress === -1 && (
                <FileBrowseElement
                    onDragEnter={(e: DragEvent<HTMLLabelElement>) => {
                        e.preventDefault();
                        e.stopPropagation();

                        if (!disabled && progress === -1) {
                            setDragging(true);
                        }
                    }}
                    onDragOver={(e: DragEvent<HTMLLabelElement>) => {
                        e.preventDefault();
                        e.stopPropagation();
                    }}
                    onDragLeave={(e: DragEvent<HTMLLabelElement>) => {
                        e.preventDefault();
                        e.stopPropagation();

                        setDragging(false);
                    }}
                    onDrop={(e: DragEvent<HTMLLabelElement>) => {
                        e.preventDefault();
                        e.stopPropagation();

                        setDragging(false);

                        if (!disabled && progress === -1) {
                            const files = e.dataTransfer.files;

                            if (files) {
                                handleUpload(files);
                            }
                        }
                    }}
                    $scale={props.styles.scale || 1}
                >
                    {uploadIcon}
                    <input
                        ref={(el) => (inputRef.current = el)}
                        type="file"
                        multiple={false}
                        tabIndex={-1}
                        disabled={disabled}
                        aria-describedby={props.ariaDescribedBy}
                        onChange={(e) => {
                            if (e.target && e.target.files) {
                                handleUpload(e.target.files);
                            }
                        }}
                    />
                    <span>{props.labels(dragging ? "dragging" : "explanation", "")}</span>
                    {props.controller.limit > 0 && <small>{props.labels("limit", `${props.controller.limit}Mb`)}</small>}
                    {props.controller.allowedExtensions.length > 0 && (
                        <small>{props.labels("extensions", Str.iterateToString(props.controller.allowedExtensions, ", "))}</small>
                    )}
                </FileBrowseElement>
            )}
            {!error && progress !== -1 && (
                <FileProgressElement
                    onDragOver={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                    onDrop={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                    $color={baseColor}
                >
                    <div>
                        <div
                            style={{
                                width: `${Num.range(progress, 0, 100)}%`,
                            }}
                        ></div>
                    </div>
                    <span>{props.labels("progress", `${Num.range(progress, 0, 100)}%`)}</span>
                </FileProgressElement>
            )}
            {error && (
                <FileErrorElement
                    onDragOver={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                    onDrop={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                >
                    <div>{props.labels("invalid-file", "")}</div>
                    <div>
                        {error === "invalid-amount"
                            ? props.labels("invalid-amount", "")
                            : error === "invalid-extension"
                            ? props.labels("invalid-extension", "")
                            : error === "invalid-size"
                            ? props.labels("invalid-size", "")
                            : props.labels("error", error)}
                    </div>
                    <ButtonFabric
                        styles={{
                            baseColor,
                            mode: "outline",
                        }}
                        tabIndex={props.tabIndex || 0}
                        label={props.labels("retry", "")}
                        onClick={() => setError("")}
                    />
                </FileErrorElement>
            )}
            {props.controller.fileSlot.hasValue && !error && progress === -1 && (
                <FilePreviewElement
                    onDragOver={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                    onDrop={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                    $scale={props.styles.scale || 1}
                >
                    {props.controller.isImage ? (
                        <FileThumbnailFabric controller={props.controller} service={props.service} loading={fileIcon} error={fileIcon} />
                    ) : (
                        fileIcon
                    )}
                    <div>{props.controller.fileSlot.string}</div>
                    <ButtonFabric
                        styles={{
                            baseColor,
                            mode: "outline",
                        }}
                        tabIndex={props.tabIndex || 0}
                        label={props.labels("delete", "")}
                        disabled={deleting}
                        onClick={handleDelete}
                    />
                </FilePreviewElement>
            )}
        </FileElement>
    );
};
