import { styled } from "styled-components";
import { ChangeEvent, FocusEvent, KeyboardEvent, useEffect, useRef, useState } from "react";
import { DateTime, Num, TSerializeTypes, cancelUITimeout, castToString, isNumberFinite, scheduleUITimeout } from "@tripetto/runner";
import { color } from "../../color";
import { handleBlur, handleFocus, setReturnValue } from "../helpers";
import { warningIcon } from "../../icons/warning";
import { DEBOUNCE_MAX, DEBOUNCE_MIN } from "../const";

const TextareaElement = styled.textarea<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $roundness: number | undefined;
    $textColor: string | undefined;
    $errorColor: string;
    $errorVisible: boolean;
    $error: boolean;
    $scale: number;
}>`
    appearance: none;
    resize: none;
    outline: none;
    box-sizing: border-box;
    display: block;
    width: 100%;
    font-size: 1em;
    line-height: 1.5em;
    background-color: ${(props) =>
        color(
            props.$error && props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                ? props.$errorColor
                : props.$backgroundColor
        )};
    background-image: ${(props) =>
        props.$error
            ? `url("data:image/svg+xml;base64,${warningIcon(
                  color(props.$errorColor, (o) =>
                      o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                  )
              )}")`
            : undefined};
    background-repeat: no-repeat;
    background-size: ${8 / 7}em;
    background-position: right 0.375em top 0.375em;
    border: ${(props) =>
        `${props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? 0 : props.$borderSize}px solid ${color(
            props.$error
                ? props.$errorColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : props.$backgroundColor
        )}`};
    color: ${(props) =>
        color(
            props.$error
                ? props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                    ? color(props.$errorColor, (o) => o.makeBlackOrWhite())
                    : props.$errorColor
                : props.$textColor
                ? props.$textColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
        )};
    border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : "0.5em")};
    padding: ${(props) =>
        props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? "0" : `${0.375 * props.$scale}em 0.75em`};
    padding-right: ${(props) => (props.$error && `${8 / 7 + 0.75}em`) || undefined};
    margin: 0;
    opacity: 0.65;
    transition:
        color 0.15s ease-in-out,
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out,
        opacity 0.15s ease-in-out;
    overflow-y: auto;
    overflow-x: hidden;
    scroll-behavior: smooth;
    -ms-overflow-style: none;
    -webkit-overflow-scrolling: touch;
    -webkit-tap-highlight-color: transparent;

    &::-webkit-scrollbar {
        display: none;
    }

    &::placeholder {
        color: ${(props) =>
            color(
                props.$error
                    ? props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                        ? color(props.$errorColor, (o) => o.makeBlackOrWhite())
                        : props.$errorColor
                    : props.$textColor
                    ? props.$textColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
            )};
        opacity: 0.5;
        transition: color 0.15s ease-in-out opacity 0.15s ease-in-out;
    }

    &:not(:disabled):not([readonly]) {
        opacity: 1;

        &:hover {
            box-shadow: ${(props) =>
                ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                    `0 0 0 0.2rem ${color(
                        props.$error
                            ? props.$errorColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : props.$backgroundColor,
                        (o) => o.manipulate((m) => m.alpha(0.2))
                    )}`) ||
                undefined};
        }

        &:focus {
            background-color: ${(props) => color(props.$backgroundColor)};
            background-image: ${(props) => (!props.$errorVisible && "none") || undefined};
            border-color: ${(props) =>
                color(props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor)};
            color: ${(props) =>
                color(
                    props.$textColor
                        ? props.$textColor
                        : props.$borderColor && props.$borderColor !== "transparent"
                        ? props.$borderColor
                        : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                )};
            box-shadow: ${(props) =>
                ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                    `0 0 0 0.2rem ${color(
                        props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                        (o) => o.manipulate((m) => m.alpha(0.5))
                    )}`) ||
                undefined};
            &::placeholder {
                color: ${(props) =>
                    color(
                        props.$textColor
                            ? props.$textColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                    )};
                opacity: 0.3;
            }
        }
    }
`;

export const TextareaFabric = (props: {
    readonly styles: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize?: number;
        readonly roundness?: number;
        readonly textColor?: string;
        readonly errorColor: string;
        readonly scale?: number;
    };
    readonly id?: string;
    readonly placeholder?: string;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly rows?: number;
    readonly autoSize?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly maxLength?: number;
    readonly value?:
        | string
        | {
              pristine: TSerializeTypes;
              readonly string: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
          };
    readonly ariaDescribedBy?: string;
    readonly onChange?: (value: string) => string | void;
    readonly onFocus?: (e: FocusEvent) => string | void;
    readonly onBlur?: (e: FocusEvent) => string | void;
    readonly onAutoFocus?: (el: HTMLTextAreaElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<{
        duration: number;
        handle: number;
        update?: () => void;
    }>({
        duration: 0,
        handle: 0,
    });
    const [focus, setFocus] = useState(false);
    const [focusValue, setFocusValue] = useState(typeof valueRef === "object" ? castToString(valueRef.pristine) : valueRef || "");
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  focus ? focusValue : valueRef.string,
                  (val: string) => {
                      cancelUITimeout(debounceRef.current.handle);

                      setFocusValue(val);

                      const nTimeout = Num.range(debounceRef.current.duration * 2, DEBOUNCE_MIN, DEBOUNCE_MAX);

                      debounceRef.current.handle = scheduleUITimeout(() => {
                          const start = DateTime.precise;

                          debounceRef.current.handle = 0;
                          debounceRef.current.update = () => {
                              debounceRef.current.duration = DateTime.elapsed(start, true);
                              debounceRef.current.update = undefined;
                          };

                          valueRef.pristine = val || undefined;
                      }, nTimeout);
                  },
              ]
            : [focusValue, setFocusValue];
    const [errorVisible, makeErrorVisible] = useState(value ? true : false);
    const [height, setHeight] = useState("auto");
    const minHeight = useRef<number>();
    const ref = useRef<HTMLTextAreaElement | null>();
    const setRef = (el: HTMLTextAreaElement | null) => {
        if (props.onAutoFocus) {
            props.onAutoFocus(el);
        }

        ref.current = el;
    };

    useEffect(() => {
        if (ref.current) {
            if (!focus) {
                ref.current.value = value;
            }

            if (props.autoSize) {
                if (typeof minHeight.current !== "number") {
                    minHeight.current = ref.current.getBoundingClientRect().height;
                }

                const current = ref.current.style.height;

                ref.current.style.height = "auto";

                const n = Math.max(minHeight.current, ref.current.scrollHeight);

                setHeight(n > 10 ? n + "px" : "auto");

                ref.current.style.height = current;
            }
        }
    }, [value]);

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current.handle);
        };
    }, []);

    if (debounceRef.current.update) {
        debounceRef.current.update();
    }

    return (
        <TextareaElement
            id={props.id}
            ref={setRef}
            tabIndex={props.tabIndex}
            placeholder={props.placeholder}
            required={props.required || false}
            disabled={props.disabled || false}
            readOnly={props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false}
            rows={props.rows || 2}
            maxLength={props.maxLength || undefined}
            defaultValue={value}
            autoComplete="off"
            inputMode="text"
            aria-describedby={props.ariaDescribedBy}
            style={{
                height,
                overflowY: (props.autoSize && "hidden") || undefined,
            }}
            onChange={(e: ChangeEvent<HTMLTextAreaElement>) => {
                setValue(e.target.value);
                makeErrorVisible(true);

                if (props.onChange) {
                    setReturnValue(setValue, props.onChange(e.target.value));
                }
            }}
            onFocus={handleFocus(setFocus, setValue, props.onFocus)}
            onBlur={handleBlur(setFocus, setValue, props.onBlur)}
            onKeyDown={(e: KeyboardEvent<HTMLTextAreaElement>) => {
                if (e.shiftKey && e.key === "Enter" && props.onSubmit) {
                    e.preventDefault();

                    props.onSubmit();
                } else if (e.key === "Escape") {
                    e.currentTarget.blur();
                } else if (e.key === "Tab") {
                    if (e.shiftKey) {
                        if (props.onCancel) {
                            e.preventDefault();

                            props.onCancel();
                        }
                    } else if (props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                }
            }}
            $backgroundColor={props.styles.backgroundColor || "transparent"}
            $borderColor={props.styles.borderColor || "transparent"}
            $borderSize={props.styles.borderSize || 1}
            $roundness={props.styles.roundness}
            $textColor={props.styles.textColor}
            $errorColor={props.styles.errorColor}
            $errorVisible={errorVisible}
            $error={props.error || false}
            $scale={props.styles.scale || 1}
        />
    );
};
