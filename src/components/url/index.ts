import { FocusEvent } from "react";
import { TSerializeTypes } from "@tripetto/runner";
import { InputFabric } from "../input";
import { IInputFabricStyles } from "../input/styles";

export const URLFabric = (props: {
    readonly styles: IInputFabricStyles;
    readonly id?: string;
    readonly placeholder?: string;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly maxLength?: number;
    readonly value?:
        | string
        | {
              pristine: TSerializeTypes;
              readonly string: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
          };
    readonly ariaDescribedBy?: string;
    readonly onChange?: (value: string) => string | void;
    readonly onFocus?: (e: FocusEvent) => string | void;
    readonly onBlur?: (e: FocusEvent) => string | void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) =>
    InputFabric({
        type: "url",
        inputMode: "url",
        ...props,
        placeholder: props.placeholder || "https://",
    });
