import { styled } from "styled-components";
import { FocusEvent, useEffect, useRef, useState } from "react";
import { SHA2, TSerializeTypes, cancelUITimeout, findFirst, scheduleUITimeout } from "@tripetto/runner";
import { color } from "../../color";
import { DEBOUNCE_NORMAL } from "../const";

export interface IRadiobutton {
    readonly id: string;
    readonly name: string;
    readonly label?: string | JSX.Element;
    readonly description?: string | JSX.Element;
    readonly value?: string;
    readonly tabIndex?: number;
}

export const RadiobuttonElement = styled.div`
    display: block;
    width: 100%;
    margin: 0;
    padding: 0;
`;

export const RadiobuttonLabelElement = styled.label<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $textColor: string;
    $scale: number;
    $disabled: boolean;
    $label: boolean;
}>`
    box-sizing: border-box;
    appearance: none;
    outline: none;
    font-size: 1em;
    line-height: 1.6em;
    padding-left: calc(${(props) => (8 / 7) * props.$scale + 0.375}em + ${(props) => props.$borderSize * 2}px);
    margin: 0;
    position: relative;
    display: inline-flex;
    min-height: ${(props) => (8 / 7 + 2 * 0.095) * props.$scale}em;
    user-select: none;
    opacity: ${(props) => (props.$disabled && ".4") || undefined};
    transition: opacity 0.15s ease-in-out;

    > input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    > span {
        box-sizing: content-box;
        color: ${(props) =>
            color(
                props.$textColor
                    ? props.$textColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : props.$backgroundColor,
                (o) => o.makeUnclear("#000")
            )};
    }

    > span:last-child {
        position: absolute;
        width: ${(props) => (8 / 7) * props.$scale}em;
        min-width: ${(props) => (8 / 7) * props.$scale}em;
        height: ${(props) => (8 / 7) * props.$scale}em;
        min-height: ${(props) => (8 / 7) * props.$scale}em;
        top: ${(props) => (props.$label ? "0.095em" : "0em")};
        left: 0;
        background-color: ${(props) => color(props.$backgroundColor)};
        border: ${(props) =>
            `${props.$borderSize}px solid ${color(
                props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor
            )}`};
        border-radius: 50%;
        transition:
            background-color 0.15s ease-in-out,
            border-color 0.15s ease-in-out,
            box-shadow 0.15s ease-in-out,
            opacity 0.15s ease-in-out;

        &:after {
            width: ${(props) => (12 / 14) * props.$scale}em;
            height: ${(props) => (12 / 14) * props.$scale}em;
            left: ${(props) => (2 / 14) * props.$scale}em;
            top: ${(props) => (2 / 14) * props.$scale}em;
            border-radius: 50%;
            background-color: ${(props) =>
                color(props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor, (o) =>
                    o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                )};
            transform: scale(0.1);
            transition:
                transform 0.15s linear,
                opacity 0s linear 0.15s;
            content: "";
            position: absolute;
            opacity: 0;
        }
    }

    &:hover {
        > input:not(:disabled) + span + span {
            box-shadow: ${(props) =>
                ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                    `0 0 0 0.2rem ${color(
                        props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                        (o) => o.manipulate((m) => m.alpha(0.2))
                    )}`) ||
                undefined};
        }
    }

    > input {
        &:not(:disabled) {
            &:active + span {
                color: ${(props) =>
                    color(
                        props.$textColor
                            ? props.$textColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : props.$backgroundColor,
                        (o) => o.makeUnclear("#000")
                    )};
            }

            &:active + span + span {
                background-color: ${(props) => color(props.$backgroundColor)};
                border: ${(props) => props.$borderSize}px solid
                    ${(props) =>
                        color(props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor)};
                box-shadow: ${(props) =>
                    ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                        `0 0 0 0.1rem ${color(
                            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                            (o) => o.manipulate((m) => m.alpha(0.2))
                        )}`) ||
                    undefined};

                &:after {
                    background-color: ${(props) =>
                        color(
                            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                            (o) => o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                        )};
                }
            }

            &:focus + span {
                color: ${(props) =>
                    color(
                        props.$textColor
                            ? props.$textColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : props.$backgroundColor,
                        (o) => o.makeUnclear("#000")
                    )};
            }

            &:focus + span + span {
                background-color: ${(props) => color(props.$backgroundColor)};
                border: ${(props) => props.$borderSize}px solid
                    ${(props) =>
                        color(props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor)};
                box-shadow: ${(props) =>
                    ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                        `0 0 0 0.2rem ${color(
                            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                            (o) => o.manipulate((m) => m.alpha(0.5))
                        )}`) ||
                    undefined};

                &:after {
                    background-color: ${(props) =>
                        color(
                            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                            (o) => o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                        )};
                }
            }
        }

        &:checked + span + span {
            &:after {
                transform: none;
                opacity: 1;
                transition:
                    transform 0.15s linear,
                    opacity 0s linear;
            }
        }
    }
`;

const assertValue = (
    valueRef: {
        reference?: string;
        readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
    },
    props: {
        readonly buttons: IRadiobutton[];
    },
    reference?: string
) => {
    const selected = findFirst(props.buttons, (radiobutton) => radiobutton.id === reference);

    if (valueRef.reference !== selected?.id) {
        valueRef.set(selected && (selected.value || selected.name), selected?.id, selected?.name);
    }

    return (selected && selected.id) || "";
};

let RadiobuttonsCount = 0;

export const RadiobuttonsFabric = (props: {
    readonly styles: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize?: number;
        readonly textColor: string;
        readonly scale?: number;
    };
    readonly buttons: IRadiobutton[];
    readonly tabIndex?: number;
    readonly ariaDescribedBy?: string;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly allowUnselect?: boolean;
    readonly value?:
        | string
        | {
              reference?: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
          };
    readonly view?: "live" | "test" | "preview";
    readonly onChange?: (value: string) => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<number>(0);
    const [proxy, setProxy] = useState((typeof valueRef !== "object" && valueRef) || "");
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  debounceRef.current !== 0 ? proxy : assertValue(valueRef, props, valueRef.reference),
                  (reference: string) => {
                      cancelUITimeout(debounceRef.current);

                      setProxy(reference);

                      debounceRef.current = scheduleUITimeout(() => {
                          debounceRef.current = 0;

                          assertValue(valueRef, props, reference);
                      }, DEBOUNCE_NORMAL);
                  },
              ]
            : [proxy, setProxy];
    const [name] = useState(() => SHA2.SHA2_256("Radiobuttons" + RadiobuttonsCount++));

    const changeValue = (val: string) => {
        setValue(val);

        if (props.onChange) {
            props.onChange(val);
        }
    };

    const disabled =
        props.disabled || props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false;
    const styles = {
        $disabled: disabled,
        $color: props.styles.backgroundColor,
        $backgroundColor: props.styles.backgroundColor || "transparent",
        $borderColor:
            (props.styles.borderColor === "transparent" &&
                props.styles.backgroundColor === "transparent" &&
                color(props.styles.textColor, (o) => o.makeUnclear("#000"))) ||
            (color(props.styles.borderColor) !== color(props.styles.backgroundColor) && props.styles.borderColor) ||
            "transparent",
        $borderSize: props.styles.borderSize || 1,
        $textColor: props.styles.textColor,
        $scale: props.styles.scale || 1,
        $label: true,
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    return (
        <>
            {props.buttons.map(
                (button, index) =>
                    button.id &&
                    (props.view === "preview" || button.label || button.name || button.description) && (
                        <RadiobuttonElement key={button.id || index}>
                            <RadiobuttonLabelElement {...styles}>
                                <input
                                    ref={((value ? value === button.id : index === 0) && props.onAutoFocus) || undefined}
                                    type="radio"
                                    name={name}
                                    checked={value === button.id}
                                    tabIndex={button.tabIndex || props.tabIndex}
                                    aria-describedby={props.ariaDescribedBy}
                                    disabled={disabled}
                                    onChange={() => changeValue(button.id)}
                                    onClick={() => {
                                        if (props.allowUnselect && value === button.id) {
                                            changeValue("");
                                        }
                                    }}
                                    onFocus={props.onFocus}
                                    onBlur={props.onBlur}
                                    onKeyDown={(e) => {
                                        if (
                                            (props.allowUnselect || value !== button.id) &&
                                            (e.key === "Enter" || e.key === " ") &&
                                            !e.shiftKey
                                        ) {
                                            e.preventDefault();

                                            changeValue(value === button.id ? "" : button.id);

                                            return;
                                        }

                                        if (e.key === "Enter" && e.shiftKey && props.onSubmit) {
                                            props.onSubmit();
                                        } else if (e.key === "Escape") {
                                            e.currentTarget.blur();
                                        } else if (e.key === "Tab") {
                                            if (e.shiftKey) {
                                                if (props.onCancel) {
                                                    e.preventDefault();

                                                    props.onCancel();
                                                }
                                            } else if (props.onSubmit) {
                                                e.preventDefault();

                                                props.onSubmit();
                                            }
                                        }
                                    }}
                                />
                                <span>
                                    {button.label || button.name || "..."}
                                    {button.description && (
                                        <small>
                                            <br />
                                            {button.description}
                                        </small>
                                    )}
                                </span>
                                <span />
                            </RadiobuttonLabelElement>
                        </RadiobuttonElement>
                    )
            )}
        </>
    );
};
