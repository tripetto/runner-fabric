import { styled } from "styled-components";
import { ChangeEvent, FocusEvent, KeyboardEvent, MouseEvent, useRef, useState } from "react";
import { DateTime, Num, arrayItem, compare, each, eachReverse, filter, findFirst, isNumberFinite } from "@tripetto/runner";
import { handleBlur, handleFocus } from "../helpers";
import { color } from "../../color";
import { TOverlayContext } from "../../overlay";
import { dropdownIcon } from "../../icons/dropdown";
import { closeIcon } from "../../icons/close";
import { warningIcon } from "../../icons/warning";

export interface IMultiSelectOption {
    readonly id: string;
    readonly name: string;
    readonly label?: string | JSX.Element;
    readonly description?: string | JSX.Element;
    readonly disabled?: boolean;
    readonly value?: {
        value: boolean;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
        readonly confirm: () => void;
    };
}

const MultiSelectElement = styled.div<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $roundness: number | undefined;
    $textColor: string | undefined;
    $errorColor: string;
    $errorVisible: boolean;
    $error: boolean;
    $scale: number;
    $focus: boolean;
}>`
    box-sizing: border-box;
    display: block;
    width: 100%;
    font-size: 1em;
    line-height: 1.5em;
    background-color: ${(props) =>
        color(
            props.$error && props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                ? props.$errorColor
                : props.$backgroundColor
        )};
    background-image: ${(props) =>
        `url("data:image/svg+xml;base64,${dropdownIcon(
            color(
                props.$textColor
                    ? props.$textColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
            )
        )}")` +
        (props.$error
            ? `,url("data:image/svg+xml;base64,${warningIcon(
                  color(props.$errorColor, (o) =>
                      o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                  )
              )}")`
            : "")};
    background-repeat: no-repeat;
    background-size: 1em, ${8 / 7}em;
    background-position:
        right 0.375em center,
        right 1.75em center;
    border: ${(props) =>
        `${props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? 0 : props.$borderSize}px solid ${color(
            props.$error
                ? props.$errorColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : props.$backgroundColor
        )}`};
    border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : "0.5em")};
    padding: ${(props) =>
        props.$backgroundColor === "transparent" && props.$borderColor === "transparent"
            ? `0 1.75em 0 0`
            : `${(0.375 * props.$scale) / 2}em ${props.$error ? 1.75 + 8 / 7 + 0.375 : 1.75}em ${(0.375 * props.$scale) / 2}em ${
                  (5 / 14) * props.$scale
              }em`};
    margin: 0;
    opacity: 0.65;
    transition:
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out,
        opacity 0.15s ease-in-out;
    cursor: default;

    > div:first-child {
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        flex-direction: row;
    }

    &:not(:disabled) {
        opacity: 1;

        ${(props) =>
            props.$focus
                ? `
            background-color: ${color(props.$backgroundColor)};
            background-image: ${
                (!props.$errorVisible &&
                    `url("data:image/svg+xml;base64,${dropdownIcon(
                        color(
                            props.$textColor
                                ? props.$textColor
                                : props.$borderColor && props.$borderColor !== "transparent"
                                ? props.$borderColor
                                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                        )
                    )}")`) ||
                undefined
            };
            border-color: ${color(
                props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor
            )};
            box-shadow: ${
                ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                    `0 0 0 0.2rem ${color(
                        props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                        (o) => o.manipulate((m) => m.alpha(0.5))
                    )}`) ||
                undefined
            };
        `
                : `
            &:hover {
                box-shadow: ${
                    ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                        `0 0 0 0.2rem ${color(
                            props.$error
                                ? props.$errorColor
                                : props.$borderColor && props.$borderColor !== "transparent"
                                ? props.$borderColor
                                : props.$backgroundColor,
                            (o) => o.manipulate((m) => m.alpha(0.2))
                        )}`) ||
                    undefined
                };
            }
        `}
    }
`;

const MultiSelectInputElement = styled.input<{
    $backgroundColor: string;
    $borderColor: string;
    $textColor: string | undefined;
    $errorColor: string;
    $errorVisible: boolean;
    $error: boolean;
    $scale: number;
    $visible: boolean;
}>`
    flex: 1 1 0%;
    min-width: 60px;
    background-color: transparent;
    border: 0;
    outline: 0;
    padding: 0;
    font-size: 1em;
    line-height: 1.5em;
    color: ${(props) =>
        color(
            props.$error
                ? props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                    ? color(props.$errorColor, (o) => o.makeBlackOrWhite())
                    : props.$errorColor
                : props.$textColor
                ? props.$textColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
        )};
    appearance: none;
    overflow: hidden;
    padding: 0;
    margin: ${(props) => `${(0.375 * props.$scale) / 2}em 0`};
    opacity: ${(props) => (props.$visible ? "1" : "0")};

    &:first-child {
        margin-left: 0.375em;
    }

    &::placeholder {
        color: ${(props) =>
            color(
                props.$error
                    ? props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                        ? color(props.$errorColor, (o) => o.makeBlackOrWhite())
                        : props.$errorColor
                    : props.$textColor
                    ? props.$textColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
            )};
        opacity: 0.5;
        transition: color 0.15s ease-in-out opacity 0.15s ease-in-out;
    }

    &:focus {
        &::placeholder {
            color: ${(props) =>
                color(
                    props.$textColor
                        ? props.$textColor
                        : props.$borderColor && props.$borderColor !== "transparent"
                        ? props.$borderColor
                        : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                )};
            opacity: 0.3;
        }
    }

    &::-webkit-search-cancel-button {
        display: none;
    }
`;

const MultiSelectValueElement = styled.div<{
    $backgroundColor: string;
    $borderColor: string;
    $textColor: string | undefined;
    $errorColor: string;
    $errorVisible: boolean;
    $error: boolean;
    $roundness: number | undefined;
    $scale: number;
    $readOnly: boolean;
    $filled: boolean;
}>`
    margin: ${(props) => `${(0.375 * props.$scale) / 2}em 0.375em ${(0.375 * props.$scale) / 2}em 0`};
    padding-left: 0.5em;
    cursor: default;
    background-color: ${(props) =>
        props.$filled
            ? color(
                  props.$textColor
                      ? props.$textColor
                      : props.$borderColor && props.$borderColor !== "transparent"
                      ? props.$borderColor
                      : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
              )
            : undefined};
    border: ${(props) =>
        !props.$filled
            ? `1px solid ${color(
                  props.$textColor
                      ? props.$textColor
                      : props.$borderColor && props.$borderColor !== "transparent"
                      ? props.$borderColor
                      : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
              )}`
            : undefined};
    border-radius: ${(props) => (props.$roundness !== 0 ? `${props.$roundness || 5}px` : "0px")};
    color: ${(props) =>
        color(
            props.$textColor
                ? props.$textColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite()),
            (o) => (props.$filled ? o.makeBlackOrWhite() : o)
        )};
    display: flex;
    align-items: stretch;
    max-width: 50%;

    > div {
        font-size: 0.8em;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    > button {
        appearance: none;
        background-color: transparent;
        border: none;
        outline: 0;
        width: 1.6em;
        min-width: 1.6em;
        background-image: ${(props) =>
            `url("data:image/svg+xml;base64,${closeIcon(
                color(
                    props.$textColor
                        ? props.$textColor
                        : props.$borderColor && props.$borderColor !== "transparent"
                        ? props.$borderColor
                        : color(props.$backgroundColor, (o) => o.makeBlackOrWhite()),
                    (o) => (props.$filled ? o.makeBlackOrWhite() : o)
                )
            )}")`};
        background-repeat: no-repeat;
        background-size: 1em;
        background-position: center;
        opacity: ${(props) => (props.$readOnly ? "0.4" : "0.7")};

        &:hover {
            opacity: ${(props) => (props.$readOnly ? "0.4" : "1")};
        }
    }
`;

const MultiSelectOptionsElement = styled.div<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $roundness: number | undefined;
    $textColor: string | undefined;
}>`
    position: fixed;
    display: block;
    top: 0;
    left: 0;
    z-index: 2147483638;
    cursor: default;
    box-sizing: border-box;
    padding: ${(props) =>
        `calc(0.2rem + ${props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? 0 : props.$borderSize}px) 0`};

    > div {
        background-color: ${(props) => color(props.$backgroundColor !== "transparent" ? props.$backgroundColor : "#fff")};
        border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : "0.5em")};
        border: ${(props) =>
            `${
                (props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? 0 : props.$borderSize) || 1
            }px solid ${color(
                props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                (o) => o.makeUnclear(props.$textColor || "#000")
            )}`};
        overflow: auto;
        scroll-behavior: auto;
        scrollbar-width: none;
        -webkit-overflow-scrolling: touch;
        -webkit-tap-highlight-color: transparent;
        -ms-overflow-style: none;

        &::-webkit-scrollbar {
            display: none;
        }
    }
`;

const MultiSelectOptionElement = styled.div<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $roundness: number | undefined;
    $textColor: string | undefined;
    $selected: boolean;
    $disabled: boolean;
}>`
    display: flex;
    flex-direction: column;
    font-size: 1em;
    line-height: 1.5em;
    width: 100%;
    padding: 0.375em 0.75em;
    padding-right: 4em;
    opacity: ${(props) => (props.$disabled ? "0.5" : "1")};
    background-color: ${(props) =>
        props.$selected && !props.$disabled
            ? color(
                  props.$textColor
                      ? props.$textColor
                      : props.$borderColor && props.$borderColor !== "transparent"
                      ? props.$borderColor
                      : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
              )
            : "transparent"};
    color: ${(props) =>
        color(
            props.$textColor
                ? props.$textColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite()),
            (o) => (props.$selected && !props.$disabled ? o.makeBlackOrWhite() : o)
        )};

    > div:nth-child(2) {
        font-size: 0.8em;
        line-height: 1.3em;
    }
`;

export const MultiSelectFabric = (props: {
    readonly styles: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize?: number;
        readonly roundness?: number;
        readonly textColor?: string;
        readonly errorColor: string;
        readonly scale?: number;
        readonly pills?: "filled" | "outline";
        readonly overlayWidth?: "auto" | "full";
    };
    readonly overlay: TOverlayContext;
    readonly id: string;
    readonly options: IMultiSelectOption[];
    readonly placeholder?: string;
    readonly required?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly maxSelected?: number;
    readonly ariaDescribedBy?: string;
    readonly onChange?: (value: string) => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const selected = filter(props.options, (option) => (option.value?.value === true && option.name ? true : false));
    const [focus, setFocus] = useState(false);
    const [query, setQuery] = useState("");
    const [cursor, setCursor] = useState("");
    const [position, setPosition] = useState<{
        left: number;
        top: number;
        width: number;
        height: number;
    }>();
    const [errorVisible, makeErrorVisible] = useState(selected.length > 0 ? true : false);
    const scrollPosition = useRef({
        position: 0,
        time: 0,
    });
    const scrollIntoView = useRef(false);
    const ref = useRef<HTMLInputElement | null>();
    const componentRef = useRef<HTMLDivElement>(null);
    const optionsRef = useRef<HTMLDivElement>(null);
    const scrollToRef = useRef<HTMLDivElement>(null);
    const setRef = (el: HTMLInputElement | null) => {
        if (props.onAutoFocus && selected.length === 0) {
            props.onAutoFocus(el);
        }

        ref.current = el;
    };
    const options =
        (focus &&
            filter(
                props.options,
                (option) =>
                    (option.value?.value !== true &&
                        option.name &&
                        !option.disabled &&
                        (!query || option.name.toLowerCase().indexOf(query.toLowerCase()) !== -1)) ||
                    false
            )) ||
        [];

    const styles = {
        $backgroundColor: props.styles.backgroundColor || "transparent",
        $borderColor: props.styles.borderColor || "transparent",
        $borderSize: props.styles.borderSize || 1,
        $roundness: props.styles.roundness,
        $textColor: props.styles.textColor,
        $errorColor: props.styles.errorColor,
        $errorVisible: errorVisible,
        $error: props.error || false,
        $scale: props.styles.scale || 1,
        $placeholder: (props.placeholder && selected.length === 0 && (!focus || !query)) || false,
        $focus: focus,
    };

    const updateScrollPosition = () => {
        scrollPosition.current = {
            position: optionsRef.current?.firstElementChild?.scrollTop || 0,
            time: DateTime.now,
        };
    };

    if (!focus && cursor) {
        setCursor("");

        scrollIntoView.current = false;
    }

    if (!focus && position) {
        setPosition(undefined);
    }

    return (
        <MultiSelectElement
            ref={componentRef}
            onMouseDown={(e: MouseEvent<HTMLDivElement>) => {
                const tagName = (e.target as HTMLElement).tagName.toLowerCase();

                if (focus && tagName !== "button" && (tagName !== "input" || !query)) {
                    e.preventDefault();
                    e.stopPropagation();

                    if (ref.current) {
                        ref.current.blur();
                    }
                } else if (tagName !== "input") {
                    e.preventDefault();
                    e.stopPropagation();

                    if (ref.current) {
                        ref.current.focus();
                    }
                }
            }}
            role="combobox"
            aria-haspopup="listbox"
            aria-owns={(focus && `${props.id}-options`) || undefined}
            aria-controls={props.id}
            aria-expanded={focus}
            aria-describedby={props.ariaDescribedBy}
            {...styles}
        >
            <div>
                {selected.map((option, index) => (
                    <MultiSelectValueElement
                        key={option.id || index}
                        $filled={props.styles.pills !== "outline"}
                        $readOnly={option.disabled || option.value?.isFrozen || option.value?.isLocked || false}
                        {...styles}
                    >
                        <div>{option.label || option.name}</div>
                        <button
                            onClick={() => {
                                if (option.value && !option.disabled && !option.value?.isFrozen && !option.value?.isLocked) {
                                    option.value.value = false;

                                    updateScrollPosition();
                                    makeErrorVisible(true);
                                }
                            }}
                        />
                    </MultiSelectValueElement>
                ))}
                <MultiSelectInputElement
                    ref={setRef}
                    id={props.id}
                    type="search"
                    autoComplete="off"
                    spellCheck="false"
                    inputMode="text"
                    value={(focus && query) || ""}
                    placeholder={(!selected.length && props.placeholder) || ""}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => setQuery(e.target.value || "")}
                    onFocus={handleFocus(setFocus, setQuery, props.onFocus)}
                    onBlur={handleBlur(setFocus, setQuery, props.onBlur)}
                    onKeyDown={(e: KeyboardEvent<HTMLInputElement>) => {
                        if (e.key === "Enter" && cursor) {
                            e.preventDefault();

                            let index = 0;
                            const option = findFirst(options, (o) => {
                                index++;

                                return o.id === cursor;
                            });

                            if (
                                option &&
                                !option.disabled &&
                                option.value &&
                                !option.value.isFrozen &&
                                !option.value.isLocked &&
                                (!props.maxSelected || selected.length < props.maxSelected)
                            ) {
                                option.value.value = true;

                                updateScrollPosition();
                                setCursor(arrayItem(options, index)?.id || "");
                                makeErrorVisible(true);
                            }
                        } else if (e.key === "Escape") {
                            e.currentTarget.blur();
                        } else if (e.key === "Tab") {
                            e.currentTarget.blur();

                            if (e.shiftKey) {
                                if (props.onCancel) {
                                    e.preventDefault();

                                    props.onCancel();
                                }
                            } else if (props.onSubmit) {
                                e.preventDefault();

                                props.onSubmit();
                            }
                        } else if (e.key === "ArrowDown" || e.key === "ArrowUp" || e.key === "ArrowLeft" || e.key === "ArrowRight") {
                            scrollIntoView.current = true;

                            if (cursor) {
                                let found = false;
                                let next = "";

                                (e.key === "ArrowDown" || e.key === "ArrowRight" ? eachReverse : each)(options, (option) => {
                                    if (option.id === cursor) {
                                        found = true;
                                    } else if (!found) {
                                        next = option.id;
                                    }
                                });

                                if (found && next) {
                                    setCursor(next);

                                    return;
                                }
                            }

                            setCursor(arrayItem(options, e.key === "ArrowUp" || e.key === "ArrowLeft" ? options.length - 1 : 0)?.id || "");
                        } else if (e.key === "Backspace" && !query && selected.length > 0) {
                            const lastOption = selected[selected.length - 1];

                            if (lastOption.value) {
                                lastOption.value.value = false;

                                updateScrollPosition();
                                makeErrorVisible(true);
                            }
                        }
                    }}
                    $visible={focus || (!selected.length && props.placeholder) ? true : false}
                    {...styles}
                />
            </div>
            {focus && options.length > 0 && (
                <props.overlay
                    onEffect={() => {
                        const updatePosition = () => {
                            if (componentRef.current && optionsRef.current) {
                                const componentRect = componentRef.current.getBoundingClientRect();
                                const optionsRect = optionsRef.current.getBoundingClientRect();
                                const scrollRect = optionsRef.current.firstElementChild?.getBoundingClientRect() || optionsRect;
                                const viewportHeight =
                                    componentRef.current.ownerDocument.documentElement.clientHeight || window.innerHeight;
                                const availableHeight = viewportHeight - componentRect.bottom;
                                const positionAbove =
                                    (optionsRef.current.firstElementChild?.scrollHeight || 0) > availableHeight &&
                                    componentRect.top > availableHeight;
                                const newPosition = {
                                    left: optionsRect.left + (componentRect.left - optionsRect.left),
                                    top:
                                        optionsRect.top +
                                        (positionAbove
                                            ? componentRect.top - optionsRect.height - optionsRect.top + styles.$borderSize - 1
                                            : componentRect.bottom - optionsRect.top - styles.$borderSize + 1),
                                    width: componentRect.width,
                                    height: Num.max(
                                        (positionAbove ? componentRect.top : availableHeight) -
                                            Num.max(optionsRect.height - scrollRect.height, 0),
                                        120
                                    ),
                                };

                                if (!compare(position, newPosition, true)) {
                                    setPosition(newPosition);
                                }
                            }
                        };

                        if (!position) {
                            updatePosition();
                        }

                        if (DateTime.elapsed(scrollPosition.current.time) < 300 && optionsRef.current?.firstElementChild) {
                            optionsRef.current.firstElementChild.scrollTo(0, scrollPosition.current.position);
                        }

                        if (scrollToRef.current) {
                            scrollToRef.current.scrollIntoView({ behavior: "auto", block: "nearest" });

                            scrollIntoView.current = false;
                        }

                        if (componentRef.current) {
                            const resizeObserver = "ResizeObserver" in window && new ResizeObserver(() => updatePosition());
                            const componentRect = componentRef.current.getBoundingClientRect();
                            const currentPosition = componentRect.top;
                            const currentWidth = componentRect.width;
                            const scrollObserverFunc = () => {
                                if (componentRef.current) {
                                    const rect = componentRef.current.getBoundingClientRect();

                                    if (rect.top !== currentPosition || rect.width !== currentWidth) {
                                        updatePosition();
                                    } else {
                                        scrollObserver = requestAnimationFrame(scrollObserverFunc);
                                    }
                                }
                            };
                            let scrollObserver = requestAnimationFrame(scrollObserverFunc);

                            if (resizeObserver) {
                                resizeObserver.observe(componentRef.current);
                            }

                            return () => {
                                if (resizeObserver) {
                                    resizeObserver.disconnect();
                                }

                                cancelAnimationFrame(scrollObserver);
                            };
                        }

                        return undefined;
                    }}
                >
                    <MultiSelectOptionsElement
                        key={`${props.id}-options`}
                        ref={optionsRef}
                        id={`${props.id}-options`}
                        style={{
                            transform: `translate(${position?.left || 0}px,${position?.top || 0}px)`,
                            maxWidth: props.styles.overlayWidth !== "full" ? position?.width : undefined,
                            width: props.styles.overlayWidth === "full" ? position?.width : undefined,
                            opacity: position ? 1 : 0,
                        }}
                        {...styles}
                    >
                        <div
                            key={`${props.id}-list`}
                            style={{
                                maxHeight: position?.height,
                            }}
                        >
                            {options.map((option, index) => (
                                <MultiSelectOptionElement
                                    key={option.id || index}
                                    ref={(scrollIntoView.current && cursor === option.id && scrollToRef) || undefined}
                                    data-id={option.id}
                                    role="option"
                                    aria-selected={cursor === option.id}
                                    onMouseMove={() => setCursor(option.id)}
                                    onMouseDown={(e: MouseEvent<HTMLDivElement>) => {
                                        e.preventDefault();
                                        e.stopPropagation();
                                    }}
                                    onClick={() => {
                                        if (
                                            option.value &&
                                            !option.disabled &&
                                            !option.value.isFrozen &&
                                            !option.value.isLocked &&
                                            (!props.maxSelected || selected.length < props.maxSelected)
                                        ) {
                                            option.value.value = true;

                                            updateScrollPosition();
                                            setCursor(arrayItem(options, index + 1)?.id || "");
                                            makeErrorVisible(true);
                                        }
                                    }}
                                    $disabled={
                                        option.disabled ||
                                        option.value?.isFrozen ||
                                        option.value?.isLocked ||
                                        (props.maxSelected && selected.length >= props.maxSelected) ||
                                        false
                                    }
                                    $selected={cursor === option.id}
                                    {...styles}
                                >
                                    <div>{option.label || option.name}</div>
                                    {option.description && <div>{option.description}</div>}
                                </MultiSelectOptionElement>
                            ))}
                        </div>
                    </MultiSelectOptionsElement>
                </props.overlay>
            )}
        </MultiSelectElement>
    );
};
