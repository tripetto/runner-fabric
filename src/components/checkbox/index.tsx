import { styled } from "styled-components";
import { FocusEvent, useEffect, useRef, useState } from "react";
import { Num, cancelUITimeout, isNumberFinite, scheduleUITimeout } from "@tripetto/runner";
import { color } from "../../color";
import { RequiredIndicatorFabric } from "../required-indicator";
import { DEBOUNCE_NORMAL } from "../const";

export const CheckboxElement = styled.div`
    display: block;
    width: 100%;
    margin: 0;
    padding: 0;
`;

const CheckboxLabelElement = styled.label<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $roundness: number | undefined;
    $textColor: string;
    $errorColor: string;
    $error: boolean;
    $scale: number;
    $disabled: boolean;
}>`
    box-sizing: border-box;
    appearance: none;
    outline: none;
    font-size: 1em;
    line-height: 1.6em;
    padding-left: calc(${(props) => (8 / 7) * props.$scale + 0.375}em + ${(props) => props.$borderSize * 2}px);
    margin: 0;
    position: relative;
    display: inline-flex;
    min-height: ${(props) => (8 / 7 + 2 * 0.095) * props.$scale}em;
    user-select: none;
    opacity: ${(props) => (props.$disabled && ".4") || undefined};
    transition: opacity 0.15s ease-in-out;

    > input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    > span {
        box-sizing: content-box;
        color: ${(props) =>
            color(
                props.$error
                    ? props.$errorColor
                    : props.$textColor
                    ? props.$textColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : props.$backgroundColor,
                (o) => o.makeUnclear("#000")
            )};
    }

    > span:last-child {
        position: absolute;
        width: ${(props) => (8 / 7) * props.$scale}em;
        height: ${(props) => (8 / 7) * props.$scale}em;
        top: 0.095em;
        left: 0;
        background-color: ${(props) =>
            color(
                props.$error && props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                    ? props.$errorColor
                    : props.$backgroundColor
            )};
        border: ${(props) =>
            `${props.$borderSize}px solid ${color(
                props.$error
                    ? props.$errorColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : props.$backgroundColor
            )}`};
        border-radius: ${(props) =>
            isNumberFinite(props.$roundness) ? `${Num.min(props.$roundness, 4)}px` : `${(4 / 14) * props.$scale}em`};
        transition:
            background-color 0.15s ease-in-out,
            border-color 0.15s ease-in-out,
            box-shadow 0.15s ease-in-out,
            opacity 0.15s ease-in-out;

        &:after {
            width: ${(props) => (3 / 14) * props.$scale}em;
            height: ${(props) => (7 / 14) * props.$scale}em;
            left: ${(props) => (6 / 14) * props.$scale}em;
            top: ${(props) => (2 / 14) * props.$scale}em;
            border-style: solid;
            border-width: 0 ${(props) => (2 / 14) * props.$scale}em ${(props) => (2 / 14) * props.$scale}em 0;
            border-color: ${(props) =>
                color(
                    props.$error
                        ? props.$errorColor
                        : props.$borderColor && props.$borderColor !== "transparent"
                        ? props.$borderColor
                        : props.$backgroundColor,
                    (o) =>
                        o.makeBlackOrWhite(
                            props.$backgroundColor !== "transparent" && (props.$error || props.$borderColor === "transparent")
                        )
                )};
            transform: rotate(45deg) scale(0.1);
            transition:
                transform 0.15s linear,
                opacity 0s linear 0.15s;
            content: "";
            position: absolute;
            opacity: 0;
        }
    }

    &:hover {
        > input:not(:disabled) + span + span {
            box-shadow: ${(props) =>
                ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                    `0 0 0 0.2rem ${color(
                        props.$error
                            ? props.$errorColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : props.$backgroundColor,
                        (o) => o.manipulate((m) => m.alpha(0.2))
                    )}`) ||
                undefined};
        }
    }

    > input {
        &:not(:disabled) {
            &:active + span {
                color: ${(props) =>
                    color(
                        props.$textColor
                            ? props.$textColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : props.$backgroundColor,
                        (o) => o.makeUnclear("#000")
                    )};
            }

            &:active + span + span {
                background-color: ${(props) => color(props.$backgroundColor)};
                border: ${(props) => props.$borderSize}px solid
                    ${(props) =>
                        color(props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor)};
                box-shadow: ${(props) =>
                    ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                        `0 0 0 0.1rem ${color(
                            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                            (o) => o.manipulate((m) => m.alpha(0.2))
                        )}`) ||
                    undefined};

                &:after {
                    border-color: ${(props) =>
                        color(
                            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                            (o) => o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                        )};
                }
            }

            &:focus + span {
                color: ${(props) =>
                    color(
                        props.$textColor
                            ? props.$textColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : props.$backgroundColor,
                        (o) => o.makeUnclear("#000")
                    )};
            }

            &:focus + span + span {
                background-color: ${(props) => color(props.$backgroundColor)};
                border: ${(props) => props.$borderSize}px solid
                    ${(props) =>
                        color(props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor)};
                box-shadow: ${(props) =>
                    ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                        `0 0 0 0.2rem ${color(
                            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                            (o) => o.manipulate((m) => m.alpha(0.5))
                        )}`) ||
                    undefined};

                &:after {
                    border-color: ${(props) =>
                        color(
                            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                            (o) => o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                        )};
                }
            }
        }

        &:checked + span + span {
            &:after {
                transform: rotate(45deg);
                opacity: 1;
                transition:
                    transform 0.15s linear,
                    opacity 0s linear;
            }
        }
    }
`;

export const CheckboxFabric = (props: {
    readonly styles: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize?: number;
        readonly roundness?: number;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale?: number;
        readonly hideRequiredIndicator?: boolean;
    };
    readonly label?: string | JSX.Element;
    readonly description?: string | JSX.Element;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly tabSubmit?: boolean;
    readonly value?:
        | boolean
        | {
              value: boolean;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly confirm: () => void;
          };
    readonly ariaDescribedBy?: string;
    readonly onChange?: (value: boolean) => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<number>(0);
    const [proxy, setProxy] = useState((typeof valueRef !== "object" && valueRef) || false);
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  debounceRef.current !== 0 ? proxy : valueRef.value,
                  (val: boolean) => {
                      cancelUITimeout(debounceRef.current);

                      setProxy(val);

                      debounceRef.current = scheduleUITimeout(() => {
                          debounceRef.current = 0;

                          valueRef.value = val;
                      }, DEBOUNCE_NORMAL);
                  },
              ]
            : [proxy, setProxy];

    const changeValue = (val: boolean) => {
        setValue(val);

        if (props.onChange) {
            props.onChange(val);
        }
    };

    const disabled =
        props.disabled || props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false;

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    if (typeof valueRef === "object") {
        valueRef.confirm();
    }

    return (
        <CheckboxElement>
            <CheckboxLabelElement
                $disabled={disabled}
                $backgroundColor={props.styles.backgroundColor || "transparent"}
                $borderColor={
                    (props.styles.borderColor === "transparent" &&
                        props.styles.backgroundColor === "transparent" &&
                        color(props.styles.textColor, (o) => o.makeUnclear("#000"))) ||
                    (color(props.styles.borderColor) !== color(props.styles.backgroundColor) && props.styles.borderColor) ||
                    "transparent"
                }
                $borderSize={props.styles.borderSize || 1}
                $roundness={props.styles.roundness}
                $textColor={props.styles.textColor}
                $errorColor={props.styles.errorColor}
                $error={props.error || false}
                $scale={props.styles.scale || 1}
            >
                <input
                    ref={props.onAutoFocus}
                    type="checkbox"
                    checked={value}
                    tabIndex={props.tabIndex}
                    aria-describedby={props.ariaDescribedBy}
                    disabled={disabled}
                    onChange={(e) => changeValue(e.target.checked)}
                    onFocus={props.onFocus}
                    onBlur={props.onBlur}
                    onKeyDown={(e) => {
                        if (e.key === "Enter") {
                            e.preventDefault();

                            if (e.shiftKey && props.onSubmit) {
                                props.onSubmit();
                            } else {
                                changeValue(!value);
                            }
                        } else if (e.key === "Escape") {
                            e.currentTarget.blur();
                        } else if (e.key === "Tab") {
                            if (e.shiftKey) {
                                if (props.onCancel) {
                                    e.preventDefault();

                                    props.onCancel();
                                }
                            } else if ((props.tabSubmit || typeof props.tabSubmit !== "boolean") && props.onSubmit) {
                                e.preventDefault();

                                props.onSubmit();
                            }
                        }
                    }}
                />
                <span>
                    {props.label || "..."}
                    {props.required && !props.styles.hideRequiredIndicator && (
                        <RequiredIndicatorFabric $errorColor={props.styles.errorColor} />
                    )}
                    {props.description && (
                        <small>
                            <br />
                            {props.description}
                        </small>
                    )}
                </span>
                <span />
            </CheckboxLabelElement>
        </CheckboxElement>
    );
};
