import { FocusEvent } from "react";
import { CheckboxFabric } from "../checkbox";

export interface ICheckbox {
    readonly id: string;
    readonly label: string | JSX.Element;
    readonly description?: string | JSX.Element;
    readonly value?:
        | boolean
        | {
              value: boolean;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly confirm: () => void;
          };
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly tabIndex?: number;
    readonly error?: boolean;
    readonly onChange?: (value: boolean) => void;
}

export const CheckboxesFabric = (props: {
    readonly styles: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize?: number;
        readonly roundness?: number;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale?: number;
    };
    readonly checkboxes: ICheckbox[];
    readonly tabIndex?: number;
    readonly ariaDescribedBy?: string;
    readonly view?: "live" | "test" | "preview";
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => (
    <>
        {props.checkboxes.map(
            (checkbox, index) =>
                (props.view === "preview" || checkbox.label) && (
                    <CheckboxFabric
                        key={checkbox.id || index}
                        styles={props.styles}
                        label={checkbox.label}
                        description={checkbox.description}
                        required={checkbox.required}
                        disabled={checkbox.disabled}
                        readOnly={checkbox.readOnly}
                        error={checkbox.error}
                        tabIndex={checkbox.tabIndex || props.tabIndex}
                        tabSubmit={index + 1 === props.checkboxes.length}
                        value={checkbox.value}
                        ariaDescribedBy={props.ariaDescribedBy}
                        onChange={checkbox.onChange}
                        onAutoFocus={(index === 0 && props.onAutoFocus) || undefined}
                        onFocus={props.onFocus}
                        onBlur={props.onBlur}
                        onSubmit={props.onSubmit}
                        onCancel={(index === 0 && props.onCancel) || undefined}
                    />
                )
        )}
    </>
);
