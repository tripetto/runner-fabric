import { styled } from "styled-components";
import { ChangeEvent, FocusEvent, KeyboardEvent, useEffect, useRef, useState } from "react";
import { TSerializeTypes, cancelUITimeout, findFirst, isNumberFinite, scheduleUITimeout } from "@tripetto/runner";
import { color } from "../../color";
import { dropdownIcon } from "../../icons/dropdown";
import { warningIcon } from "../../icons/warning";
import { DEBOUNCE_NORMAL } from "../const";

export interface IDropdownFabricOption {
    readonly id: string;
    readonly name: string;
    readonly value?: string;
}

const SelectElement = styled.select<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $roundness: number | undefined;
    $textColor: string | undefined;
    $errorColor: string;
    $errorVisible: boolean;
    $error: boolean;
    $scale: number;
    $placeholder: boolean;
}>`
    appearance: none;
    outline: none;
    box-sizing: border-box;
    display: block;
    width: 100%;
    font-size: 1em;
    line-height: 1.5em;
    background-color: ${(props) =>
        color(
            props.$error && props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                ? props.$errorColor
                : props.$backgroundColor
        )};
    background-image: ${(props) =>
        `url("data:image/svg+xml;base64,${dropdownIcon(
            color(
                props.$textColor
                    ? props.$textColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
            )
        )}")` +
        (props.$error
            ? `,url("data:image/svg+xml;base64,${warningIcon(
                  color(props.$errorColor, (o) =>
                      o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                  )
              )}")`
            : "")};
    background-repeat: no-repeat;
    background-size: 1em, ${8 / 7}em;
    background-position:
        right 0.375em center,
        right 1.75em center;
    border: ${(props) =>
        `${props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? 0 : props.$borderSize}px solid ${color(
            props.$error
                ? props.$errorColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : props.$backgroundColor
        )}`};
    border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : "0.5em")};
    color: ${(props) =>
        color(
            props.$error
                ? props.$backgroundColor !== "transparent" && props.$borderColor === "transparent"
                    ? color(props.$errorColor, (o) => o.makeBlackOrWhite())
                    : props.$errorColor
                : props.$textColor
                ? props.$textColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite()),
            (o) => o.manipulate((m) => m.alpha(0.5), props.$placeholder && !props.$error)
        )};
    padding: ${(props) =>
        props.$backgroundColor === "transparent" && props.$borderColor === "transparent"
            ? `0 1.75em 0 0`
            : `${0.375 * props.$scale}em 1.75em ${0.375 * props.$scale}em 0.75em`};
    padding-right: ${(props) => (props.$error && `${1.75 + 8 / 7 + 0.375}em`) || undefined};
    margin: 0;
    opacity: 0.65;
    transition:
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out,
        opacity 0.15s ease-in-out;

    &::-ms-expand {
        display: none;
    }

    > option {
        background-color: ${(props) => color(props.$backgroundColor)};
        color: ${(props) =>
            color(
                props.$textColor
                    ? props.$textColor
                    : props.$borderColor && props.$borderColor !== "transparent"
                    ? props.$borderColor
                    : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
            )};
    }

    &:not(:disabled) {
        opacity: 1;

        &:hover {
            box-shadow: ${(props) =>
                ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                    `0 0 0 0.2rem ${color(
                        props.$error
                            ? props.$errorColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : props.$backgroundColor,
                        (o) => o.manipulate((m) => m.alpha(0.2))
                    )}`) ||
                undefined};
        }

        &:focus {
            background-color: ${(props) => color(props.$backgroundColor)};
            background-image: ${(props) =>
                (!props.$errorVisible &&
                    `url("data:image/svg+xml;base64,${dropdownIcon(
                        color(
                            props.$textColor
                                ? props.$textColor
                                : props.$borderColor && props.$borderColor !== "transparent"
                                ? props.$borderColor
                                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                        )
                    )}")`) ||
                undefined};
            border-color: ${(props) =>
                color(props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor)};
            color: ${(props) =>
                color(
                    props.$textColor
                        ? props.$textColor
                        : props.$borderColor && props.$borderColor !== "transparent"
                        ? props.$borderColor
                        : color(props.$backgroundColor, (o) => o.makeBlackOrWhite()),
                    (o) => o.manipulate((m) => m.alpha(0.5), props.$placeholder)
                )};
            box-shadow: ${(props) =>
                ((props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                    `0 0 0 0.2rem ${color(
                        props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                        (o) => o.manipulate((m) => m.alpha(0.5))
                    )}`) ||
                undefined};
        }
    }
`;

const PlaceholderElement = styled.option<{
    $backgroundColor: string;
    $borderColor: string;
    $textColor: string | undefined;
    $errorColor: string;
    $error: boolean;
}>`
    color: ${(props) =>
        color(
            props.$textColor
                ? props.$textColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite()),
            (o) => o.manipulate((m) => m.alpha(0.5))
        )} !important;
`;

const assertValue = (
    valueRef: {
        reference?: string;
        readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
        readonly default: (value: TSerializeTypes, reference?: string, display?: string) => void;
    },
    props: {
        readonly options: IDropdownFabricOption[];
        readonly placeholder?: string;
    },
    reference?: string
) => {
    const selected = findFirst(props.options, (option) => option.id === reference);

    if (!selected && !props.placeholder && props.options.length > 0) {
        const defaultOption = findFirst(props.options, (option) => (option.name ? true : false));

        if (defaultOption) {
            valueRef.default(defaultOption.value || defaultOption.name, defaultOption.id, defaultOption.name);
        }
    } else if (valueRef.reference !== selected?.id) {
        valueRef.set(selected && (selected.value || selected.name), selected?.id, selected?.name);
    }

    return (selected && selected.id) || "";
};

export const DropdownFabric = (props: {
    readonly styles: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize?: number;
        readonly roundness?: number;
        readonly textColor?: string;
        readonly errorColor: string;
        readonly scale?: number;
    };
    readonly id?: string;
    readonly options: IDropdownFabricOption[];
    readonly placeholder?: string;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly value?:
        | string
        | {
              reference?: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
              readonly default: (value: TSerializeTypes, reference?: string, display?: string) => void;
          };
    readonly ariaDescribedBy?: string;
    readonly onChange?: (value: string) => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLSelectElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<number>(0);
    const [proxy, setProxy] = useState((typeof valueRef !== "object" && valueRef) || "");
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  debounceRef.current !== 0 ? proxy : assertValue(valueRef, props, valueRef.reference),
                  (reference: string) => {
                      cancelUITimeout(debounceRef.current);

                      setProxy(reference);

                      debounceRef.current = scheduleUITimeout(() => {
                          debounceRef.current = 0;

                          assertValue(valueRef, props, reference);
                      }, DEBOUNCE_NORMAL);
                  },
              ]
            : [proxy, setProxy];
    const [errorVisible, makeErrorVisible] = useState(value ? true : false);

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    return (
        <SelectElement
            id={props.id}
            ref={props.onAutoFocus}
            tabIndex={props.tabIndex}
            required={props.required || false}
            disabled={
                props.disabled || props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false
            }
            value={value}
            aria-describedby={props.ariaDescribedBy}
            onChange={(e: ChangeEvent<HTMLSelectElement>) => {
                setValue(e.target.value);
                makeErrorVisible(true);

                if (props.onChange) {
                    props.onChange(e.target.value);
                }
            }}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            onKeyDown={(e: KeyboardEvent<HTMLSelectElement>) => {
                if (e.shiftKey && e.key === "Enter" && props.onSubmit) {
                    e.preventDefault();

                    props.onSubmit();
                } else if (e.key === "Escape") {
                    e.currentTarget.blur();
                } else if (e.key === "Tab") {
                    if (e.shiftKey) {
                        if (props.onCancel) {
                            e.preventDefault();

                            props.onCancel();
                        }
                    } else if (props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                }
            }}
            $backgroundColor={props.styles.backgroundColor || "transparent"}
            $borderColor={props.styles.borderColor || "transparent"}
            $borderSize={props.styles.borderSize || 1}
            $roundness={props.styles.roundness}
            $textColor={props.styles.textColor}
            $errorColor={props.styles.errorColor}
            $errorVisible={errorVisible}
            $error={props.error || false}
            $scale={props.styles.scale || 1}
            $placeholder={(props.placeholder && value === "") || false}
        >
            {props.placeholder && (
                <PlaceholderElement
                    value=""
                    $backgroundColor={props.styles.backgroundColor || "transparent"}
                    $borderColor={props.styles.borderColor || "transparent"}
                    $textColor={props.styles.textColor}
                    $errorColor={props.styles.errorColor}
                    $error={props.error || false}
                >
                    {props.placeholder}
                </PlaceholderElement>
            )}
            {props.options.map(
                (option, index) =>
                    option.name &&
                    option.id && (
                        <option key={index} value={option.id}>
                            {option.name}
                        </option>
                    )
            )}
        </SelectElement>
    );
};
