export const DEBOUNCE_NORMAL = 1000 / 20;
export const DEBOUNCE_MIN = 1000 / 60;
export const DEBOUNCE_MAX = 1000;
