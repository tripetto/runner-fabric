import { styled } from "styled-components";
import { color } from "../../color";

export const RequiredIndicatorFabric = styled.span<{
    $errorColor: string;
}>`
    color: ${(props) => color(props.$errorColor)};
    position: relative;
    left: 0.1em;
    top: 0.18em;
    text-transform: uppercase;
    font-size: 1.4em;
    line-height: 0.5em;

    &:after {
        content: "*";
    }
`;
