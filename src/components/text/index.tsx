import { FocusEvent, useState } from "react";
import { SHA2, TSerializeTypes } from "@tripetto/runner";
import { InputFabric } from "../input";
import { IInputFabricStyles } from "../input/styles";

let TextCount = 0;

export const TextFabric = (props: {
    readonly styles: IInputFabricStyles;
    readonly id?: string;
    readonly placeholder?: string;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly maxLength?: number;
    readonly value?:
        | string
        | {
              pristine: TSerializeTypes;
              readonly string: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
          };
    readonly ariaDescribedBy?: string;
    readonly autoComplete?: string;
    readonly suggestions?: string[];
    readonly onChange?: (value: string) => string | void;
    readonly onFocus?: (e: FocusEvent) => string | void;
    readonly onBlur?: (e: FocusEvent) => string | void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const [id] = useState(() => SHA2.SHA2_256("Text" + TextCount++));

    return (
        <>
            <InputFabric list={props.suggestions && id} type="text" inputMode={props.autoComplete === "tel" ? "tel" : "text"} {...props} />
            {props.suggestions && (
                <datalist id={id}>
                    {props.suggestions.map((suggestion, index) => suggestion && <option key={index} value={suggestion} />)}
                </datalist>
            )}
        </>
    );
};
