import { styled } from "styled-components";
import { FocusEvent, useEffect, useRef, useState } from "react";
import { TSerializeTypes, arrayItem, cancelUITimeout, castToBoolean, each, findFirst, scheduleUITimeout } from "@tripetto/runner";
import { handleAutoSubmit } from "../helpers";
import { ButtonFabric } from "../button";
import { color } from "../../color";
import { placeholderImage } from "../../icons/placeholder";
import { DEBOUNCE_NORMAL } from "../const";

export interface IPictureChoiceOption {
    readonly id: string;
    readonly image?: string;
    readonly emoji?: string;
    readonly name?: string;
    readonly nameVisible?: boolean;
    readonly value?: string;
    readonly label?: string | JSX.Element;
    readonly description?: string | JSX.Element;
    readonly url?: string;
    readonly target?: "self" | "blank";
    readonly disabled?: boolean;
    readonly slot?: {
        value: boolean;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
        readonly confirm: () => void;
    };
    readonly tabIndex?: number;
    readonly color?: string;
    readonly onChange?: (selected: boolean) => void;
}

const PictureChoiceElement = styled.div`
    display: flex;
    width: 100%;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: stretch;

    > * {
        margin-top: 8px;
        margin-right: 8px;
        display: flex;
        flex-direction: column;
        align-items: center;
    }
`;

const assertValue = (
    valueRef: {
        reference?: string;
        readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
    },
    options: IPictureChoiceOption[],
    reference?: string
) => {
    const selected = findFirst(options, (option) => option.id === reference);

    if (valueRef.reference !== selected?.id) {
        valueRef.set(selected && (selected.value || selected.name), selected?.id, selected?.name);
    }

    return (selected && selected.id) || "";
};

export const PictureChoiceFabric = (props: {
    readonly styles: {
        readonly color: string;
        readonly outlineSize?: number;
        readonly roundness?: number;
        readonly scale?: number;
        readonly margin?: number;
    };
    readonly options: IPictureChoiceOption[];
    readonly size?: "small" | "medium" | "large";
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly ariaDescribedBy?: string;
    readonly tabIndex?: number;
    readonly value?:
        | string
        | {
              reference?: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
          };
    readonly autoSubmit?: boolean;
    readonly view?: "live" | "test" | "preview";
    readonly onChange?: (value: string) => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLButtonElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<number>(0);
    const optionsRef = useRef<{
        [option: string]: boolean;
    }>({});
    const [proxy, setProxy] = useState((typeof valueRef !== "object" && valueRef) || "");
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  debounceRef.current !== 0 ? proxy : assertValue(valueRef, props.options, valueRef.reference),
                  (reference: string) => {
                      cancelUITimeout(debounceRef.current);

                      setProxy(reference);

                      debounceRef.current = scheduleUITimeout(() => {
                          debounceRef.current = 0;

                          assertValue(valueRef, props.options, reference);

                          if (props.autoSubmit && reference) {
                              handleAutoSubmit(autoSubmit);
                          }
                      }, DEBOUNCE_NORMAL);
                  },
              ]
            : [proxy, setProxy];
    const [, update] = useState({});
    const autoSubmit = useRef({
        id: 0,
        cb: props.onSubmit,
    });

    const toggle = (option: IPictureChoiceOption) => {
        if (option.url) {
            return;
        }

        const isSelected = option.slot ? castToBoolean(optionsRef.current[option.id], option.slot.value) : value === option.id;

        if (option.slot) {
            cancelUITimeout(debounceRef.current);

            optionsRef.current[option.id] = !isSelected;

            update({});

            debounceRef.current = scheduleUITimeout(() => {
                debounceRef.current = 0;

                each(
                    optionsRef.current,
                    (val, id: string) => {
                        const changedOption = findFirst(props.options, (ref) => ref.id === id);

                        delete optionsRef.current[id];

                        if (changedOption && changedOption.slot) {
                            changedOption.slot.value = val;
                        }
                    },
                    {
                        keys: true,
                    }
                );
            }, DEBOUNCE_NORMAL);
        } else {
            const val = isSelected && !props.required ? "" : option.id;

            if (autoSubmit.current.id) {
                clearTimeout(autoSubmit.current.id);

                autoSubmit.current.id = 0;
            }

            setValue(val);

            if (props.onChange) {
                props.onChange(val);
            }

            if (typeof valueRef !== "object" && props.autoSubmit && val) {
                handleAutoSubmit(autoSubmit);
            }
        }

        if (option.onChange) {
            option.onChange(!option.slot && props.required ? true : !isSelected);
        }
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    autoSubmit.current.cb = props.onSubmit;

    return (
        <PictureChoiceElement>
            {props.options.map((option, index) => {
                const isSelected = option.slot ? castToBoolean(optionsRef.current[option.id], option.slot.value) : value === option.id;

                if (option.slot) {
                    option.slot.confirm();
                }

                return (
                    (props.view === "preview" ||
                        option.image ||
                        option.emoji ||
                        (option.nameVisible && option.name) ||
                        option.description) && (
                        <ButtonFabric
                            key={option.id || index}
                            styles={{
                                baseColor: option.color || props.styles.color,
                                mode: isSelected ? "fill" : "outline",
                                hover: "outline",
                                outlineSize: props.styles.outlineSize,
                                roundness: props.styles.roundness,
                                scale: props.styles.scale,
                            }}
                            hyperlink={
                                (option.url && {
                                    url: option.url,
                                    target: option.target || "blank",
                                }) ||
                                undefined
                            }
                            image={
                                option.image ||
                                (!option.emoji &&
                                    `data:image/svg+xml;base64,${placeholderImage(
                                        color(props.styles.color, (o) => o.makeBlackOrWhite(isSelected).manipulate((m) => m.alpha(0.2)))
                                    )}`) ||
                                undefined
                            }
                            emoji={option.emoji}
                            label={(option.nameVisible && (option.label || option.name)) || undefined}
                            description={option.description}
                            size={props.size}
                            tabIndex={option.tabIndex || props.tabIndex}
                            ariaDescribedBy={props.ariaDescribedBy}
                            disabled={
                                option.disabled ||
                                props.disabled ||
                                props.readOnly ||
                                (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) ||
                                (option.slot && (option.slot.isFrozen || option.slot.isLocked)) ||
                                false
                            }
                            onAutoFocus={((option.slot || !value ? index === 0 : isSelected) && props.onAutoFocus) || undefined}
                            onFocus={props.onFocus}
                            onBlur={props.onBlur}
                            onKeyDown={(e) => {
                                if (e.shiftKey && e.key === "Enter" && props.onSubmit) {
                                    e.preventDefault();

                                    props.onSubmit();
                                } else if (e.key === "Escape") {
                                    e.currentTarget.blur();
                                } else if (e.key === "Tab") {
                                    if (e.shiftKey) {
                                        if (props.onCancel && index === 0) {
                                            e.preventDefault();

                                            props.onCancel();
                                        }
                                    } else if (props.onSubmit && index + 1 === props.options.length) {
                                        e.preventDefault();

                                        props.onSubmit();
                                    }
                                } else {
                                    const keyCode = (e.key.length === 1 && e.key.charCodeAt(0)) || 0;
                                    const offset =
                                        (keyCode <= 57 ? keyCode - 48 : 0) ||
                                        (keyCode <= 90 ? keyCode - 64 : 0) ||
                                        (keyCode <= 122 ? keyCode - 96 : 0);

                                    if (offset > 0 && offset <= 26) {
                                        const toggleOption = arrayItem(props.options, offset - 1);

                                        if (toggleOption) {
                                            toggle(toggleOption);
                                        }
                                    }
                                }
                            }}
                            onClick={() => toggle(option)}
                        />
                    )
                );
            })}
        </PictureChoiceElement>
    );
};
