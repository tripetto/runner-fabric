export interface IInputFabricStyles {
    readonly backgroundColor: string;
    readonly borderColor: string;
    readonly borderSize?: number;
    readonly roundness?: number;
    readonly textColor?: string;
    readonly errorColor: string;
    readonly scale?: number;
}
