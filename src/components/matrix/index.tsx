import { styled } from "styled-components";
import { FocusEvent, useEffect, useRef, useState } from "react";
import { SHA2, TSerializeTypes, cancelUITimeout, castToString, each, findFirst, scheduleUITimeout } from "@tripetto/runner";
import { color } from "../../color";
import { RadiobuttonLabelElement } from "../radiobuttons";
import { RequiredIndicatorFabric } from "../required-indicator";
import { DEBOUNCE_NORMAL } from "../const";

export interface IMatrixColumn {
    readonly id: string;
    readonly name: string;
    readonly label?: string | JSX.Element;
    readonly value?: string;
}

export interface IMatrixRow {
    readonly id: string;
    readonly label: string | JSX.Element;
    readonly alias?: string;
    readonly explanation?: string | JSX.Element;
    readonly required?: boolean;
    readonly tabIndex?: number;
    readonly value?:
        | string
        | {
              reference?: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
          };
    readonly onChange?: (id: string) => void;
}

const MatrixContainerElement = styled.div`
    overflow-x: auto;
    scroll-behavior: smooth;
    scrollbar-width: none;
    -ms-overflow-style: none;
    -webkit-overflow-scrolling: touch;
    -webkit-tap-highlight-color: transparent;

    &::-webkit-scrollbar {
        display: none;
    }
`;

const MatrixElement = styled.table<{
    $textColor: string;
    $scale: number;
}>`
    display: table;
    width: 100%;
    border-collapse: collapse;
    font-size: 1em;
    line-height: 1.5em;

    > thead > tr {
        > th {
            text-align: center;
            vertical-align: bottom;
            font-weight: normal;
            color: ${(props) => props.$textColor};
            background-color: ${(props) => color(props.$textColor, (o) => o.manipulate((m) => m.alpha(0.2)))};
            border-bottom: 1px solid ${(props) => color(props.$textColor, (o) => o.manipulate((m) => m.alpha(0.2)))};
            cursor: default;
            padding-top: 0.5em;
            padding-bottom: 0.5em;

            &:first-child {
                background-color: transparent;
            }
        }

        > th + th {
            padding-left: 8px;
            padding-right: 8px;
        }
    }

    > tbody {
        > tr {
            > th {
                text-align: left;
                font-weight: normal;
                padding-right: 8px;
                padding-top: 0.5em;
                padding-bottom: 0.5em;
                color: ${(props) => props.$textColor};
                border-bottom: 1px solid ${(props) => color(props.$textColor, (o) => o.manipulate((m) => m.alpha(0.2)))};
                cursor: default;
                min-width: 50%;

                > small {
                    opacity: 0.8;
                }
            }

            > td {
                position: relative;
                border-bottom: 1px solid ${(props) => color(props.$textColor, (o) => o.manipulate((m) => m.alpha(0.15)))};

                > label {
                    display: flex;
                    justify-content: center;
                    position: absolute;
                    left: 0;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    padding-left: 0;

                    > span:nth-child(2) {
                        position: absolute;
                    }

                    > span:last-child {
                        position: relative;
                        align-self: center;
                    }
                }
            }
        }

        > tr:last-child {
            > th,
            > td {
                border-bottom: none;
            }
        }
    }
`;

let MatrixCount = 0;

export const MatrixFabric = (props: {
    readonly styles: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize?: number;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale?: number;
        readonly hideRequiredIndicator?: boolean;
    };
    readonly columns: IMatrixColumn[];
    readonly rows: IMatrixRow[];
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly allowUnselect?: boolean;
    readonly ariaDescribedBy?: string;
    readonly tabIndex?: number;
    readonly required?: boolean;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const [name] = useState(() => SHA2.SHA2_256("Matrix" + MatrixCount++));
    const debounceRef = useRef<number>(0);
    const rowsRef = useRef<{
        [row: string]: string;
    }>({});
    const [, update] = useState({});
    const styles = {
        $disabled: props.disabled || props.readOnly || false,
        $color: props.styles.backgroundColor,
        $backgroundColor: (props.styles.backgroundColor !== props.styles.borderColor && props.styles.backgroundColor) || "transparent",
        $borderColor:
            (props.styles.borderColor === "transparent" &&
                props.styles.backgroundColor === "transparent" &&
                color(props.styles.textColor, (o) => o.makeUnclear("#000"))) ||
            (color(props.styles.borderColor) !== color(props.styles.backgroundColor) && props.styles.borderColor) ||
            "transparent",
        $borderSize: props.styles.borderSize || 1,
        $textColor: props.styles.textColor,
        $errorColor: props.styles.errorColor,
        $scale: props.styles.scale || 1,
        $label: false,
    };

    const changeValue = (row: IMatrixRow, column: IMatrixColumn | undefined) => {
        if (typeof row.value === "object") {
            cancelUITimeout(debounceRef.current);

            rowsRef.current[row.id] = column ? column.id : "";

            update({});

            debounceRef.current = scheduleUITimeout(() => {
                debounceRef.current = 0;

                each(
                    rowsRef.current,
                    (columnId, rowId: string) => {
                        const changedRow = findFirst(props.rows, (ref) => ref.id === rowId);

                        delete rowsRef.current[rowId];

                        if (changedRow && typeof changedRow.value === "object") {
                            const selectedColumn = (columnId && findFirst(props.columns, (ref) => ref.id === columnId)) || undefined;

                            changedRow.value.set(
                                selectedColumn && (selectedColumn.value || selectedColumn.name),
                                selectedColumn?.id,
                                selectedColumn?.name
                            );
                        }
                    },
                    {
                        keys: true,
                    }
                );
            }, DEBOUNCE_NORMAL);
        }

        if (row.onChange) {
            row.onChange((column && column.id) || "");
        }
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    return (
        <MatrixContainerElement>
            <MatrixElement {...styles}>
                <thead>
                    <tr>
                        <th scope="col" />
                        {props.columns.map((column, index) => (
                            <th key={index} scope="col">
                                {column.label || column.name || "..."}
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {props.rows.map((row, rowIndex) => (
                        <tr key={rowIndex}>
                            <th scope="row">
                                {row.label || "..."}
                                {(row.required || props.required) && !props.styles.hideRequiredIndicator && (
                                    <RequiredIndicatorFabric $errorColor={props.styles.errorColor} />
                                )}
                                {row.explanation && (
                                    <small>
                                        <br />
                                        {row.explanation}
                                    </small>
                                )}
                            </th>
                            {props.columns.map((column: IMatrixColumn, columnIndex: number) => {
                                const checked =
                                    (typeof row.value === "object"
                                        ? castToString(rowsRef.current[row.id], row.value.reference || "")
                                        : row.value) === column.id;

                                return (
                                    <td key={rowIndex + "-" + columnIndex}>
                                        <RadiobuttonLabelElement {...styles}>
                                            <input
                                                ref={(rowIndex === 0 && columnIndex === 0 && props.onAutoFocus) || undefined}
                                                type="radio"
                                                name={name + rowIndex}
                                                checked={checked}
                                                tabIndex={row.tabIndex || props.tabIndex}
                                                aria-describedby={props.ariaDescribedBy}
                                                disabled={
                                                    props.disabled ||
                                                    props.readOnly ||
                                                    (typeof row.value === "object" && (row.value.isFrozen || row.value.isLocked)) ||
                                                    false
                                                }
                                                onChange={() => changeValue(row, column)}
                                                onClick={() => {
                                                    if (props.allowUnselect && checked) {
                                                        changeValue(row, undefined);
                                                    }
                                                }}
                                                onFocus={props.onFocus}
                                                onBlur={props.onBlur}
                                                onKeyDown={(e) => {
                                                    if (
                                                        (props.allowUnselect || !checked) &&
                                                        (e.key === "Enter" || e.key === " ") &&
                                                        !e.shiftKey
                                                    ) {
                                                        e.preventDefault();

                                                        changeValue(row, (!checked && column) || undefined);

                                                        return;
                                                    }

                                                    if (e.key === "Enter" && e.shiftKey && props.onSubmit) {
                                                        props.onSubmit();
                                                    } else if (e.key === "Escape") {
                                                        e.currentTarget.blur();
                                                    } else if (e.key === "Tab") {
                                                        if (e.shiftKey) {
                                                            if (props.onCancel && rowIndex === 0) {
                                                                e.preventDefault();

                                                                props.onCancel();
                                                            }
                                                        } else if (props.onSubmit && rowIndex + 1 === props.rows.length) {
                                                            e.preventDefault();

                                                            props.onSubmit();
                                                        }
                                                    }
                                                }}
                                            />
                                            <span />
                                            <span />
                                        </RadiobuttonLabelElement>
                                    </td>
                                );
                            })}
                        </tr>
                    ))}
                </tbody>
            </MatrixElement>
        </MatrixContainerElement>
    );
};
