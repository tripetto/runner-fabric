import { styled } from "styled-components";
import { FocusEvent, ReactNode, useState, useEffect, useRef } from "react";
import { L10n, Num, isNumberFinite, DateTime, castToNumber } from "@tripetto/runner";
import { color, getColor } from "../../color";
import { penIcon } from "../../icons/pen";
import { warningIcon } from "../../icons/warning";
import { clearIcon } from "../../icons/clear";
import { ButtonFabric } from "../button";

export interface ISignatureController {
    readonly upload: (files: FileList, service: ISignatureService | undefined, onProgress: (percent: number) => void) => Promise<void>;
    readonly download: (service?: ISignatureService) => Promise<string>;
    readonly delete: (service?: ISignatureService) => Promise<void>;
    readonly signatureSlot: {
        readonly hasValue: boolean;
        readonly string: string;
        readonly reference: string | undefined;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
        readonly time: number | undefined;
    };
}

export interface ISignatureService {
    readonly get: (signature: string) => Promise<Blob>;
    readonly put: (signature: File, onProgress?: (percentage: number) => void) => Promise<string>;
    readonly delete: (signature: string) => Promise<void>;
}

const SignatureElement = styled.div<{
    $backgroundColor: string;
    $borderColor: string;
    $borderSize: number;
    $roundness: number | undefined;
    $textColor: string | undefined;
    $errorColor: string;
    $error: boolean;
    $disabled: boolean;
    $scale: number;
    $allowSign: boolean;
    $processing: boolean;
    $size: "sm" | "md" | "lg";
}>`
    outline: none;
    box-sizing: border-box;
    display: block;
    width: 100%;
    height: ${(props) => `${(props.$size === "sm" ? 9 : props.$size === "lg" ? 19 : 14) * props.$scale}em`};
    position: relative;
    overflow: hidden;
    font-size: 1em;
    line-height: 1.5em;
    background-color: ${(props) => color(props.$backgroundColor)};
    background-image: ${(props) =>
        props.$error
            ? `url("data:image/svg+xml;base64,${warningIcon(
                  color(props.$errorColor, (o) =>
                      o.makeBlackOrWhite(props.$backgroundColor !== "transparent" && props.$borderColor === "transparent")
                  )
              )}")`
            : undefined};
    background-repeat: no-repeat;
    background-size: ${8 / 7}em;
    background-position: right 0.375em top 0.375em;
    border: ${(props) =>
        `${props.$backgroundColor === "transparent" && props.$borderColor === "transparent" ? 0 : props.$borderSize}px solid ${color(
            props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor
        )}`};
    color: ${(props) =>
        color(
            props.$textColor
                ? props.$textColor
                : props.$borderColor && props.$borderColor !== "transparent"
                ? props.$borderColor
                : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
        )};
    border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : "0.5em")};
    margin: 0;
    opacity: ${(props) => (props.$disabled ? 0.65 : 1)};
    transition:
        color 0.15s ease-in-out,
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out,
        opacity 0.15s ease-in-out;
    cursor: default;

    &:hover {
        box-shadow: ${(props) =>
            (!props.$disabled &&
                (props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                `0 0 0 0.2rem ${color(
                    props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                    (o) => o.manipulate((m) => m.alpha(0.2))
                )}`) ||
            undefined};
    }

    &:focus {
        background-color: ${(props) => (!props.$disabled && color(props.$backgroundColor)) || undefined};
        border-color: ${(props) =>
            (!props.$disabled &&
                color(props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor)) ||
            undefined};
        color: ${(props) =>
            (!props.$disabled &&
                color(
                    props.$textColor
                        ? props.$textColor
                        : props.$borderColor && props.$borderColor !== "transparent"
                        ? props.$borderColor
                        : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                )) ||
            undefined};
        box-shadow: ${(props) =>
            (!props.$disabled &&
                (props.$backgroundColor !== "transparent" || props.$borderColor !== "transparent") &&
                `0 0 0 0.2rem ${color(
                    props.$borderColor && props.$borderColor !== "transparent" ? props.$borderColor : props.$backgroundColor,
                    (o) => o.manipulate((m) => m.alpha(0.5))
                )}`) ||
            undefined};
    }

    > canvas {
        position: absolute;
        left: 0;
        top: 0;
        width: 1px;
        height: 1px;
        touch-action: none;

        &:first-of-type {
            pointer-events: none;
            opacity: 0;
        }

        &:last-of-type {
            cursor: ${(props) =>
                `url("data:image/svg+xml;base64,${penIcon(
                    color(
                        props.$textColor
                            ? props.$textColor
                            : props.$borderColor && props.$borderColor !== "transparent"
                            ? props.$borderColor
                            : color(props.$backgroundColor, (o) => o.makeBlackOrWhite())
                    )
                )}") 1 32, auto`};
            pointer-events: ${(props) => (!props.$disabled && props.$allowSign ? "auto" : "none")};
            opacity: ${(props) => (props.$disabled || props.$processing ? 0.3 : 1)};
            transition: opacity 0.3s;
            filter: blur(0.5px);
        }
    }

    > div {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        opacity: ${(props) => (!props.$disabled && !props.$allowSign ? 1 : 0)};
        pointer-events: ${(props) => (props.$disabled || props.$allowSign ? "none" : "auto")};
        transition: opacity 0.3s;
        display: flex;
        align-items: center;
        justify-content: center;
    }
`;

const SignatureProgressElement = styled.div<{
    $color: string;
}>`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    > div {
        display: inline-block;
        width: 80%;
        max-width: 300px;
        height: 16px;
        background-color: ${(props) => color(props.$color, (o) => o.manipulate((m) => m.alpha(0.25)))};
        border-radius: 4px;
        overflow: hidden;
        margin-bottom: 0.375em;

        > div {
            width: 0%;
            height: 16px;
            background-color: ${(props) => color(props.$color)};
            background-image: linear-gradient(
                45deg,
                ${(props) => color(props.$color, (o) => o.makeBlackOrWhite().manipulate((m) => m.alpha(0.25)))} 25%,
                transparent 25%,
                transparent 50%,
                ${(props) => color(props.$color, (o) => o.makeBlackOrWhite().manipulate((m) => m.alpha(0.25)))} 50%,
                ${(props) => color(props.$color, (o) => o.makeBlackOrWhite().manipulate((m) => m.alpha(0.25)))} 75%,
                transparent 75%,
                transparent
            );
            background-size: 1rem 1rem;
            transition: width 0.5s ease-out;
        }
    }

    > span {
        display: block;
        text-align: center;
        white-space: nowrap;
    }
`;

export const SignatureSignedElement = styled.div`
    position: absolute;
    right: 10px;
    top: 10px;
    display: flex;
    align-items: center;

    > span {
        display: block;
        margin-right: 10px;
        white-space: nowrap;
    }
`;

export const SignatureViewFabric = (props: {
    readonly controller: ISignatureController;
    readonly service?: ISignatureService;
    readonly host?: (props: { children: ReactNode }) => JSX.Element;
    readonly error?: ReactNode;
    readonly loading?: ReactNode;
}) => {
    const [data, setData] = useState({
        loading: true,
        base64data: "",
    });

    useEffect(() => {
        if (!data.loading) {
            setData({
                loading: true,
                base64data: "",
            });
        }

        props.controller
            .download(props.service)
            .then((base64data) =>
                setData({
                    loading: false,
                    base64data,
                })
            )
            .catch(() =>
                setData({
                    loading: false,
                    base64data: "",
                })
            );
    }, [props.controller.signatureSlot.reference]);

    if (data.loading) {
        return <>{props.loading}</>;
    }

    return (
        (data.base64data && (props.host ? <props.host children={<img src={data.base64data} />} /> : <img src={data.base64data} />)) || (
            <>{props.error}</>
        )
    );
};

export const SignatureFabric = (props: {
    readonly styles: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize?: number;
        readonly roundness?: number;
        readonly textColor?: string;
        readonly errorColor: string;
        readonly scale?: number;
    };
    readonly l10n?: L10n.Namespace;
    readonly labels: (id: "clear" | "processing" | "signed", message: string) => string;
    readonly controller: ISignatureController;
    readonly service?: ISignatureService;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly ariaDescribedBy?: string;
    readonly size?: "sm" | "md" | "lg";
    readonly color?: string;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLDivElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const canvasRef = useRef<HTMLCanvasElement | null>();
    const bufferRef = useRef<HTMLCanvasElement | null>();
    const [progress, setProgress] = useState(-1);
    const [reset, setReset] = useState(false);
    const [loading, setLoading] = useState(false);
    const propsRef = useRef<typeof props>();
    const allowSign = useRef(false);
    const signedRef = useRef(false);
    const colorRef = useRef("");
    const disabled = props.disabled || props.controller.signatureSlot.isFrozen || props.controller.signatureSlot.isLocked || false;
    const baseColor = props.styles.textColor
        ? props.styles.textColor
        : props.styles.borderColor && props.styles.borderColor !== "transparent"
        ? props.styles.borderColor
        : color(props.styles.backgroundColor || "transparent", (o) => o.makeBlackOrWhite());

    colorRef.current = props.color || baseColor;

    const updateColor = () => {
        const canvas = canvasRef.current;

        if (canvas && canvas.width > 0 && canvas.height > 0) {
            const canvasCtx = canvas.getContext("2d");

            if (canvasCtx) {
                const signatureData = canvasCtx.getImageData(0, 0, canvas.width, canvas.height);
                const color = getColor(colorRef.current);

                for (let i = 0; i < signatureData.data.length; i += 4) {
                    if (signatureData.data[i + 3] !== 0) {
                        signatureData.data[i] = color.red();
                        signatureData.data[i + 1] = color.green();
                        signatureData.data[i + 2] = color.blue();
                    }
                }

                canvasCtx.putImageData(signatureData, 0, 0);
            }
        }
    };

    const handleReset = () => {
        if (props.controller.signatureSlot.hasValue && progress === -1) {
            setReset(true);

            props.controller
                .delete(props.service)
                .then(() => {
                    const canvas = canvasRef.current;

                    if (canvas) {
                        canvas.getContext("2d")?.clearRect(0, 0, canvas.width, canvas.height);
                    }

                    setReset(false);
                })
                .catch(() => setReset(false));
        }
    };

    useEffect(() => {
        if (!canvasRef.current) {
            return;
        }

        const parentRect = canvasRef.current.parentElement?.getBoundingClientRect();

        if (!parentRect) {
            return;
        }

        let drawing = false;
        let waiting = 0;
        const canvas = canvasRef.current;
        const canvasCtx = canvas.getContext("2d", { willReadFrequently: true });
        const scale = 2;
        const resizeObserver =
            "ResizeObserver" in window &&
            new ResizeObserver(() => {
                const computedStyles = getComputedStyle(canvas.parentElement as HTMLCanvasElement);
                const width = castToNumber(computedStyles.width);
                const height = castToNumber(computedStyles.height);

                if (width > 0 && height > 0 && (width * scale !== canvas.width || height * scale !== canvas.height)) {
                    const buffer =
                        (canvas.width > 0 && canvas.height > 0 && canvasCtx?.getImageData(0, 0, canvas.width, canvas.height)) || undefined;

                    canvas.style.width = `${width}px`;
                    canvas.style.height = `${height}px`;
                    canvas.width = width * scale;
                    canvas.height = height * scale;

                    if (buffer) {
                        canvasCtx?.putImageData(buffer, 0, 0);
                    }
                }
            });

        canvas.style.width = `${parentRect.width}px`;
        canvas.style.height = `${parentRect.height}px`;
        canvas.width = parentRect.width * scale;
        canvas.height = parentRect.height * scale;

        if (!canvasCtx) {
            return;
        }

        const startSignature = (event: PointerEvent) => {
            const rect = (event.target as HTMLCanvasElement).getBoundingClientRect();

            if (waiting) {
                clearTimeout(waiting);

                waiting = 0;
            }

            if (!allowSign.current || canvas.width < 1 || canvas.height < 1) {
                drawing = false;

                return;
            }

            canvasCtx.lineWidth = 3 * scale;
            canvasCtx.lineJoin = canvasCtx.lineCap = "round";
            canvasCtx.imageSmoothingQuality = "high";
            canvasCtx.strokeStyle = color(colorRef.current);

            canvasCtx.beginPath();
            canvasCtx.moveTo((event.clientX - rect.x) * scale, (event.clientY - rect.y) * scale);

            drawing = true;

            canvas.parentElement?.focus();
        };

        const drawSignature = (event: PointerEvent) => {
            if (event.target && drawing && allowSign.current) {
                const rect = (event.target as HTMLCanvasElement).getBoundingClientRect();

                if (waiting) {
                    clearTimeout(waiting);

                    waiting = 0;
                }

                const x = (event.clientX - rect.x) * scale;
                const y = (event.clientY - rect.y) * scale;

                canvasCtx.lineTo(x, y);
                canvasCtx.stroke();
            }

            event.preventDefault();
            event.stopPropagation();
        };

        const endSignature = () => {
            drawing = false;

            if (waiting) {
                clearTimeout(waiting);
            }

            waiting = setTimeout(() => {
                const buffer = bufferRef.current;

                setProgress(0);

                if (buffer && canvas.width > 0 && canvas.height > 0) {
                    const bufferCtx = buffer.getContext("2d");

                    if (bufferCtx) {
                        const signatureData = canvasCtx.getImageData(0, 0, canvas.width, canvas.height);
                        let x1 = -1;
                        let y1 = -1;
                        let x2 = 0;
                        let y2 = 0;

                        for (let i = 0; i < signatureData.data.length; i += 4) {
                            if (signatureData.data[i + 3] !== 0) {
                                const x = (i / 4) % canvas.width;
                                const y = Num.floor(i / 4 / canvas.width);

                                signatureData.data[i] = 0;
                                signatureData.data[i + 1] = 0;
                                signatureData.data[i + 2] = 0;

                                if (x1 === -1 || x < x1) {
                                    x1 = x;
                                }

                                if (y1 === -1 || y < y1) {
                                    y1 = y;
                                }

                                if (x > x2) {
                                    x2 = x;
                                }

                                if (y > y2) {
                                    y2 = y;
                                }
                            }
                        }

                        const width = x2 - x1;
                        const height = y2 - y1;

                        if (x1 >= 0 && y1 >= 0 && width > 0 && height > 0) {
                            const bufferData = canvasCtx.getImageData(x1, y1, width, height);

                            for (let i = 0; i < bufferData.data.length; i += 4) {
                                if (bufferData.data[i + 3] !== 0) {
                                    bufferData.data[i] = 0;
                                    bufferData.data[i + 1] = 0;
                                    bufferData.data[i + 2] = 0;
                                }
                            }

                            buffer.width = Num.min(width + 50, canvas.width);
                            buffer.height = Num.min(height + 50, canvas.height);

                            bufferCtx.clearRect(0, 0, buffer.width, buffer.height);
                            bufferCtx.putImageData(bufferData, (buffer.width - width) / 2, (buffer.height - height) / 2);

                            signedRef.current = true;

                            buffer.toBlob((blob) => {
                                if (blob && propsRef.current) {
                                    const dataTransfer = new DataTransfer();

                                    dataTransfer.items.add(
                                        new File([blob], `signature-${DateTime.format("yyyyMMddHHmmss")}.png`, {
                                            type: "image/png",
                                            lastModified: new Date().getTime(),
                                        })
                                    );

                                    propsRef.current.controller
                                        .upload(dataTransfer.files, propsRef.current.service, (percent) => setProgress(Num.floor(percent)))
                                        .then(() => setProgress(-1))
                                        .catch(() => setProgress(-1));
                                }
                            }, "image/png");
                        } else {
                            setProgress(-1);
                        }
                    }
                }

                waiting = 0;
            }, 1500) as unknown as number;
        };

        canvas.addEventListener("pointerdown", startSignature);
        canvas.addEventListener("pointermove", drawSignature);
        canvas.addEventListener("pointerup", endSignature);

        if (resizeObserver && canvas.parentElement) {
            resizeObserver.observe(canvas.parentElement);
        }

        return () => {
            if (resizeObserver) {
                resizeObserver.disconnect();
            }

            canvas.removeEventListener("pointerdown", startSignature);
            canvas.removeEventListener("pointermove", drawSignature);
            canvas.removeEventListener("pointerup", endSignature);
        };
    }, []);

    useEffect(() => {
        if (props.controller.signatureSlot.hasValue && progress === -1 && !loading && !signedRef.current) {
            setLoading(true);

            props.controller
                .download(props.service)
                .then((base64data) => {
                    if (base64data) {
                        const buffer = new Image();

                        buffer.onload = () => {
                            const canvas = canvasRef.current;

                            if (canvas) {
                                const ctx = canvas.getContext("2d");

                                if (ctx) {
                                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                                    ctx.drawImage(
                                        buffer,
                                        Num.min(50, Num.max(canvas.width - buffer.width, 0)),
                                        Num.max(canvas.height - buffer.height, 0) / 2
                                    );

                                    updateColor();
                                }
                            }
                        };

                        buffer.src = base64data;
                    }

                    setLoading(false);
                })
                .catch(() => setLoading(false));
        }
    }, [props.controller.signatureSlot.reference]);

    useEffect(() => updateColor(), [colorRef.current]);

    propsRef.current = props;
    allowSign.current = !loading && !reset && progress === -1 && !props.controller.signatureSlot.hasValue;

    return (
        <SignatureElement
            ref={props.onAutoFocus}
            tabIndex={props.tabIndex || 0}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            $backgroundColor={props.styles.backgroundColor || "transparent"}
            $borderColor={props.styles.borderColor || "transparent"}
            $borderSize={props.styles.borderSize || 1}
            $roundness={props.styles.roundness}
            $textColor={props.styles.textColor}
            $errorColor={props.styles.errorColor}
            $error={props.error || false}
            $scale={props.styles.scale || 1}
            $disabled={disabled}
            $allowSign={allowSign.current}
            $size={props.size || "md"}
            $processing={progress !== -1}
        >
            <canvas ref={(c) => (bufferRef.current = c)} width="1" height="1"></canvas>
            <canvas ref={(c) => (canvasRef.current = c)} width="1" height="1"></canvas>
            <div>
                {progress !== -1 ? (
                    <SignatureProgressElement $color={baseColor}>
                        <div>
                            <div
                                style={{
                                    width: `${Num.range(progress, 0, 100)}%`,
                                }}
                            ></div>
                        </div>
                        <span>{props.labels("processing", `${Num.range(progress, 0, 100)}%`)}</span>
                    </SignatureProgressElement>
                ) : (
                    <SignatureSignedElement>
                        {(props.controller.signatureSlot.hasValue && (props.controller.signatureSlot.time || 0) > 0 && (
                            <span>
                                {props.labels("signed", (props.l10n?.locale || L10n.Locales).dateLong(props.controller.signatureSlot.time))}
                            </span>
                        )) ||
                            undefined}
                        <ButtonFabric
                            styles={{
                                baseColor,
                                mode: "outline",
                            }}
                            tabIndex={props.tabIndex || 0}
                            label={props.labels("clear", "")}
                            icon={clearIcon}
                            disabled={reset}
                            onClick={handleReset}
                        />
                    </SignatureSignedElement>
                )}
            </div>
        </SignatureElement>
    );
};
