import { css, keyframes, styled } from "styled-components";
import { FocusEvent, KeyboardEvent, ReactNode, useEffect, useRef, useState } from "react";
import { Num, cancelUITimeout, scheduleUITimeout } from "@tripetto/runner";
import { handleAutoSubmit } from "../helpers";
import { color } from "../../color";
import { starShape } from "../../shapes/star";
import { heartShape } from "../../shapes/heart";
import { thumbsUpShape } from "../../shapes/thumbs-up";
import { thumbsDownShape } from "../../shapes/thumbs-down";
import { personShape } from "../../shapes/person";
import { DEBOUNCE_NORMAL } from "../const";

export type RatingShapes = "stars" | "hearts" | "thumbs-up" | "thumbs-down" | "persons";

const RatingElement = styled.div`
    display: block;
    width: 100%;
`;

const RatingShapeElement = styled.button<{
    $color: string;
    $scale: number;
    $checked: boolean;
    $hover: boolean;
    $showLabels: boolean;
}>`
    appearance: none;
    box-sizing: border-box;
    background: none;
    border: none;
    outline: none;
    width: ${(props) => `${2 * props.$scale}em`};
    height: ${(props) => `${2 * props.$scale}em`};
    margin: 0;
    padding: 0;
    margin-bottom: ${(props) => props.$showLabels && "1em"};
    display: inline-block;
    opacity: 0.2;
    cursor: default;
    transition:
        width 0.15s ease-in-out,
        height 0.15s ease-in-out,
        opacity 0.15 ease-in-out;

    > svg {
        path {
            fill: ${(props) =>
                (props.$checked && color(props.$color)) ||
                (props.$hover && color(props.$color, (o) => o.manipulate((m) => m.alpha(0.3)))) ||
                "none"};
            stroke: ${(props) => color(props.$color)};
            stroke-width: 1px;
            transition:
                fill 0.15s ease-in-out,
                stroke 0.15s ease-in-out;
        }
    }

    > small {
        display: block;
        font-size: 0.8em;
        line-height: 1em;
        text-align: center;
        margin: 0;
        padding: 0;
        color: ${(props) => color(props.$color)};
    }

    animation: ${(props) =>
        props.$checked
            ? css`
                  ${keyframes`
                        50% {
                            transform: scale(1.1);
                        }

                        100% {
                            transform: none;
                        }
                    `} .5s linear
              `
            : undefined};

    &:not(:disabled) {
        opacity: 1;
        cursor: pointer;

        &:focus {
            > svg {
                path {
                    stroke-width: ${(props) => (props.$checked && "2px") || undefined};
                    fill: ${(props) => (!props.$checked && color(props.$color, (o) => o.manipulate((m) => m.alpha(0.3)))) || undefined};
                    stroke: ${(props) => (props.$checked && color(props.$color, (o) => o.manipulate((m) => m.alpha(0.3)))) || undefined};
                }
            }
        }
    }
`;

const getShape = (shape: RatingShapes) => {
    switch (shape) {
        case "stars":
            return starShape;
        case "hearts":
            return heartShape;
        case "thumbs-up":
            return thumbsUpShape;
        case "thumbs-down":
            return thumbsDownShape;
        case "persons":
            return personShape;
    }
};

const assertValue = (
    valueRef: {
        value: number;
        readonly clear: () => void;
    },
    value: number
) => {
    if (value) {
        return (valueRef.value = value);
    }

    valueRef.clear();

    return 0;
};

export const RatingFabric = (props: {
    readonly styles: {
        readonly color: string;
        readonly scale?: number;
    };
    readonly steps: number;
    readonly shape?: RatingShapes;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly showLabels?: boolean;
    readonly ariaDescribedBy?: string;
    readonly tabIndex?: number;
    readonly value?:
        | number
        | {
              value: number;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly clear: () => void;
          };
    readonly required?: boolean;
    readonly autoSubmit?: boolean;
    readonly onChange?: (value: number) => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLButtonElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<number>(0);
    const [proxy, setProxy] = useState((typeof valueRef !== "object" && valueRef) || 0);
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  debounceRef.current !== 0 ? proxy : assertValue(valueRef, valueRef.value),
                  (val: number, submit: boolean) => {
                      cancelUITimeout(debounceRef.current);

                      setProxy(val);

                      debounceRef.current = scheduleUITimeout(() => {
                          debounceRef.current = 0;

                          assertValue(valueRef, val);

                          if (submit && props.autoSubmit && val) {
                              handleAutoSubmit(autoSubmit);
                          }
                      }, DEBOUNCE_NORMAL);
                  },
              ]
            : [proxy, setProxy];
    const [hover, setHover] = useState(0);
    const items: ReactNode[] = [];
    const autoSubmit = useRef({
        id: 0,
        cb: props.onSubmit,
    });
    const steps = Num.max(1, props.steps);
    const isDisabled =
        props.disabled || props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false;
    const changeValue = (val: number, submit: boolean) => {
        if (props.disabled || props.readOnly) {
            return;
        }

        val = Num.range(val, 0, steps);

        if (autoSubmit.current.id) {
            clearTimeout(autoSubmit.current.id);

            autoSubmit.current.id = 0;
        }

        setValue(val, submit);

        if (props.onChange) {
            props.onChange(val);
        }

        if (submit && props.autoSubmit && val) {
            handleAutoSubmit(autoSubmit);
        }
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    for (let i = 1; i <= steps; i++) {
        items.push(
            <RatingShapeElement
                key={i}
                ref={(i === (value || 1) && props.onAutoFocus) || undefined}
                disabled={isDisabled}
                tabIndex={props.tabIndex}
                aria-describedby={props.ariaDescribedBy}
                onMouseOver={() => setHover(i)}
                onMouseOut={() => setHover(0)}
                onClick={() => changeValue(value === 1 && !props.required ? 0 : i, true)}
                onFocus={props.onFocus}
                onBlur={props.onBlur}
                onKeyDown={(e: KeyboardEvent<HTMLButtonElement>) => {
                    if (e.shiftKey && e.key === "Enter" && props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    } else if (e.key === "Enter" || e.key === " ") {
                        e.preventDefault();

                        changeValue(value === 1 && !props.required ? 0 : i, true);
                    } else if (e.key === "Escape") {
                        e.currentTarget.blur();
                    } else if (e.key === "Tab") {
                        if (e.shiftKey) {
                            if (props.onCancel && i === 1) {
                                e.preventDefault();

                                props.onCancel();
                            }
                        } else if (props.onSubmit && i === steps) {
                            e.preventDefault();

                            props.onSubmit();
                        }
                    } else {
                        if (e.key === "-") {
                            if (value > 0) {
                                changeValue(value - 1, false);
                            }
                        } else if (e.key === "+") {
                            if (value < steps) {
                                changeValue(value + 1, false);
                            }
                        } else {
                            const n = e.key.length === 1 ? e.key.charCodeAt(0) - 48 : -1;

                            if (n >= 0 && n <= 9) {
                                changeValue(value === n ? n - 1 : n, true);
                            }
                        }
                    }
                }}
                $color={props.styles.color}
                $scale={props.styles.scale || 1}
                $hover={!isDisabled && hover >= i}
                $checked={i <= value}
                $showLabels={props.showLabels || false}
            >
                {getShape(props.shape || "stars")}
                {props.showLabels && <small>{i}</small>}
            </RatingShapeElement>
        );
    }

    autoSubmit.current.cb = props.onSubmit;

    return <RatingElement>{items}</RatingElement>;
};
