import { styled } from "styled-components";
import { FocusEvent, useEffect, useRef, useState } from "react";
import { TSerializeTypes, arrayItem, cancelUITimeout, castToBoolean, each, findFirst, scheduleUITimeout } from "@tripetto/runner";
import { handleAutoSubmit } from "../helpers";
import { ButtonFabric } from "../button";
import { hyperlinkIcon } from "../../icons/hyperlink";
import { DEBOUNCE_NORMAL } from "../const";

export interface IMultipleChoiceButton {
    readonly id: string;
    readonly name: string;
    readonly value?: string;
    readonly label?: string | JSX.Element;
    readonly description?: string | JSX.Element;
    readonly url?: string;
    readonly target?: "self" | "blank";
    readonly disabled?: boolean;
    readonly slot?: {
        value: boolean;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
        readonly confirm: () => void;
    };
    readonly tabIndex?: number;
    readonly color?: string;
    readonly onChange?: (selected: boolean) => void;
}

const MultipleChoiceElement = styled.div<{
    $alignment: "vertical" | "equal" | "full" | "columns" | "horizontal";
    $margin?: number;
}>`
    display: table;
    width: ${(props) => (props.$alignment === "equal" && "auto") || "100%"};

    > * {
        display: ${(props) =>
            ((props.$alignment === "vertical" || props.$alignment === "equal" || props.$alignment === "full") && "block") ||
            "inline-block"};
        width: ${(props) =>
            ((props.$alignment === "equal" || props.$alignment === "full") && "100%") ||
            (props.$alignment === "columns" && "calc(50% - 4px)") ||
            undefined};
        margin-top: 8px;
        margin-right: ${(props) =>
            ((props.$alignment === "columns" || props.$alignment === "horizontal") && `${props.$margin || 8}px`) || undefined};

        &:nth-child(even) {
            margin-right: ${(props) => (props.$alignment === "columns" && "0px") || undefined};
        }

        &:last-child {
            margin-right: ${(props) => (props.$alignment === "horizontal" && "0px") || undefined};
        }

        > span {
            display: ${(props) => (props.$alignment === "full" && "flex") || undefined};
            justify-content: ${(props) => (props.$alignment === "full" && "center") || undefined};
            text-align: ${(props) => (props.$alignment === "full" && "center") || undefined};
            overflow: hidden;
        }
    }
`;

const assertValue = (
    valueRef: {
        reference?: string;
        readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
    },
    buttons: IMultipleChoiceButton[],
    reference?: string
) => {
    const selected = findFirst(buttons, (button) => button.id === reference);

    if (valueRef.reference !== selected?.id) {
        valueRef.set(selected && (selected.value || selected.name), selected?.id, selected?.name);
    }

    return (selected && selected.id) || "";
};

export const MultipleChoiceFabric = (props: {
    readonly styles: {
        readonly color: string;
        readonly outlineSize?: number;
        readonly roundness?: number;
        readonly scale?: number;
        readonly margin?: number;
    };
    readonly buttons: IMultipleChoiceButton[];
    readonly alignment?: "vertical" | "equal" | "full" | "columns" | "horizontal";
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly ariaDescribedBy?: string;
    readonly tabIndex?: number;
    readonly value?:
        | string
        | {
              reference?: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
          };
    readonly autoSubmit?: boolean;
    readonly view?: "live" | "test" | "preview";
    readonly onChange?: (value: string) => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLButtonElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<number>(0);
    const buttonsRef = useRef<{
        [button: string]: boolean;
    }>({});
    const [proxy, setProxy] = useState((typeof valueRef !== "object" && valueRef) || "");
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  debounceRef.current !== 0 ? proxy : assertValue(valueRef, props.buttons, valueRef.reference),
                  (reference: string) => {
                      cancelUITimeout(debounceRef.current);

                      setProxy(reference);

                      debounceRef.current = scheduleUITimeout(() => {
                          debounceRef.current = 0;

                          assertValue(valueRef, props.buttons, reference);

                          if (props.autoSubmit && reference) {
                              handleAutoSubmit(autoSubmit);
                          }
                      }, DEBOUNCE_NORMAL);
                  },
              ]
            : [proxy, setProxy];
    const [, update] = useState({});
    const autoSubmit = useRef({
        id: 0,
        cb: props.onSubmit,
    });

    const toggle = (button: IMultipleChoiceButton) => {
        if (button.url) {
            return;
        }

        const isSelected = button.slot ? castToBoolean(buttonsRef.current[button.id], button.slot.value) : value === button.id;

        if (button.slot) {
            cancelUITimeout(debounceRef.current);

            buttonsRef.current[button.id] = !isSelected;

            update({});

            debounceRef.current = scheduleUITimeout(() => {
                debounceRef.current = 0;

                each(
                    buttonsRef.current,
                    (val, id: string) => {
                        const changedButton = findFirst(props.buttons, (ref) => ref.id === id);

                        delete buttonsRef.current[id];

                        if (changedButton && changedButton.slot) {
                            changedButton.slot.value = val;
                        }
                    },
                    {
                        keys: true,
                    }
                );
            }, DEBOUNCE_NORMAL);
        } else {
            const val = isSelected && !props.required ? "" : button.id;

            if (autoSubmit.current.id) {
                clearTimeout(autoSubmit.current.id);

                autoSubmit.current.id = 0;
            }

            setValue(val);

            if (props.onChange) {
                props.onChange(val);
            }

            if (typeof valueRef !== "object" && props.autoSubmit && val) {
                handleAutoSubmit(autoSubmit);
            }
        }

        if (button.onChange) {
            button.onChange(!button.slot && props.required ? true : !isSelected);
        }
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    autoSubmit.current.cb = props.onSubmit;

    return (
        <MultipleChoiceElement $alignment={props.alignment || "vertical"} $margin={props.styles.margin}>
            {props.buttons.map((button, index) => {
                const isSelected = button.slot ? castToBoolean(buttonsRef.current[button.id], button.slot.value) : value === button.id;

                if (button.slot) {
                    button.slot.confirm();
                }

                return (
                    (props.view === "preview" || button.name || button.description) && (
                        <ButtonFabric
                            key={button.id || index}
                            styles={{
                                baseColor: button.color || props.styles.color,
                                mode: isSelected ? "fill" : "outline",
                                hover: "outline",
                                outlineSize: props.styles.outlineSize,
                                roundness: props.styles.roundness,
                                scale: props.styles.scale,
                            }}
                            hyperlink={
                                (button.url && {
                                    url: button.url,
                                    target: button.target || "blank",
                                }) ||
                                undefined
                            }
                            label={button.label || button.name || "..."}
                            description={button.description}
                            icon={(button.url && button.target !== "self" && hyperlinkIcon) || undefined}
                            iconPosition="right"
                            tabIndex={button.tabIndex || props.tabIndex}
                            ariaDescribedBy={props.ariaDescribedBy}
                            disabled={
                                button.disabled ||
                                props.disabled ||
                                props.readOnly ||
                                (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) ||
                                (button.slot && (button.slot.isFrozen || button.slot.isLocked)) ||
                                false
                            }
                            onAutoFocus={((button.slot || !value ? index === 0 : isSelected) && props.onAutoFocus) || undefined}
                            onFocus={props.onFocus}
                            onBlur={props.onBlur}
                            onKeyDown={(e) => {
                                if (e.shiftKey && e.key === "Enter" && props.onSubmit) {
                                    e.preventDefault();

                                    props.onSubmit();
                                } else if (e.key === "Escape") {
                                    e.currentTarget.blur();
                                } else if (e.key === "Tab") {
                                    if (e.shiftKey) {
                                        if (props.onCancel && index === 0) {
                                            e.preventDefault();

                                            props.onCancel();
                                        }
                                    } else if (props.onSubmit && index + 1 === props.buttons.length) {
                                        e.preventDefault();

                                        props.onSubmit();
                                    }
                                } else {
                                    const keyCode = (e.key.length === 1 && e.key.charCodeAt(0)) || 0;
                                    const offset =
                                        (keyCode <= 57 ? keyCode - 48 : 0) ||
                                        (keyCode <= 90 ? keyCode - 64 : 0) ||
                                        (keyCode <= 122 ? keyCode - 96 : 0);

                                    if (offset > 0 && offset <= 26) {
                                        const toggleButton = arrayItem(props.buttons, offset - 1);

                                        if (toggleButton) {
                                            toggle(toggleButton);
                                        }
                                    }
                                }
                            }}
                            onClick={() => toggle(button)}
                        />
                    )
                );
            })}
        </MultipleChoiceElement>
    );
};
