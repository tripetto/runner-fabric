import { styled } from "styled-components";
import { FocusEvent, KeyboardEvent, MouseEvent } from "react";
import { isNumberFinite } from "@tripetto/runner";
import { color } from "../../color";

const ButtonElement = styled.button<{
    $baseColor: string;
    $textColor?: string;
    $outlineSize: number;
    $roundness: number | undefined;
    $mode: "fill" | "outline";
    $hover: "fill" | "outline";
    $iconPosition: "left" | "right" | "solo";
    $scale: number;
    $group?: "start" | "middle" | "end";
    $hasLabel: boolean;
    $isImage: boolean;
    $size: "small" | "medium" | "large";
}>`
    appearance: none;
    font-size: 1em;
    line-height: 1.5em;
    outline: none;
    user-select: none;
    box-sizing: border-box;
    background-color: ${(props) => (props.$mode === "fill" && color(props.$baseColor)) || "transparent"};
    border: ${(props) =>
        `${props.$outlineSize}px solid ${color(props.$baseColor, (o) => o.manipulate((m) => m.darken(0.1), props.$mode === "fill"))}`};
    border-left: ${(props) => (props.$group === "middle" || props.$group === "end" ? "none" : undefined)};
    color: ${(props) =>
        color(props.$textColor || props.$baseColor, (o) => o.makeBlackOrWhite(!props.$textColor && props.$mode === "fill"))};
    text-align: left;
    border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : "0.5em")};
    border-top-left-radius: ${(props) => (props.$group && props.$group !== "start" && "0") || undefined};
    border-bottom-left-radius: ${(props) => (props.$group && props.$group !== "start" && "0") || undefined};
    border-top-right-radius: ${(props) => (props.$group && props.$group !== "end" && "0") || undefined};
    border-bottom-right-radius: ${(props) => (props.$group && props.$group !== "end" && "0") || undefined};
    width: ${(props) => props.$isImage && `calc(${props.$size === "small" ? 75 : props.$size === "large" ? 300 : 150}px + 1.5em)`};
    overflow: ${(props) => (props.$isImage && `hidden`) || undefined};
    padding: ${(props) => `${0.375 * props.$scale}em 0.75em`};
    padding-top: ${(props) => (props.$isImage && "0.75em") || undefined};
    padding-bottom: ${(props) => (props.$isImage && !props.$hasLabel && "0.75em") || undefined};
    margin: 0;
    opacity: ${(props) => (props.$mode === "outline" ? 0.2 : 0.4)};
    transition:
        color 0.15s ease-in-out,
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out,
        opacity 0.15s ease-in-out;

    @media (max-device-width: 500px) {
        width: ${(props) => props.$isImage && `calc(${props.$size === "small" ? 75 : props.$size === "large" ? 280 : 120}px + 1.5em)`};
    }

    @media (max-width: 500px) {
        width: ${(props) => props.$isImage && `calc(${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px + 1.5em)`};
    }

    > div {
        display: flex;
        width: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 300 : 150}px`};
        height: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 300 : 150}px`};
        min-width: 100%;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        justify-content: center;
        align-items: center;
        overflow: hidden;
        white-space: nowrap;

        > span {
            font-size: ${(props) => `${props.$size === "small" ? 40 : props.$size === "large" ? 160 : 80}px`};
        }

        @media (max-device-width: 500px) {
            width: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px`};
            height: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px`};

            > span {
                font-size: ${(props) => `${props.$size === "small" ? 40 : props.$size === "large" ? 136 : 68}px`};
            }
        }

        @media (max-width: 500px) {
            width: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px`};
            height: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px`};

            > span {
                font-size: ${(props) => `${props.$size === "small" ? 40 : props.$size === "large" ? 136 : 68}px`};
            }
        }
    }

    > span {
        display: block;

        > span {
            display: table-cell;

            > small {
                display: block;
                font-size: 0.8em;
                opacity: 0.8;
            }

            > svg {
                width: 1em;
                height: 1em;
                position: relative;
                top: ${(props) => props.$iconPosition !== "solo" && "-2px"};
                margin-left: ${(props) => props.$iconPosition === "right" && "0.5em"};
                margin-right: ${(props) => props.$iconPosition === "left" && "0.5em"};
                overflow: hidden;
                vertical-align: middle;

                path {
                    fill: ${(props) =>
                        color(props.$textColor || props.$baseColor, (o) =>
                            o.makeBlackOrWhite(!props.$textColor && props.$mode === "fill")
                        )};
                    transition: fill 0.15s ease-in-out;
                }
            }
        }
    }

    > div + span {
        display: flex;
        justify-content: center;
        margin-top: ${(props) => `${0.375 * props.$scale}em`};
        text-align: center;
    }

    &:not(:disabled) {
        cursor: pointer;
        opacity: 1;

        &:hover {
            background-color: ${(props) =>
                (props.$hover === "fill" && color(props.$baseColor, (o) => o.manipulate((m) => m.darken(0.05), props.$mode === "fill"))) ||
                undefined};
            border-color: ${(props) =>
                (props.$hover === "fill" && color(props.$baseColor, (o) => o.manipulate((m) => m.darken(0.1), props.$mode === "fill"))) ||
                undefined};
            color: ${(props) =>
                (props.$mode === "outline" && props.$hover === "fill" && color(props.$baseColor, (o) => o.makeBlackOrWhite())) ||
                undefined};
            box-shadow: 0 0 0 0.2rem ${(props) => color(props.$baseColor, (o) => o.manipulate((m) => m.alpha(0.2)))};

            svg {
                path {
                    fill: ${(props) =>
                        (props.$mode === "outline" && props.$hover === "fill" && color(props.$baseColor, (o) => o.makeBlackOrWhite())) ||
                        undefined};
                }
            }
        }

        &:focus {
            box-shadow: 0 0 0 0.2rem ${(props) => color(props.$baseColor, (o) => o.manipulate((m) => m.alpha(0.5)))};
        }

        &:active {
            box-shadow: 0 0 0 0.2rem ${(props) => color(props.$baseColor, (o) => o.manipulate((m) => m.alpha(0.5)))};
            background-color: ${(props) => props.$hover === "fill" && color(props.$baseColor, (o) => o.manipulate((m) => m.darken(0.15)))};
        }
    }
`;

export const ButtonFabric = (props: {
    readonly styles: {
        readonly baseColor: string;
        readonly textColor?: string;
        readonly outlineSize?: number;
        readonly roundness?: number;
        readonly mode?: "fill" | "outline";
        readonly hover?: "fill" | "outline";
        readonly scale?: number;
        readonly group?: "start" | "middle" | "end";
    };
    readonly label?: string | JSX.Element;
    readonly description?: string | JSX.Element;
    readonly icon?: JSX.Element;
    readonly iconPosition?: "left" | "right";
    readonly image?: string;
    readonly emoji?: string;
    readonly size?: "small" | "medium" | "large";
    readonly disabled?: boolean;
    readonly tabIndex?: number;
    readonly ariaDescribedBy?: string;
    readonly hyperlink?: {
        readonly url: string;
        readonly target?: "self" | "blank";
    };
    readonly onClick?: () => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLButtonElement | null) => void;
    readonly onKeyDown?: (e: KeyboardEvent<HTMLButtonElement>) => void;
    readonly onTab?: () => void;
    readonly onCancel?: () => void;
}) => {
    const iconPosition = !props.label ? "solo" : props.iconPosition || "left";
    const disabled = props.disabled || (!props.onClick && !props.hyperlink);

    return (
        <ButtonElement
            ref={props.onAutoFocus}
            type="button"
            disabled={disabled}
            tabIndex={props.tabIndex}
            aria-describedby={props.ariaDescribedBy}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            onKeyDown={(e: KeyboardEvent<HTMLButtonElement>) => {
                if (props.onTab && e.key === "Tab" && !e.shiftKey) {
                    e.preventDefault();

                    props.onTab();

                    return;
                }

                if (props.onCancel && e.key === "Tab" && e.shiftKey) {
                    e.preventDefault();

                    props.onCancel();

                    return;
                }

                if (props.onKeyDown) {
                    props.onKeyDown(e);
                }
            }}
            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                e.stopPropagation();

                if (props.hyperlink) {
                    window.open(props.hyperlink.url, `_${props.hyperlink.target || "self"}`, "noopener");
                } else if (props.onClick) {
                    props.onClick();
                }
            }}
            $baseColor={props.styles.baseColor}
            $textColor={props.styles.textColor}
            $outlineSize={props.styles.outlineSize || 1}
            $roundness={props.styles.roundness}
            $mode={props.styles.mode || "fill"}
            $hover={props.styles.hover || "fill"}
            $iconPosition={iconPosition}
            $scale={props.styles.scale || 1}
            $group={props.styles.group}
            $hasLabel={props.icon || props.label ? true : false}
            $isImage={props.image || props.emoji ? true : false}
            $size={props.size || "medium"}
        >
            {(props.image || props.emoji) && (
                <div
                    style={{
                        backgroundImage: (props.image && `url("${props.image}")`) || undefined,
                    }}
                >
                    {props.emoji && <span>{props.emoji}</span>}
                </div>
            )}
            {(props.icon || props.label) && (
                <span>
                    {props.icon && iconPosition !== "right" && <span>{props.icon}</span>}
                    {props.label && (
                        <span>
                            {props.label}
                            {props.description && <small>{props.description}</small>}
                        </span>
                    )}
                    {props.icon && iconPosition === "right" && <span>{props.icon}</span>}
                </span>
            )}
        </ButtonElement>
    );
};
