import { styled } from "styled-components";
import { FocusEvent, KeyboardEvent, PointerEvent, useRef, useState } from "react";
import { Num, TSerializeTypes, arrayItem, castToNumber, each, filter, findFirst, isNumberFinite } from "@tripetto/runner";
import { color } from "../../color";
import { positionIcon } from "../../icons/position";

export interface IRankingOption {
    readonly id: string;
    readonly name: string;
    readonly value?: string;
    readonly label?: string | JSX.Element;
    readonly description?: string | JSX.Element;
}

const RankingElement = styled.div<{
    $margin?: number;
}>`
    display: table;
    width: auto;

    > * {
        display: block;
        width: 100%;
        margin-top: 8px !important;

        > span {
            overflow: hidden;
        }
    }
`;

const RankingOptionElement = styled.button<{
    $baseColor: string;
    $outlineSize: number;
    $roundness: number | undefined;
    $mode: "fill" | "outline";
    $hover: "fill" | "outline";
    $scale: number;
}>`
    appearance: none;
    position: relative;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-items: flex-start;
    justify-content: space-between;
    font-size: 1em;
    line-height: 1.5em;
    outline: none;
    user-select: none;
    box-sizing: border-box;
    background-color: ${(props) => (props.$mode === "fill" && color(props.$baseColor)) || "transparent"};
    border: ${(props) =>
        `${props.$outlineSize}px solid ${color(props.$baseColor, (o) => o.manipulate((m) => m.darken(0.1), props.$mode === "fill"))}`};
    color: ${(props) => color(props.$baseColor, (o) => o.makeBlackOrWhite(props.$mode === "fill"))};
    text-align: left;
    border-radius: ${(props) => (isNumberFinite(props.$roundness) ? `${props.$roundness}px` : "0.5em")};
    padding: ${(props) => `${0.375 * props.$scale}em 2.75em ${0.375 * props.$scale}em ${0.375 * props.$scale}em`};
    margin: 0;
    opacity: ${(props) => (props.$mode === "outline" ? 0.2 : 0.4)};
    transition:
        color 0.15s ease-in-out,
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out,
        opacity 0.15s ease-in-out,
        transform 0.15s ease-out;

    > select {
        display: block;
        flex-shrink: 0;
        font-size: 0.8em;
        background-color: ${(props) =>
            props.$mode === "fill" ? color(props.$baseColor, (o) => o.makeBlackOrWhite(true)) : props.$baseColor};
        color: ${(props) => (props.$mode === "fill" ? props.$baseColor : color(props.$baseColor, (o) => o.makeBlackOrWhite(true)))};
        border: ${(props) =>
            `0.3em solid ${props.$mode === "fill" ? color(props.$baseColor, (o) => o.makeBlackOrWhite(true)) : props.$baseColor}`};
        border-radius: ${(props) => (props.$roundness !== 0 ? `${props.$roundness || 5}px` : "0px")};
        margin-right: 0.75em;
    }

    > span {
        display: table-cell;
        flex-grow: 1;

        > small {
            display: block;
            font-size: 0.8em;
            opacity: 0.8;
        }
    }

    > div {
        position: absolute;
        top: 0;
        right: 0;
        width: 3em;
        bottom: 0;
        display: flex;
        align-items: center;
        justify-content: flex-end;
        touch-action: none;
        padding-right: 0.75em;

        > svg {
            width: 1.3em;
            height: 1.3em;
            pointer-events: none;

            path {
                fill: ${(props) => color(props.$baseColor, (o) => o.makeBlackOrWhite(props.$mode === "fill"))};
                transition: fill 0.15s ease-in-out;
            }
        }
    }

    &:not(:disabled) {
        cursor: grab;
        opacity: 1;

        &:hover {
            background-color: ${(props) =>
                (props.$hover === "fill" && color(props.$baseColor, (o) => o.manipulate((m) => m.darken(0.05), props.$mode === "fill"))) ||
                undefined};
            border-color: ${(props) =>
                (props.$hover === "fill" && color(props.$baseColor, (o) => o.manipulate((m) => m.darken(0.1), props.$mode === "fill"))) ||
                undefined};
            color: ${(props) =>
                (props.$mode === "outline" && props.$hover === "fill" && color(props.$baseColor, (o) => o.makeBlackOrWhite())) ||
                undefined};
            box-shadow: 0 0 0 0.2rem ${(props) => color(props.$baseColor, (o) => o.manipulate((m) => m.alpha(0.2)))};

            svg {
                path {
                    fill: ${(props) =>
                        (props.$mode === "outline" && props.$hover === "fill" && color(props.$baseColor, (o) => o.makeBlackOrWhite())) ||
                        undefined};
                }
            }
        }

        &:focus {
            box-shadow: 0 0 0 0.2rem ${(props) => color(props.$baseColor, (o) => o.manipulate((m) => m.alpha(0.5)))};
        }

        &:active {
            box-shadow: 0 0 0 0.2rem ${(props) => color(props.$baseColor, (o) => o.manipulate((m) => m.alpha(0.5)))};
            background-color: ${(props) => props.$hover === "fill" && color(props.$baseColor, (o) => o.manipulate((m) => m.darken(0.15)))};
        }
    }
`;

export const RankingFabric = (props: {
    readonly styles: {
        readonly color: string;
        readonly outlineSize?: number;
        readonly roundness?: number;
        readonly scale?: number;
        readonly margin?: number;
    };
    readonly options: IRankingOption[];
    readonly slots?: {
        reference?: string;
        readonly key: string;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
        readonly set: (value: TSerializeTypes, reference?: string, display?: string) => void;
    }[];
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly ariaDescribedBy?: string;
    readonly tabIndex?: number;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLButtonElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const rankingRef = useRef<
        (IRankingOption & {
            ref?: HTMLElement;
            moveId?: number;
            moveHeight?: number;
            moveX?: number;
            moveY?: number;
            moveDirection?: number;
            moveTransform?: string;
        })[]
    >([]);
    const slotsRef = useRef<{
        [slot: string]: string | undefined;
    }>({});
    const [animations, update] = useState<{ disable?: true }>({});
    const onPointerDown = (index: number) => (e: PointerEvent<HTMLButtonElement>) => {
        const item = rankingRef.current[index];

        if (
            item.ref &&
            typeof item.moveId !== "number" &&
            e.isPrimary &&
            (e.target as HTMLElement).tagName !== "SELECT" &&
            (e.target as HTMLElement).tagName !== "A" &&
            (e.pointerType === "mouse" || (e.target as HTMLElement).tagName === "DIV")
        ) {
            item.moveHeight = item.ref.nextElementSibling
                ? item.ref.nextElementSibling.getBoundingClientRect().top - item.ref.getBoundingClientRect().top
                : item.ref.previousElementSibling
                ? item.ref.getBoundingClientRect().bottom - item.ref.previousElementSibling.getBoundingClientRect().bottom
                : 0;

            if (item.moveHeight > 0) {
                e.preventDefault();
                e.stopPropagation();

                item.moveId = e.pointerId;
                item.moveX = e.clientX;
                item.moveY = item.moveDirection = e.clientY;

                item.ref.setPointerCapture(item.moveId);
                item.ref.focus();

                update({});
            }
        }
    };
    const onPointerMove = (index: number) => (e: PointerEvent<HTMLButtonElement>) => {
        const item = rankingRef.current[index];

        if (e.isPrimary && typeof item.moveId === "number" && item.ref) {
            const rect = item.ref.getBoundingClientRect();
            const movementY = e.clientY - (item.moveDirection || 0);

            e.preventDefault();
            e.stopPropagation();

            item.moveDirection = e.clientY;
            item.ref.style.transform = item.moveTransform = `translate3d(${e.clientX - (item.moveX || 0)}px,${
                e.clientY - (item.moveY || 0)
            }px,0)`;

            for (let i = 0; i < rankingRef.current.length; i++) {
                if (i !== index) {
                    const sibling = rankingRef.current[i];

                    if (sibling.ref) {
                        const siblingRect = sibling.ref.getBoundingClientRect();

                        if (movementY > 0 && rect.bottom > siblingRect.top + siblingRect.height / 2) {
                            sibling.moveTransform = i > index ? `translateY(-${item.moveHeight || 0}px)` : "";
                        } else if (movementY < 0 && rect.top < siblingRect.top + siblingRect.height / 2) {
                            sibling.moveTransform = i < index ? `translateY(${item.moveHeight || 0}px)` : "";
                        }
                    }
                }
            }

            update({});
        }
    };
    const onPointerUp = (index: number) => (e: PointerEvent<HTMLButtonElement>) => {
        const item = rankingRef.current[index];

        if (e.isPrimary && typeof item.moveId === "number") {
            const rect = item.ref?.getBoundingClientRect();
            let to = -1;

            e.preventDefault();
            e.stopPropagation();

            item.ref?.releasePointerCapture(item.moveId);
            item.moveId = undefined;

            for (let i = 0; i < rankingRef.current.length; i++) {
                const sibling = rankingRef.current[i];

                sibling.moveTransform = undefined;

                if ((to === -1 || i > index) && i !== index && sibling.ref && rect) {
                    const siblingRect = sibling.ref.getBoundingClientRect();

                    if (
                        (i < index && rect.top < siblingRect.top + siblingRect.height / 2) ||
                        (i > index && rect.bottom > siblingRect.top + siblingRect.height / 2)
                    ) {
                        to = i;
                    }
                }
            }

            if (to !== -1) {
                moveTo(index, to);
            } else {
                update({
                    disable: true,
                });
            }
        }
    };
    const onKeyDown = (index: number) => (e: KeyboardEvent<HTMLButtonElement>) => {
        if ((e.target as HTMLElement).tagName !== "SELECT") {
            if (e.key === "Escape") {
                e.currentTarget.blur();
            } else if (e.key === "Tab") {
                e.currentTarget.blur();

                if (e.shiftKey) {
                    if (props.onCancel && index === 0) {
                        e.preventDefault();

                        props.onCancel();
                    }
                } else if (props.onSubmit && !e.shiftKey && index === rankingRef.current.length - 1) {
                    e.preventDefault();

                    props.onSubmit();
                }
            } else if (e.key === "ArrowDown" || e.key === "ArrowUp") {
                e.preventDefault();
                e.stopPropagation();

                if ((e.key === "ArrowDown" && index < rankingRef.current.length - 1) || (e.key === "ArrowUp" && index > 0)) {
                    moveTo(index, e.key === "ArrowDown" ? index + 1 : index - 1);
                }
            } else if (e.keyCode >= 49 && e.keyCode <= 57) {
                moveTo(index, e.keyCode - 49);
            }
        }
    };
    const moveTo = (src: number, dest: number) => {
        if (dest === -1 && props.slots) {
            dest = Num.min(props.slots.length, rankingRef.current.length);
        }

        if (src >= 0 && src < rankingRef.current.length && dest >= 0 && src !== dest) {
            rankingRef.current.splice(dest, 0, rankingRef.current.splice(src, 1)[0]);

            if (props.slots) {
                let free = 0;

                while (free < props.slots.length && props.slots[free].reference) {
                    free++;
                }

                if (dest < props.slots.length && dest - 1 === free) {
                    free++;
                }

                for (let i = 0; i < free; i++) {
                    const item = arrayItem(rankingRef.current, i);

                    props.slots[i].set(item?.value || item?.name, item?.id, item?.name);
                }
            }
        }

        update({
            disable: true,
        });
    };

    // Update the ranking array when there is a change in the number of options or when one or more slot values are changed
    if (
        rankingRef.current.length !== props.options.length ||
        findFirst(props.slots, (slot) => slotsRef.current[slot.key] !== slot.reference)
    ) {
        const assigned: string[] = [];
        const options = filter(rankingRef.current.splice(0, rankingRef.current.length), (option) =>
            findFirst(props.options, (ref) => ref.id === option.id) ? true : false
        );

        each(props.slots, (slot) => {
            if (slot.reference && !findFirst(assigned, (ref) => ref === slot.reference)) {
                const option = findFirst(props.options, (opt) => opt.id === slot.reference);

                if (option) {
                    slotsRef.current[slot.key] = slot.reference;
                    assigned.push(slot.reference);
                    rankingRef.current.push(option);

                    for (let i = 0; i < options.length; i++) {
                        if (options[i].id === option.id) {
                            options.splice(i, 1);

                            break;
                        }
                    }
                } else {
                    slotsRef.current[slot.key] = undefined;

                    slot.set(undefined, undefined, undefined);
                }
            } else {
                slotsRef.current[slot.key] = undefined;

                if (slot.reference) {
                    slot.set(undefined, undefined, undefined);
                }
            }
        });

        rankingRef.current.push(...options);

        each(props.options, (option) => {
            if (!findFirst(rankingRef.current, (ref) => ref.id === option.id)) {
                rankingRef.current.push(option);
            }
        });
    }

    // Check if there are any slots without an option
    if (props.slots && findFirst(props.slots, (slot) => !slot.reference)) {
        for (let i = 0; i < props.slots.length; i++) {
            const item = arrayItem(rankingRef.current, i);

            props.slots[i].set(item?.value || item?.name, item?.id, item?.name);
        }
    }

    const allowDeselect = (props.slots && props.slots.length < rankingRef.current.length) || false;

    return (
        <RankingElement $margin={props.styles.margin}>
            {rankingRef.current.map((option, index) => {
                const slot = arrayItem(props.slots, index);
                const selected = slot?.reference === option.id;
                const disabled = slot?.isFrozen || slot?.isLocked || props.disabled;

                return (
                    <RankingOptionElement
                        key={option.id}
                        ref={(ref: HTMLButtonElement) => {
                            if (ref) {
                                option.ref = ref;
                            }

                            if (!index && props.onAutoFocus) {
                                props.onAutoFocus(ref);
                            }
                        }}
                        type="button"
                        style={
                            (option.moveTransform && {
                                zIndex: (typeof option.moveId === "number" && 214748364) || undefined,
                                transition: (typeof option.moveId === "number" && "none") || undefined,
                                transform: option.moveTransform,
                                cursor: "grabbing",
                            }) ||
                            (animations.disable && {
                                transition: "none",
                            }) ||
                            undefined
                        }
                        disabled={disabled}
                        tabIndex={props.tabIndex}
                        aria-describedby={props.ariaDescribedBy}
                        onFocus={props.onFocus}
                        onBlur={props.onBlur}
                        onPointerDown={onPointerDown(index)}
                        onPointerMove={onPointerMove(index)}
                        onPointerUp={onPointerUp(index)}
                        onPointerCancel={onPointerUp(index)}
                        onKeyDown={onKeyDown(index)}
                        $baseColor={props.styles.color}
                        $mode={selected && (props.slots?.length || 0) < rankingRef.current.length ? "fill" : "outline"}
                        $hover={"outline"}
                        $outlineSize={props.styles.outlineSize || 1}
                        $roundness={props.styles.roundness}
                        $scale={props.styles.scale || 1}
                    >
                        <select
                            tabIndex={-1}
                            value={!allowDeselect || selected ? index : -1}
                            onChange={(e) => moveTo(index, castToNumber(e.currentTarget.value, -1))}
                            title={option.name}
                        >
                            {(props.slots || rankingRef.current).map((_, i) => (
                                <option value={i} key={i}>
                                    {i + 1}
                                </option>
                            ))}
                            {(allowDeselect && <option value={-1}>-</option>) || undefined}
                        </select>
                        <span>
                            {option.label || option.name || "..."}
                            {option.description && <small>{option.description}</small>}
                        </span>
                        <div>{positionIcon}</div>
                    </RankingOptionElement>
                );
            })}
        </RankingElement>
    );
};
