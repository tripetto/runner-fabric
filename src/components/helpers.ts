import { FocusEvent, MutableRefObject } from "react";

export const setReturnValue = <T>(setValue: (value: T) => void, value: T | void) => {
    if (typeof value !== "undefined") {
        setValue(value);
    }
};

export const handleEvent =
    <T>(setValue: (value: T) => void, event?: (e: FocusEvent) => T | void) =>
    (e: FocusEvent) => {
        if (event) {
            setReturnValue(setValue, event(e));
        }
    };

export const handleFocus =
    <T>(setFocus: (focus: boolean) => void, setValue: (value: T) => void, event?: (e: FocusEvent) => T | void) =>
    (e: FocusEvent) => {
        setFocus(true);

        if (event) {
            setReturnValue(setValue, event(e));
        }
    };

export const handleBlur =
    <T>(setFocus: (focus: boolean) => void, setValue: (value: T) => void, event?: (e: FocusEvent) => T | void) =>
    (e: FocusEvent) => {
        setFocus(false);

        if (event) {
            setReturnValue(setValue, event(e));
        }
    };

export const handleAutoSubmit = (
    autoSubmitRef: MutableRefObject<{
        id: number;
        cb?: () => void;
    }>
) => {
    autoSubmitRef.current.id = setTimeout(() => autoSubmitRef.current.cb && autoSubmitRef.current.cb(), 200) as unknown as number;
};
