import { MutableRefObject, useEffect, useRef, useState } from "react";
import { FontLoader } from "./loader";

export const useFontLoader = (font: string, frameRef?: MutableRefObject<HTMLIFrameElement>, fallback?: string) => {
    const [fontFamily, setFontFamily] = useState<string | undefined>(() =>
        font ? (FontLoader.isStandardFont(font) ? font : undefined) : ""
    );

    const idRef = useRef("");
    const fontRef = useRef("");
    const setRef = useRef(setFontFamily);

    if (!idRef.current) {
        idRef.current = FontLoader.id;
    }

    setRef.current = setFontFamily;

    useEffect(() => {
        if (fontRef.current !== font) {
            fontRef.current = font;

            if (fontFamily !== font) {
                FontLoader.request(idRef, fontRef, frameRef, setRef, fallback);
            }
        }
    });

    return [typeof fontFamily !== "string", fontFamily] as [boolean, string];
};
