import { MutableRefObject } from "react";
import { TAny, arraySize, findFirst, scheduleAnimation, scheduleFrame } from "@tripetto/runner";

interface IFontLoaderRequest {
    readonly id: string;
    readonly ref: MutableRefObject<string>;
    readonly context: MutableRefObject<HTMLIFrameElement> | undefined;
    font: string;
    url: string;
    set: MutableRefObject<(family: string) => void>;
    fallback?: string;
    loading?: boolean;
}

export class FontLoader {
    private static counter = 0;
    private static initialized = false;
    private static ready = false;
    private static requests: {
        [id: string]: IFontLoaderRequest;
    } = {};

    static get id() {
        return `${++this.counter}`;
    }

    private static init(): boolean {
        if (!this.initialized) {
            this.initialized = true;

            // Firefox needs some extra time before we can use WebFontLoader
            if (navigator.userAgent.toLowerCase().indexOf("firefox") > -1) {
                scheduleAnimation(() => {
                    this.ready = true;

                    this.load();
                });
            } else {
                this.ready = true;
            }
        }

        return !this.ready;
    }

    private static load(): void {
        if (this.init()) {
            return;
        }

        if (arraySize(this.requests) > 0) {
            const nextRequest = findFirst(this.requests, (request) => (!request.context || request.context.current ? true : false));

            if (nextRequest) {
                const loading = findFirst(this.requests, (request) => request.loading || false);

                if (loading && loading.id !== nextRequest.id) {
                    return;
                }

                const font = nextRequest.ref.current;
                const config = nextRequest.url
                    ? {
                          custom: {
                              families: [nextRequest.font],
                              urls: [nextRequest.url],
                          },
                      }
                    : {
                          google: {
                              families: [nextRequest.font],
                          },
                      };

                nextRequest.loading = true;

                // eslint-disable-next-line @typescript-eslint/no-var-requires
                require("webfontloader").load({
                    ...config,
                    classes: false,
                    fontactive: (familyName: string) => this.done(nextRequest, font, familyName),
                    inactive: () => this.done(nextRequest, font, nextRequest.fallback || ""),
                    context: nextRequest.context?.current.contentWindow as TAny,
                });
            } else {
                scheduleFrame(() => this.load());
            }
        }
    }

    private static done(request: IFontLoaderRequest, font: string, familyName: string): void {
        if (request.ref.current === font) {
            delete this.requests[request.id];

            request.set.current(familyName);
        } else {
            request.loading = false;
        }

        this.load();
    }

    static request(
        id: MutableRefObject<string>,
        ref: MutableRefObject<string>,
        context: MutableRefObject<HTMLIFrameElement> | undefined,
        set: MutableRefObject<(family: string) => void>,
        fallback?: string
    ) {
        if (!ref.current || this.isStandardFont(ref.current)) {
            delete this.requests[id.current];

            set.current(ref.current);

            return;
        }

        let font = ref.current;
        let url = "";

        if (font.indexOf("https://") === 0 || font.indexOf("/") === 0) {
            const hashSign = font.lastIndexOf("#");

            if (hashSign !== -1) {
                let family = font.substr(hashSign + 1);

                url = font.substr(0, hashSign);

                if (family.indexOf("&") !== -1) {
                    family = family.substr(0, family.indexOf("&"));
                }

                if (family.indexOf("|") !== -1) {
                    family = family.substr(0, family.indexOf("|"));
                }

                try {
                    font = decodeURIComponent(family.replace(/\+/g, " "));
                } catch {
                    font = "";
                    url = "";
                }
            } else {
                font = "";
            }
        }

        if (font) {
            const instance = this.requests[id.current];

            if (instance) {
                instance.font = font;
                instance.url = url;
                instance.set = set;
            } else {
                this.requests[id.current] = {
                    id: id.current,
                    ref,
                    font,
                    url,
                    context,
                    set,
                    fallback,
                };
            }

            this.load();
        } else {
            delete this.requests[id.current];

            set.current(fallback || "");
        }
    }

    static isStandardFont(font: string): boolean {
        switch (font.toLowerCase()) {
            case "arial":
                return true;
            case "helvetica":
                return true;
            case "times new roman":
                return true;
            case "times":
                return true;
            case "courier new":
                return true;
            case "courier":
                return true;
            case "sans-serif":
                return true;
        }

        return false;
    }
}
