## <a href="https://tripetto.com/sdk/"><img src="https://unpkg.com/@tripetto/builder/assets/header.svg" alt="Tripetto FormBuilder SDK"></a>

🙋‍♂️ The *Tripetto FormBuilder SDK* helps building **powerful and deeply customizable forms for your application, web app, or website.**

👩‍💻 Create and run forms and surveys **without depending on external services.**

💸 Developing a custom form solution is tedious and expensive. Instead, use Tripetto and **save time and money!**

🌎 Trusted and used by organizations **around the globe**, including [Fortune 500 companies](https://en.wikipedia.org/wiki/Fortune_500).

---

*This SDK is the ultimate form solution for everything from basic contact forms to surveys, quizzes and more with intricate flow logic. Whether you're just adding conversational forms to your website or application, or also need visual form-building capabilities inside your app, Tripetto has got you covered! Pick what you need from the SDK with [visual form builder](https://tripetto.com/sdk/docs/builder/introduction/), [form runners](https://tripetto.com/sdk/docs/runner/introduction/), and countless [question types](https://tripetto.com/sdk/docs/blocks/introduction/) – all with [extensive docs](https://tripetto.com/sdk/docs/). Or take things up a notch by developing your [own question types](https://tripetto.com/sdk/docs/blocks/custom/introduction/) or even [form runner UIs](https://tripetto.com/sdk/docs/runner/custom/introduction/).*

---

## 📦 Tripetto Runner Fabric
[![Version](https://badgen.net/npm/v/@tripetto/runner-fabric?icon=npm&label)](https://www.npmjs.com/package/@tripetto/runner-fabric)
[![Downloads](https://badgen.net/npm/dt/@tripetto/runner-fabric?icon=libraries&label)](https://www.npmjs.com/package/@tripetto/runner-fabric)
[![License](https://badgen.net/npm/license/@tripetto/runner-fabric?icon=libraries&label)](https://www.npmjs.com/package/@tripetto/runner-fabric)
[![Read the docs](https://badgen.net/badge/icon/docs/cyan?icon=wiki&label)](https://tripetto.com/sdk/docs/runner/api/fabric/)
[![Source code](https://badgen.net/badge/icon/source/black?icon=gitlab&label)](https://gitlab.com/tripetto/runner-fabric/)
[![Follow us on Twitter](https://badgen.net/badge/icon/@tripetto?icon=twitter&label)](https://twitter.com/tripetto)

The Runner Fabric package contains a set of UI components that are used in the [stock runners](https://tripetto.com/sdk/docs/runner/stock/introduction/). If you are developing [custom blocks](https://tripetto.com/sdk/docs/blocks/custom/introduction/) for those stock runners, you can use these controls in your custom block if you like.

## 🚀 Get started
You can find all the information to start with this package at [tripetto.com/sdk/docs/runner/api/fabric/](https://tripetto.com/sdk/docs/runner/api/fabric/).

## 📖 Documentation
Tripetto has practical, extensive documentation. Find everything you need at [tripetto.com/sdk/docs/](https://tripetto.com/sdk/docs/).

## 🆘 Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/runner-fabric/-/issues).

Need help or assistance? Please go to our [support page](https://tripetto.com/sdk/support/). We're more than happy to help you.

## 👋 About us
If you want to learn more about Tripetto or contribute in any way, visit us at [tripetto.com](https://tripetto.com/).
