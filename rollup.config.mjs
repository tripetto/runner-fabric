import typescript from "@rollup/plugin-typescript";

export default [
    {
        input: ["src/index.ts"],
        output: [{ dir: "dist/esm", format: "esm", entryFileNames: "[name].mjs", preserveModules: true }],
        external: ["@tripetto/runner", "tslib", "styled-components", "color", "color-name", "react", "react/jsx-runtime"],
        plugins: [
            typescript({
                target: "ES6",
                module: "ES6",
                outDir: "./dist/esm/",
                removeComments: true,
            }),
        ],
        onwarn(warning, rollupWarn) {
            if (warning.code !== "CIRCULAR_DEPENDENCY") {
                rollupWarn(warning);
            }
        },
    },
];
